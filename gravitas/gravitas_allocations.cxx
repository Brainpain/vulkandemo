//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_allocations.hxx"

#include "gravitas_device.hxx"
#include "gravitas_log.hxx"

bool
CreateBuffer(const CGravitasDevice   *gravitas_device,
             VkDeviceSize             size,
             VkBufferUsageFlags       buffer_usage,
             VmaMemoryUsage           memory_usage,
             VmaAllocationCreateFlags allocation_flags,
             GravitasAllocatedBuffer &allocated_buffer)
{
   VkDevice           device      = gravitas_device->GetVulkanDevice();
   VkBufferCreateInfo create_info = {};
   create_info.sType              = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
   create_info.pNext              = nullptr;
   create_info.size               = size;
   create_info.usage              = buffer_usage;
   create_info.sharingMode        = VK_SHARING_MODE_EXCLUSIVE;

   VmaAllocationCreateInfo alloc_info = {};
   alloc_info.flags                   = allocation_flags;
   alloc_info.usage                   = memory_usage;

   // Create the buffer on the GPU for storing the data
   VmaAllocator vma_allocator = gravitas_device->GetAllocator();
   if (VK_SUCCESS != vmaCreateBuffer(vma_allocator,
                                     &create_info,
                                     &alloc_info,
                                     &allocated_buffer.vk_buffer,
                                     &allocated_buffer.vma_allocation,
                                     nullptr))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed allocating vma buffer!";
      return false;
   }
   return true;
}

bool
CreateImage(const CGravitasDevice  *gravitas_device,
            VkFormat                format,
            uint32_t                width,
            uint32_t                height,
            VkImageUsageFlags       image_usage,
            uint32_t                num_mipmap_levels,
            GravitasAllocatedImage &allocated_image)
{
   VkDevice          device      = gravitas_device->GetVulkanDevice();
   VkImageCreateInfo create_info = {};
   create_info.sType             = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
   create_info.pNext             = nullptr;
   create_info.flags             = 0;
   create_info.imageType         = VK_IMAGE_TYPE_2D;
   create_info.format            = format;
   create_info.extent            = {width, height, 1};
   create_info.mipLevels         = num_mipmap_levels;
   create_info.arrayLayers       = 1;
   create_info.samples           = VK_SAMPLE_COUNT_1_BIT;
   create_info.tiling            = VK_IMAGE_TILING_OPTIMAL;
   create_info.usage             = image_usage;
   create_info.sharingMode       = VK_SHARING_MODE_EXCLUSIVE;
   create_info.initialLayout     = VK_IMAGE_LAYOUT_UNDEFINED;

   VmaAllocationCreateInfo alloc_info = {};
   alloc_info.usage                   = VMA_MEMORY_USAGE_GPU_ONLY;

   // Create the image on the GPU for storing the texture data
   VmaAllocator vma_allocator = gravitas_device->GetAllocator();
   if (VK_SUCCESS != vmaCreateImage(vma_allocator,
                                    &create_info,
                                    &alloc_info,
                                    &allocated_image.vk_image,
                                    &allocated_image.vma_allocation,
                                    nullptr))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Texture failed allocating vma buffer!";
      return false;
   }
   return true;
}
