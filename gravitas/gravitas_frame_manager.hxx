//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_allocations.hxx"
#include "gravitas_frame_data.hxx"

#include <string>
#include <vector>
#include <vulkan/vk_platform.h>
#include <vulkan/vulkan_core.h>

class IVulkanInstanceOwner;
class CGravitasDevice;
class CGravitasSwapchain;

struct GravitasDepthBuffer
{
   GravitasAllocatedImage alloc_image {};
   VkImageView            vk_image_view = VK_NULL_HANDLE;
   VkFormat               vk_format     = VK_FORMAT_D32_SFLOAT;
};

class CGravitasFrameManager
{
 public:
   CGravitasFrameManager(const IVulkanInstanceOwner *instance_owner,
                         const VkExtent2D           *window_extents,
                         CGravitasDevice            *gravitas_device,
                         uint8_t                     num_swapchain_images,
                         VkFormat                    swapchain_format);
   ~CGravitasFrameManager();

   // Frame utility methods
   SFrameData *StartFrame();
   bool        EndFrame();

   SFrameData *GetCurrentFrame()
   {
      return &frames_[cur_frame_data_];
   }
   SFrameData *GetFrame(uint8_t index)
   {
      return &frames_[index];
   }
   uint8_t GetFrameCount()
   {
      return num_swapchain_images_;
   }
   uint8_t GetCurrentFrameIndex()
   {
      return swapchain_index_;
   }
   VkRenderPass GetVkRenderPass() const
   {
      return vk_render_pass_;
   }

 private:
   bool InitVulkanFrameData();
   void FreeVulkanFrameData();

   bool CreateSwapchain();
   void FreeSwapchain();

   const IVulkanInstanceOwner *instance_owner_ = nullptr;
   VkExtent2D                  window_extents_;
   CGravitasDevice            *gravitas_device_ = VK_NULL_HANDLE;
   VkDevice                    vk_device_       = VK_NULL_HANDLE;
   VkFormat                    vk_swapchain_format_;
   uint8_t                     num_swapchain_images_ = 2;
   uint32_t                    swapchain_index_      = 0;
   CGravitasSwapchain         *gravitas_swapchain_   = nullptr;
   uint8_t                     cur_frame_data_       = 0;
   std::vector<SFrameData>     frames_;
   GravitasDepthBuffer         gravitas_depth_buffer_;
   VkRenderPass                vk_render_pass_ = VK_NULL_HANDLE;
};
