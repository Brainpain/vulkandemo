//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//
// Based off log header reported by Alberto Lepe <dev@alepe.com> on StackOverflow
// created December 1, 2015, 6:00 PM.
//

#pragma once

#include <assert.h>
#include <fstream>
#include <iostream>
#include <sys/time.h>

enum GravitasLogLevel
{
   GRAV_LOG_VERBOSE,
   GRAV_LOG_INFO,
   GRAV_LOG_WARNING,
   GRAV_LOG_ERROR,
};

struct GavitasLogConfig
{
   bool             output_time       = false;
   GravitasLogLevel min_log_level     = GRAV_LOG_WARNING;
   std::ostream    &std_output_stream = std::cout;
   std::ofstream   *file_stream       = nullptr;
};

extern GavitasLogConfig gravitas_log_config;

class CGravitasLog
{
 public:
   CGravitasLog()
   {}

   CGravitasLog(GravitasLogLevel type)
   {
      std::string message;
      message_type_ = type;
      if (gravitas_log_config.output_time)
      {
         message = GetCurTimeString();
      }
      message += TypeLabel(type);
      operator<<(message);
#if !defined(NDEBUG) && defined(ASSERT_ON_ERRORS)
      if (type == GRAV_LOG_ERROR)
      {
         if (gravitas_log_config.file_stream)
         {
            (*gravitas_log_config.file_stream).flush();
         }
         else
         {
            gravitas_log_config.std_output_stream.flush();
         }
         assert(false);
      }
#endif
   }
   ~CGravitasLog()
   {
      if (writing_message_)
      {
         operator<<("\n");
      }
      writing_message_ = false;
   }

   template<class T>
   CGravitasLog &operator<<(const T &msg)
   {
      if (message_type_ >= gravitas_log_config.min_log_level)
      {
         if (gravitas_log_config.file_stream)
         {
            (*gravitas_log_config.file_stream) << msg;
         }
         else
         {
            gravitas_log_config.std_output_stream << msg;
         }
         writing_message_ = true;
      }
      return *this;
   }

 private:
   inline std::string TypeLabel(GravitasLogLevel type)
   {
      std::string label;
      switch (type)
      {
         case GRAV_LOG_ERROR:
            label = "!!! Error !!! ";
            break;
         case GRAV_LOG_WARNING:
            label = "** Warning ** ";
            break;
         case GRAV_LOG_INFO:
            label = "--  Info   -- ";
            break;
         case GRAV_LOG_VERBOSE:
            label = ".. Verbose .. ";
            break;
         default:
            label = "";
            break;
      }
      return label;
   }

   std::string GetCurTimeString()
   {
      char           fmt[64];
      char           buf[64];
      struct timeval tv;
      struct tm     *tm;

      gettimeofday(&tv, NULL);
      tm = localtime(&tv.tv_sec);
      strftime(fmt, sizeof(fmt), "[%H:%M:%S:%%06u] ", tm);
      snprintf(buf, sizeof(buf), fmt, tv.tv_usec);
      return buf;
   }

   bool             writing_message_ = false;
   GravitasLogLevel message_type_    = GRAV_LOG_VERBOSE;
};
