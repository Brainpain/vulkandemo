//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include <string>
#include <vector>
#include <vulkan/vk_platform.h>
#include <vulkan/vulkan_core.h>

class IVulkanInstanceOwner;
class CGravitasDevice;
class CGravitasSwapchain
{
 public:
   CGravitasSwapchain(const IVulkanInstanceOwner *instance_owner,
                      const VkExtent2D           *window_extents,
                      const CGravitasDevice      *device,
                      VkFormat                    format,
                      uint8_t                     num_images);
   ~CGravitasSwapchain();

   bool           GetNextIndex(VkSemaphore present_sem, uint64_t timeout, uint32_t *index);
   VkSwapchainKHR GetVkSwapchain()
   {
      return vk_swapchain_;
   }
   VkFormat GetFormat()
   {
      return vk_swapchain_format_;
   }
   VkImage GetImage(uint8_t image_index)
   {
      return vk_swapchain_images_[image_index];
   }
   VkImageView GetImageView(uint8_t image_index)
   {
      return vk_swapchain_imageviews_[image_index];
   }

 private:
   bool InitVkSwapchain();
   void FreeVkSwapchain();

   const IVulkanInstanceOwner *instance_owner_ = nullptr;
   VkExtent2D                  window_extents_;
   const CGravitasDevice      *gravitas_device_ = nullptr;
   VkDevice                    vk_device_       = VK_NULL_HANDLE;
   VkSurfaceKHR                vk_surface_      = VK_NULL_HANDLE;
   VkSwapchainKHR              vk_swapchain_    = VK_NULL_HANDLE;
   uint8_t                     num_swapchain_images_;
   VkFormat                    vk_swapchain_format_;
   std::vector<VkImage>        vk_swapchain_images_;
   std::vector<VkImageView>    vk_swapchain_imageviews_;
};
