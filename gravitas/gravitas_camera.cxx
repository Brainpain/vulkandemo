//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_camera.hxx"

#include <cstring>
#include <glm/gtx/transform.hpp>

void
CGravitasCamera::LookAt(glm::vec3 target)
{
   camera_shader_data_.project_mat =
     glm::perspective(current_.fov_half, width_ / height_, near_dist_, far_dist_);
   // Flip y for Vulkan
   camera_shader_data_.project_mat[1].y *= -1.f;
   glm::vec3 direction  = glm::normalize(target - current_.position);
   current_.orientation = glm::quatLookAt(direction, glm::vec3(0.f, 1.f, 0.f));

   // Turn off the path
   moving_through_path_ = false;
}

bool
CGravitasCamera::Update(float update_time_ms)
{
   camera_shader_data_.view_mat =
     glm::toMat4(current_.orientation) * glm::translate(glm::mat4(1.f), -current_.position);
   camera_shader_data_.view_project_mat =
     camera_shader_data_.project_mat * camera_shader_data_.view_mat;
   return true;
}

void
CGravitasCamera::WriteCameraMatrixInfo(float *write_data)
{
   memcpy(write_data, &camera_shader_data_, sizeof(GravitasCameraShaderData));
}
