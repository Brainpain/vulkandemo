//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_scene.hxx"

#include "gravitas_app.hxx"
#include "gravitas_camera.hxx"
#include "gravitas_device.hxx"
#include "gravitas_log.hxx"
#include "gravitas_render_object.hxx"
#include "gravitas_resource_manager.hxx"

#include <array>

CGravitasScene::CGravitasScene()
{}

CGravitasScene::~CGravitasScene()
{
   if (active_)
   {
      StopScene();
   }
   if (loaded_)
   {
      UnloadScene();
   }
}

bool
CGravitasScene::InternalInit(CGravitasApp *parent, const std::string &scene_name)
{
   if (nullptr != parent_app_)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Scene " << scene_name << " received multiple set parent calls!";
   }
   parent_app_            = parent;
   name_                  = scene_name;
   gravitas_device_       = parent_app_->GetDevice();
   vk_device_             = gravitas_device_->GetVulkanDevice();
   vulkan_dev_dispatch_   = gravitas_device_->GetDispatchTable();
   gravitas_resource_mgr_ = parent_app_->GetResourceManager();

   CGravitasWindow *window = parent_app_->GetWindow();
   camera_.UpdateWindowDimensions(window->GetWidth(), window->GetHeight());

   CalculateSceneUniformBufferSize();

   // Allocate scene memory
   size_t  uniform_aligned_size = scene_uniform_buffer_size_;
   uint8_t num_frames           = gravitas_resource_mgr_->GetFrameManager()->GetFrameCount();

   if (uniform_aligned_size * num_frames > gravitas_device_->GetLimits()->maxUniformBufferRange)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Scene " << scene_name << " uniform buffer data too large to fit on device!";
      return false;
   }

   VkBufferCreateInfo create_info = {};
   create_info.sType              = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
   create_info.pNext              = nullptr;
   create_info.size               = uniform_aligned_size * num_frames;
   create_info.usage              = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

   VmaAllocationCreateInfo alloc_info = {};
   alloc_info.flags                   = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
   alloc_info.usage                   = VMA_MEMORY_USAGE_CPU_TO_GPU;

   VmaAllocator vma_allocator = gravitas_device_->GetAllocator();
   if (VK_SUCCESS != vmaCreateBuffer(vma_allocator,
                                     &create_info,
                                     &alloc_info,
                                     &scene_uniform_buffer_.vk_buffer,
                                     &scene_uniform_buffer_.vma_allocation,
                                     nullptr))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Scene " << scene_name << " failed allocating scene uniform buffer!";
      return false;
   }

   uint32_t                          max_set_count         = 10;
   std::vector<VkDescriptorPoolSize> descriptor_pool_sizes = {
     {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, max_set_count},
     {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, max_set_count},
     {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, max_set_count},
   };
   VkDescriptorPoolCreateInfo desc_pool_create = {};
   desc_pool_create.sType                      = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
   desc_pool_create.pNext                      = nullptr;
   desc_pool_create.flags                      = 0;
   desc_pool_create.maxSets                    = max_set_count;
   desc_pool_create.poolSizeCount = static_cast<uint32_t>(descriptor_pool_sizes.size());
   desc_pool_create.pPoolSizes    = descriptor_pool_sizes.data();
   if (VK_SUCCESS != vulkan_dev_dispatch_->CreateDescriptorPool(
                       vk_device_, &desc_pool_create, nullptr, &scene_desc_pool_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Scene " << scene_name << " failed creating descriptor pool!";
      return false;
   }

   scene_desc_sets_.resize(num_frames);
   for (uint32_t scs = 0; scs < num_frames; ++scs)
   {
      VkDescriptorSetLayout layout = parent_app_->GetGlobalSceneDescriptorLayout();
      // Create the descriptor set for the uniform buffer
      VkDescriptorSetAllocateInfo desc_set_alloc_info = {};
      desc_set_alloc_info.sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
      desc_set_alloc_info.pNext              = nullptr;
      desc_set_alloc_info.descriptorPool     = scene_desc_pool_;
      desc_set_alloc_info.descriptorSetCount = 1;
      desc_set_alloc_info.pSetLayouts        = &layout;
      if (VK_SUCCESS != vulkan_dev_dispatch_->AllocateDescriptorSets(
                          vk_device_, &desc_set_alloc_info, &scene_desc_sets_[scs]))
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Scene " << scene_name << " failed allocating descriptor set from pool!";
         return false;
      }

      VkDescriptorBufferInfo desc_buf_info = {};
      desc_buf_info.buffer                 = scene_uniform_buffer_.vk_buffer;
      desc_buf_info.offset                 = 0;
      desc_buf_info.range                  = uniform_aligned_size;

      VkWriteDescriptorSet write_desc_set = {};
      write_desc_set.sType                = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
      write_desc_set.pNext                = nullptr;
      write_desc_set.dstSet               = scene_desc_sets_[scs];
      write_desc_set.dstBinding           = 0;
      write_desc_set.dstArrayElement      = 0;
      write_desc_set.descriptorCount      = 1;
      write_desc_set.descriptorType       = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
      write_desc_set.pImageInfo           = nullptr;
      write_desc_set.pBufferInfo          = &desc_buf_info;
      write_desc_set.pTexelBufferView     = nullptr;
      vulkan_dev_dispatch_->UpdateDescriptorSets(vk_device_, 1, &write_desc_set, 0, nullptr);
   }

   return true;
}

void
CGravitasScene::CalculateSceneUniformBufferSize()
{
   uint64_t alignment_req     = gravitas_device_->GetLimits()->minUniformBufferOffsetAlignment;
   uint64_t needed_size       = sizeof(GravitasCameraShaderData);
   scene_uniform_buffer_size_ = (needed_size + (alignment_req - 1)) & ~(alignment_req - 1);
}

bool
CGravitasScene::WriteSceneUniformBufferData(uint8_t *vma_ptr)
{
   camera_.WriteCameraMatrixInfo(reinterpret_cast<float *>(vma_ptr));
   return true;
}

bool
CGravitasScene::LoadScene(CGravitasApp      *parent,
                          const std::string &scene_name,
                          const std::string &scene_file)
{
   InternalInit(parent, scene_name);
   // TODO: Load scene
   return false;
}

bool
CGravitasScene::UploadScene()
{
   loaded_ = true;
   for (auto &ro : render_objects_)
   {
      if (!gravitas_resource_mgr_->UploadResource(ro->GetResourceId()))
      {
         loaded_ = false;
         break;
      }
   }
   return loaded_;
}

bool
CGravitasScene::UnloadScene()
{
   loaded_ = false;
   if (VK_NULL_HANDLE != scene_desc_pool_)
   {
      vulkan_dev_dispatch_->DestroyDescriptorPool(vk_device_, scene_desc_pool_, nullptr);
   }
   if (VK_NULL_HANDLE != scene_uniform_buffer_.vk_buffer)
   {
      vmaDestroyBuffer(gravitas_device_->GetAllocator(),
                       scene_uniform_buffer_.vk_buffer,
                       scene_uniform_buffer_.vma_allocation);
      scene_uniform_buffer_.vk_buffer      = VK_NULL_HANDLE;
      scene_uniform_buffer_.vma_allocation = nullptr;
   }
   for (auto &ro : render_objects_)
   {
      uint64_t resource_id = ro->GetResourceId();
      gravitas_resource_mgr_->UnloadResource(resource_id);
      gravitas_resource_mgr_->ReleaseResource(resource_id);
   }
   render_objects_.clear();
   return true;
}

bool
CGravitasScene::StartScene(float transition_time)
{
   active_ = true;
   return false;
}

bool
CGravitasScene::AddRenderObject(CGravitasRenderObject *render_object)
{
   render_objects_.push_back(render_object);
   return true;
}

bool
CGravitasScene::Update(SFrameData *frame_data, float update_time_ms)
{
   if (!loaded_ || !active_)
   {
      return false;
   }

   VkCommandBufferBeginInfo begin_info = {};
   begin_info.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
   begin_info.pNext                    = nullptr;
   begin_info.flags                    = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
   begin_info.pInheritanceInfo         = nullptr;
   if (VK_SUCCESS !=
       vulkan_dev_dispatch_->BeginCommandBuffer(frame_data->primary_command_buffer, &begin_info))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed starting command buffer";
      return false;
   }

   bool success = camera_.Update(update_time_ms);
   if (success)
   {
      uint8_t cur_index = gravitas_resource_mgr_->GetFrameManager()->GetCurrentFrameIndex();
      void   *vma_ptr;
      if (VK_SUCCESS != vmaMapMemory(gravitas_device_->GetAllocator(),
                                     scene_uniform_buffer_.vma_allocation,
                                     &vma_ptr))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Scene " << name_ << " failed to map uniform buffer!";
         return false;
      }
      uint8_t *u8_ptr =
        reinterpret_cast<uint8_t *>(vma_ptr) + (scene_uniform_buffer_size_ * cur_index);
      if (!WriteSceneUniformBufferData(u8_ptr))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Scene " << name_ << " failed writing to uniform buffer!";
         return false;
      }
      vmaUnmapMemory(gravitas_device_->GetAllocator(), scene_uniform_buffer_.vma_allocation);
   }
   return success;
}

bool
CGravitasScene::Draw(SFrameData *frame_data)
{
   if (!loaded_ || !active_ || paused_)
   {
      return false;
   }

   uint8_t cur_index = gravitas_resource_mgr_->GetFrameManager()->GetCurrentFrameIndex();

   // Reset the current pipeline so we always update the pipeline on every first draw
   VkPipeline      cur_pipeline       = VK_NULL_HANDLE;
   VkBuffer        cur_index_buffer   = VK_NULL_HANDLE;
   VkBuffer        cur_vertex_buffer  = VK_NULL_HANDLE;
   VkDescriptorSet cur_model_desc_set = VK_NULL_HANDLE;

   VkClearValue clear_values[2];
   clear_values[0].color                = {0.0f, 0.0f, 0.0f, 1.0f};
   clear_values[1].depthStencil.depth   = 1.f;
   clear_values[1].depthStencil.stencil = 0;

   CGravitasWindow      *window    = parent_app_->GetWindow();
   VkRenderPassBeginInfo rpInfo    = {};
   rpInfo.sType                    = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
   rpInfo.pNext                    = nullptr;
   rpInfo.renderPass               = gravitas_resource_mgr_->GetFrameManager()->GetVkRenderPass();
   rpInfo.framebuffer              = frame_data->framebuffer;
   rpInfo.renderArea.offset.x      = 0;
   rpInfo.renderArea.offset.y      = 0;
   rpInfo.renderArea.extent.width  = window->GetWidth();
   rpInfo.renderArea.extent.height = window->GetHeight();
   rpInfo.clearValueCount          = 2;
   rpInfo.pClearValues             = clear_values;
   vulkan_dev_dispatch_->CmdBeginRenderPass(
     frame_data->primary_command_buffer, &rpInfo, VK_SUBPASS_CONTENTS_INLINE);

   for (auto &ro : render_objects_)
   {
      ro->Draw(frame_data->primary_command_buffer,
               cur_vertex_buffer,
               cur_index_buffer,
               cur_pipeline,
               cur_model_desc_set,
               scene_desc_sets_[cur_index],
               (cur_index * scene_uniform_buffer_size_));
   }

   vulkan_dev_dispatch_->CmdEndRenderPass(frame_data->primary_command_buffer);
   vulkan_dev_dispatch_->EndCommandBuffer(frame_data->primary_command_buffer);

   current_frame_++;
   return true;
}

bool
CGravitasScene::StopScene(float transition_time)
{
   vulkan_dev_dispatch_->DeviceWaitIdle(gravitas_device_->GetVulkanDevice());
   active_ = false;
   return false;
}

bool
CGravitasScene::ResizeEvent(uint32_t width, uint32_t height)
{
   for (auto &render_obj : render_objects_)
   {
      render_obj->ResizeEvent();
   }
   camera_.UpdateWindowDimensions((float)width, (float)height);
   return true;
}
