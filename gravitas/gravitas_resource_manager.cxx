//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_resource_manager.hxx"

#include "gravitas_app.hxx"
#include "gravitas_device.hxx"
#include "gravitas_frame_manager.hxx"
#include "gravitas_log.hxx"
#include "gravitas_material_library.hxx"
#include "gravitas_pipeline_library.hxx"
#include "gravitas_render_obj_library.hxx"
#include "gravitas_shader_library.hxx"
#include "gravitas_texture_library.hxx"
#include "gravitas_window.hxx"

#include <sstream>

CGravitasResourceManager::CGravitasResourceManager(CGravitasDevice            *gravitas_device,
                                                   const IGravitasWindowOwner *window_owner,
                                                   const IVulkanInstanceOwner *instance_owner)
  : gravitas_device_(gravitas_device)
  , vulkan_dev_dispatch_(gravitas_device->GetDispatchTable())
  , window_owner_(window_owner)
  , instance_owner_(instance_owner)
{
   material_lib_ = new CGravitasMaterialLibrary(this, gravitas_device_);
   if (nullptr == material_lib_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create CGravitasMaterialLibrary!";
      exit(-1);
   }

   render_object_lib_ = new CGravitasRenderObjectLibrary(this, gravitas_device_);
   if (nullptr == render_object_lib_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create CGravitasRenderObjectLibrary!";
      exit(-1);
   }

   pipeline_lib_ = new CGravitasPipelineLibrary(this, gravitas_device_);
   if (nullptr == pipeline_lib_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create CGravitasPipelineLibrary!";
      exit(-1);
   }

   shader_lib_ = new CGravitasShaderLibrary(this, gravitas_device_);
   if (nullptr == shader_lib_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create CGravitasShaderLibrary!";
      exit(-1);
   }

   texture_lib_ = new CGravitasTextureLibrary(this, gravitas_device_);
   if (nullptr == texture_lib_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create CGravitasTextureLibrary!";
      exit(-1);
   }

   if (!InitFrameManager())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to initializing frame manager!";
      exit(-1);
   }
}

CGravitasResourceManager::~CGravitasResourceManager()
{
   if (nullptr != pipeline_lib_)
   {
      delete pipeline_lib_;
      pipeline_lib_ = nullptr;
   }
   if (nullptr != material_lib_)
   {
      delete material_lib_;
      material_lib_ = nullptr;
   }
   if (nullptr != render_object_lib_)
   {
      delete render_object_lib_;
      render_object_lib_ = nullptr;
   }
   if (nullptr != shader_lib_)
   {
      delete shader_lib_;
      shader_lib_ = nullptr;
   }
   if (nullptr != texture_lib_)
   {
      delete texture_lib_;
      texture_lib_ = nullptr;
   }
   if (nullptr != frame_manager_)
   {
      delete frame_manager_;
      frame_manager_ = nullptr;
   }
}

bool
CGravitasResourceManager::ResetBeforeResize()
{
   vulkan_dev_dispatch_->DeviceWaitIdle(gravitas_device_->GetVulkanDevice());
   if (nullptr != frame_manager_)
   {
      delete frame_manager_;
      frame_manager_ = nullptr;
   }
   if (nullptr != pipeline_lib_)
   {
      pipeline_lib_->FreeInternals();
   }
   return true;
}

bool
CGravitasResourceManager::RestoreAfterResize()
{
   return InitFrameManager();
}

bool
CGravitasResourceManager::InitFrameManager()
{
   VkBool32     supports_present = VK_FALSE;
   VkSurfaceKHR vk_surface       = VK_NULL_HANDLE;

   CGravitasWindow *win = window_owner_->GetWindow();

   if (!win->CreateWindowSurface(instance_owner_->GetInstance()))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create Vulkan surface for Window!";
      return false;
   }
   vk_surface = win->GetVkSurface();

   vkGetPhysicalDeviceSurfaceSupportKHR(gravitas_device_->GetVulkanPhysicalDevice(),
                                        gravitas_device_->GetGraphicsQueueIndex(),
                                        vk_surface,
                                        &supports_present);
   if (VK_TRUE != supports_present)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Graphics Queue does not support present!";
      return false;
   }

   VkSurfaceCapabilitiesKHR capabilities;
   vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
     gravitas_device_->GetVulkanPhysicalDevice(), vk_surface, &capabilities);

   VkExtent2D drawable_size;
   win->GetDrawableSize(drawable_size);
   if (drawable_size.width < capabilities.minImageExtent.width)
   {
      drawable_size.width = capabilities.minImageExtent.width;
   }
   if (drawable_size.height < capabilities.minImageExtent.height)
   {
      drawable_size.height = capabilities.minImageExtent.height;
   }
   if (drawable_size.width > capabilities.maxImageExtent.width)
   {
      drawable_size.width = capabilities.maxImageExtent.width;
   }
   if (drawable_size.height > capabilities.maxImageExtent.height)
   {
      drawable_size.height = capabilities.maxImageExtent.height;
   }

   uint32_t num_swapchain_images = 2;
   if (capabilities.minImageCount <= 1)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Device does not support swapchains of depth > 1!";
      return false;
   }
   else if (capabilities.minImageCount > num_swapchain_images)
   {
      num_swapchain_images = capabilities.minImageCount;
   }
#ifdef ENABLE_BENCHMARK_MODE
   // For benchmarking, we'd like to do 3 swapchain images in mailbox mode so
   // we can flip to the most recent frame quickly.
   if (capabilities.maxImageCount >= 3)
   {
      num_swapchain_images = 3;
   }
#endif
   frame_manager_ = new CGravitasFrameManager(instance_owner_,
                                              &drawable_size,
                                              gravitas_device_,
                                              num_swapchain_images,
                                              window_owner_->GetDesiredFormat());
   if (nullptr == frame_manager_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating frame manager!";
      return false;
   }
   return true;
}

bool
CGravitasResourceManager::CreateResource(GravitasResourceCreateInfo *create_info,
                                         uint64_t                   &resource_id)
{
   std::ostringstream outstream_uint64;
   uint64_t           flag = GetResourceFlag(create_info->flag);

   outstream_uint64 << resource_id;
   switch (flag)
   {
      case GRAV_RESOURCE_MATERIAL: {
         GravitasMaterialResourceCreateInfo *mat_create_info =
           reinterpret_cast<GravitasMaterialResourceCreateInfo *>(create_info);
         return material_lib_->AddMaterial(mat_create_info, resource_id);
         break;
      }
      case GRAV_RESOURCE_RENDER_OBJECT: {
         GravitasRenderObjectResourceCreateInfo *render_obj_create_info =
           reinterpret_cast<GravitasRenderObjectResourceCreateInfo *>(create_info);
         return render_object_lib_->AddRenderObject(render_obj_create_info, resource_id);
      }
      case GRAV_RESOURCE_PIPELINE: {
         GravitasPipelineResourceCreateInfo *pipeline_create_info =
           reinterpret_cast<GravitasPipelineResourceCreateInfo *>(create_info);
         return pipeline_lib_->AddPipeline(pipeline_create_info, resource_id);
      }
      case GRAV_RESOURCE_SHADER: {
         GravitasShaderResourceCreateInfo *shader_create_info =
           reinterpret_cast<GravitasShaderResourceCreateInfo *>(create_info);
         return shader_lib_->AddShader(shader_create_info, resource_id);
      }
      case GRAV_RESOURCE_TEXTURE: {
         GravitasTextureResourceCreateInfo *texture_create_info =
           reinterpret_cast<GravitasTextureResourceCreateInfo *>(create_info);
         return texture_lib_->AddTexture(texture_create_info, resource_id);
         break;
      }
      default:
         outstream_uint64 << (flag >> GRAV_RESOURCE_SHIFT);
         CGravitasLog(GRAV_LOG_ERROR)
           << "CreateResource received invalid resource type " << outstream_uint64.str();
   }
   return false;
}

bool
CGravitasResourceManager::ReleaseResource(uint64_t resource_id)
{
   std::ostringstream outstream_uint64;
   uint64_t           flag = GetResourceFlag(resource_id);
   switch (flag)
   {
      case GRAV_RESOURCE_MATERIAL: {
         return material_lib_->RemoveMaterial(resource_id);
         break;
      }
      case GRAV_RESOURCE_PIPELINE: {
         return pipeline_lib_->RemovePipeline(resource_id);
      }
      case GRAV_RESOURCE_RENDER_OBJECT: {
         return render_object_lib_->RemoveRenderObject(resource_id);
      }
      case GRAV_RESOURCE_SHADER: {
         return shader_lib_->RemoveShader(resource_id);
      }
      case GRAV_RESOURCE_TEXTURE: {
         return texture_lib_->RemoveTexture(resource_id);
         break;
      }
      default:
         outstream_uint64 << (flag >> GRAV_RESOURCE_SHIFT);
         CGravitasLog(GRAV_LOG_ERROR)
           << "ReleaseResource received invalid resource type " << outstream_uint64.str();
   }
   return false;
}

bool
CGravitasResourceManager::GetPipelineResource(uint64_t                   resource_id,
                                              GravitasPipelineResource **resource)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if ((resource_id & GRAV_RESOURCE_PIPELINE) != GRAV_RESOURCE_PIPELINE)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "GetPipelineResource ID " << outstream_uint64.str() << " is not a pipeline resource!";
      return false;
   }
   *resource = pipeline_lib_->GetResource(resource_id);
   return true;
}

bool
CGravitasResourceManager::GetMaterialResource(uint64_t                   resource_id,
                                              GravitasMaterialResource **resource)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if ((resource_id & GRAV_RESOURCE_MATERIAL) != GRAV_RESOURCE_MATERIAL)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "GetMaterialResource ID " << outstream_uint64.str() << " is not a material resource!";
      return false;
   }
   *resource = material_lib_->GetResource(resource_id);
   return true;
}

bool
CGravitasResourceManager::GetRenderObjectResource(uint64_t                       resource_id,
                                                  GravitasRenderObjectResource **resource)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if ((resource_id & GRAV_RESOURCE_RENDER_OBJECT) != GRAV_RESOURCE_RENDER_OBJECT)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "GetRenderObjectResource ID " << outstream_uint64.str()
                                   << " is not a render object resource!";
      return false;
   }
   *resource = render_object_lib_->GetResource(resource_id);
   return true;
}

bool
CGravitasResourceManager::GetShaderResource(uint64_t resource_id, GravitasShaderResource **resource)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if ((resource_id & GRAV_RESOURCE_SHADER) != GRAV_RESOURCE_SHADER)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "GetShaderResource ID " << outstream_uint64.str() << " is not a shader resource!";
      return false;
   }
   *resource = shader_lib_->GetResource(resource_id);
   return true;
}

bool
CGravitasResourceManager::GetTextureResource(uint64_t                  resource_id,
                                             GravitasTextureResource **resource)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if ((resource_id & GRAV_RESOURCE_TEXTURE) != GRAV_RESOURCE_TEXTURE)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "GetTextureResource ID " << outstream_uint64.str() << " is not a texture resource!";
      return false;
   }
   *resource = texture_lib_->GetResource(resource_id);
   return true;
}

bool
CGravitasResourceManager::UploadResource(uint64_t resource_id)
{
   std::ostringstream outstream_uint64;
   uint64_t           flag     = GetResourceFlag(resource_id);
   bool               uploaded = false;

   outstream_uint64 << resource_id;
   switch (flag)
   {
      case GRAV_RESOURCE_MATERIAL: {
         uploaded = material_lib_->Upload(resource_id);
         break;
      }
      case GRAV_RESOURCE_RENDER_OBJECT: {
         uploaded = render_object_lib_->Upload(resource_id);
         break;
      }
      case GRAV_RESOURCE_PIPELINE: {
         uploaded = pipeline_lib_->Upload(resource_id);
         break;
      }
      case GRAV_RESOURCE_SHADER: {
         uploaded = shader_lib_->Upload(resource_id);
         break;
      }
      case GRAV_RESOURCE_TEXTURE: {
         uploaded = texture_lib_->Upload(resource_id);
         break;
      }
      default:
         outstream_uint64 << (flag >> GRAV_RESOURCE_SHIFT);
         CGravitasLog(GRAV_LOG_ERROR)
           << "UploadResource received invalid resource type " << outstream_uint64.str();
   }
   return uploaded;
}

bool
CGravitasResourceManager::UnloadResource(uint64_t resource_id)
{
   std::ostringstream outstream_uint64;
   uint64_t           flag     = GetResourceFlag(resource_id);
   bool               unloaded = false;

   outstream_uint64 << resource_id;
   switch (flag)
   {
      case GRAV_RESOURCE_MATERIAL: {
         unloaded = material_lib_->Unload(resource_id);
         break;
      }
      case GRAV_RESOURCE_RENDER_OBJECT: {
         unloaded = render_object_lib_->Unload(resource_id);
         break;
      }
      case GRAV_RESOURCE_PIPELINE: {
         unloaded = pipeline_lib_->Unload(resource_id);
         break;
      }
      case GRAV_RESOURCE_SHADER: {
         unloaded = shader_lib_->Unload(resource_id);
         break;
      }
      case GRAV_RESOURCE_TEXTURE: {
         unloaded = texture_lib_->Unload(resource_id);
         break;
      }
      default:
         outstream_uint64 << (flag >> GRAV_RESOURCE_SHIFT);
         CGravitasLog(GRAV_LOG_ERROR)
           << "UnloadResource received invalid resource type " << outstream_uint64.str();
   }
   return unloaded;
}

bool
CGravitasResourceManager::IncrementImportRef(uint64_t resource_id)
{
   std::ostringstream outstream_uint64;
   uint64_t           flag     = GetResourceFlag(resource_id);
   bool               unloaded = false;

   outstream_uint64 << resource_id;
   switch (flag)
   {
      case GRAV_RESOURCE_MATERIAL: {
         unloaded = material_lib_->IncrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_RENDER_OBJECT: {
         unloaded = render_object_lib_->IncrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_PIPELINE: {
         unloaded = pipeline_lib_->IncrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_SHADER: {
         unloaded = shader_lib_->IncrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_TEXTURE: {
         unloaded = texture_lib_->IncrementImportRef(resource_id);
         break;
      }
      default:
         outstream_uint64 << (flag >> GRAV_RESOURCE_SHIFT);
         CGravitasLog(GRAV_LOG_ERROR)
           << "IncrementImportRef received invalid resource type " << outstream_uint64.str();
   }
   return unloaded;
}

bool
CGravitasResourceManager::DecrementImportRef(uint64_t resource_id)
{
   std::ostringstream outstream_uint64;
   uint64_t           flag     = GetResourceFlag(resource_id);
   bool               unloaded = false;

   outstream_uint64 << resource_id;
   switch (flag)
   {
      case GRAV_RESOURCE_MATERIAL: {
         unloaded = material_lib_->DecrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_RENDER_OBJECT: {
         unloaded = render_object_lib_->DecrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_PIPELINE: {
         unloaded = pipeline_lib_->DecrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_SHADER: {
         unloaded = shader_lib_->DecrementImportRef(resource_id);
         break;
      }
      case GRAV_RESOURCE_TEXTURE: {
         unloaded = texture_lib_->DecrementImportRef(resource_id);
         break;
      }
      default:
         outstream_uint64 << (flag >> GRAV_RESOURCE_SHIFT);
         CGravitasLog(GRAV_LOG_ERROR)
           << "DecrementImportRef received invalid resource type " << outstream_uint64.str();
   }
   return unloaded;
}
