//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_allocations.hxx"
#include "gravitas_base_types.hxx"
#include "gravitas_camera.hxx"

#include <glm/glm.hpp>
#include <string>
#include <unordered_map>
#include <vector>
#include <vulkan/vulkan_core.h>

class CGravitasApp;
class CGravitasCamera;
class CGravitasDevice;
class CGravitasRenderObject;
class CGravitasResourceManager;
struct GravitasDeviceDispatchTable;
struct GravitasMaterial;
struct SFrameData;

class CGravitasScene
{
 public:
   CGravitasScene();
   virtual ~CGravitasScene();

   virtual bool InitScene(CGravitasApp *parent, const std::string &scene_name) = 0;
   virtual bool LoadScene(CGravitasApp      *parent,
                          const std::string &scene_name,
                          const std::string &scene_file);
   virtual bool UploadScene();
   virtual bool UnloadScene();

   virtual bool StartScene(float transition_time = 0.f);
   virtual bool AddRenderObject(CGravitasRenderObject *render_object);
   virtual bool Update(SFrameData *frame_data, float update_time_ms);
   virtual bool Draw(SFrameData *frame_data);
   virtual bool StopScene(float transition_time = 0.f);
   virtual bool ResizeEvent(uint32_t width, uint32_t height);

   void SetPaused(bool paused)
   {
      paused_ = paused;
   }

   virtual void CalculateSceneUniformBufferSize();
   virtual bool WriteSceneUniformBufferData(uint8_t *vma_ptr);

 protected:
   bool InternalInit(CGravitasApp *parent, const std::string &scene_name);

   CGravitasApp                        *parent_app_            = nullptr;
   CGravitasDevice                     *gravitas_device_       = nullptr;
   VkDevice                             vk_device_             = VK_NULL_HANDLE;
   const GravitasDeviceDispatchTable   *vulkan_dev_dispatch_   = nullptr;
   CGravitasResourceManager            *gravitas_resource_mgr_ = nullptr;
   std::string                          name_;
   GravitasVersionFields                version_;
   bool                                 active_ = false;
   bool                                 loaded_ = false;
   bool                                 paused_ = false;
   CGravitasCamera                      camera_;
   float                                current_time_  = 0.f;
   uint64_t                             current_frame_ = 0ULL;
   std::vector<CGravitasRenderObject *> render_objects_;
   VkDescriptorPool                     scene_desc_pool_ = VK_NULL_HANDLE;
   std::vector<VkDescriptorSet>         scene_desc_sets_;
   GravitasAllocatedBuffer              scene_uniform_buffer_;
   uint64_t                             scene_uniform_buffer_size_;
};
