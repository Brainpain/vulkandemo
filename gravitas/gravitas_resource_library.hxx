//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_resource.hxx"

#include <mutex>
#include <unordered_map>

class CGravitasDevice;
struct GravitasDeviceDispatchTable;
class CGravitasResourceManager;

class CGravitasResourceLibrary
{
 public:
   CGravitasResourceLibrary(CGravitasResourceManager *resource_mgr,
                            CGravitasDevice          *gravitas_device,
                            uint64_t                  resource_type);
   virtual ~CGravitasResourceLibrary();

   GravitasResource *GetResource(const uint64_t &resource_id)
   {
      return resources_[resource_id];
   }
   virtual bool Upload(const uint64_t &resource_id) = 0;
   virtual bool Unload(const uint64_t &resource_id) = 0;
   bool         IncrementImportRef(const uint64_t &resource_id)
   {
      return ImportRef(resource_id);
   }
   bool DecrementImportRef(const uint64_t &resource_id)
   {
      return ImportUnref(resource_id);
   }

 protected:
   bool AddResource(GravitasResource *resource, uint64_t &resource_id);
   bool ImportRef(const uint64_t &resource_id);
   bool ImportUnref(const uint64_t &resource_id);
   bool UploadRef(const uint64_t &resource_id);
   bool UploadUnref(const uint64_t &resource_id);
   bool RemoveResource(uint64_t &resource_id);
   void RemoveAll();

   CGravitasResourceManager                        *resource_manager_    = nullptr;
   CGravitasDevice                                 *gravitas_device_     = nullptr;
   const GravitasDeviceDispatchTable               *vulkan_dev_dispatch_ = nullptr;
   std::mutex                                       resource_mutex_;
   uint64_t                                         resource_type_ = 0ULL;
   uint64_t                                         next_index_    = 1ULL;
   std::unordered_map<uint64_t, GravitasResource *> resources_;
};
