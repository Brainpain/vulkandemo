//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_resource_library.hxx"

class CGravitasDevice;
class CGravitasResourceManager;

class CGravitasMaterialLibrary : public CGravitasResourceLibrary
{
 public:
   CGravitasMaterialLibrary(CGravitasResourceManager *resource_mgr,
                            CGravitasDevice          *gravitas_device);
   virtual ~CGravitasMaterialLibrary();

   GravitasMaterialResource *GetResource(const uint64_t &resource_id)
   {
      return reinterpret_cast<GravitasMaterialResource *>(resources_[resource_id]);
   }

   bool AddMaterial(const GravitasMaterialResourceCreateInfo *create_info, uint64_t &resource_id);
   bool RemoveMaterial(uint64_t &resource_id);
   bool Upload(const uint64_t &resource_id);
   bool Unload(const uint64_t &resource_id);

 private:
   bool FindMatchingMaterial(const GravitasMaterialResource *resource_struct,
                             uint64_t                       &resource_id);
   bool GenMaterialResourceInfo(const GravitasMaterialResourceCreateInfo *create_info,
                                GravitasMaterialResource                **material_resource);
   void FreeInternals();

   VkDescriptorPool mat_lib_desc_pool_ = VK_NULL_HANDLE;
};
