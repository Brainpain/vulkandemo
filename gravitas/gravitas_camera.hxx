//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>

struct GravitasCameraShaderData
{
   glm::mat4 project_mat;
   glm::mat4 view_mat;
   glm::mat4 view_project_mat;
};

// Camera paths use Quaternions for orientation so we can do smooth blending
// on rotation.
struct GravitasCameraPathNode
{
   float time_from_start = 0.f;
   float fov_half        = glm::radians(70.f);
   bool  repeat_loop     = false; // If true, this will repeat the loop with
                                  // first node and reset the path_time_ to
                                  // 0.f at the time we hit the last node.
                                  // The last node orientation and position
                                  // should match to make it a true loop.
   glm::quat orientation;
   glm::vec3 position;
};

class CGravitasCamera
{
 public:
   CGravitasCamera()
   {
      // Set up a default projection that should just work in case someone never calls
      // LookAt
      glm::mat4 project_mat_ = glm::perspective(glm::radians(70.f), 1.f, 0.01f, 1.f);
   }
   ~CGravitasCamera()
   {}

   void SetPosition(glm::vec3 pos)
   {
      current_.position    = pos;
      moving_through_path_ = false;
   }
   void UpdateWindowDimensions(float width, float height)
   {
      width_  = width;
      height_ = height;
   }
   void LookAt(glm::vec3 target);
   bool Update(float update_time_ms);
   void WriteCameraMatrixInfo(float *write_data);

 private:
   GravitasCameraPathNode              current_;
   float                               path_time_           = 0.f;
   bool                                moving_through_path_ = false;
   std::vector<GravitasCameraPathNode> path_;
   GravitasCameraShaderData            camera_shader_data_;
   float                               width_     = 500.f;
   float                               height_    = 500.f;
   float                               near_dist_ = 0.1f;
   float                               far_dist_  = 100.f;
};
