//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_material_library.hxx"

#include "gravitas_device.hxx"
#include "gravitas_device_dispatch.hxx"
#include "gravitas_log.hxx"
#include "gravitas_resource_manager.hxx"
#include "gravitas_texture.hxx"

#include <iostream>
#include <sstream>

CGravitasMaterialLibrary::CGravitasMaterialLibrary(CGravitasResourceManager *resource_mgr,
                                                   CGravitasDevice          *gravitas_device)
  : CGravitasResourceLibrary(resource_mgr, gravitas_device, GRAV_RESOURCE_MATERIAL)
{

   uint32_t                          max_set_count         = 500;
   std::vector<VkDescriptorPoolSize> descriptor_pool_sizes = {
     {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, max_set_count}};
   VkDescriptorPoolCreateInfo desc_pool_create = {};
   desc_pool_create.sType                      = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
   desc_pool_create.pNext                      = nullptr;
   desc_pool_create.flags                      = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
   desc_pool_create.maxSets                    = max_set_count;
   desc_pool_create.poolSizeCount = static_cast<uint32_t>(descriptor_pool_sizes.size());
   desc_pool_create.pPoolSizes    = descriptor_pool_sizes.data();
   if (VK_SUCCESS !=
       vulkan_dev_dispatch_->CreateDescriptorPool(
         gravitas_device->GetVulkanDevice(), &desc_pool_create, nullptr, &mat_lib_desc_pool_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Material library failed creating descriptor pool!";
      exit(-1);
   }
}

CGravitasMaterialLibrary::~CGravitasMaterialLibrary()
{
   FreeInternals();
   vulkan_dev_dispatch_->DestroyDescriptorPool(
     gravitas_device_->GetVulkanDevice(), mat_lib_desc_pool_, nullptr);
}

bool
CGravitasMaterialLibrary::GenMaterialResourceInfo(
  const GravitasMaterialResourceCreateInfo *create_info,
  GravitasMaterialResource                **material_resource)
{
   bool                      success = false;
   std::ostringstream        outstream_uint64;
   GravitasMaterialResource *resource = new GravitasMaterialResource();
   if (nullptr == resource)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed allocating Material resource for " << create_info->name;
      goto out;
   }

   resource->name           = create_info->name;
   resource->ambient_color  = create_info->ambient_color;
   resource->diffuse_color  = create_info->diffuse_color;
   resource->specular_color = create_info->specular_color;
   resource->emissive_color = create_info->emissive_color;
   for (auto &shdr : create_info->shader_resource_ids)
   {
      GravitasShaderResource *sdr_resource;
      if (!resource_manager_->GetShaderResource(shdr, &sdr_resource))
      {
         outstream_uint64 << shdr;
         CGravitasLog(GRAV_LOG_ERROR) << "Failed finding Material resource " << create_info->name
                                      << " shader resource " << outstream_uint64.str();
         goto out;
      }
      resource->shaders.push_back(sdr_resource);
   }
   for (auto &texture : create_info->texture_resource_ids)
   {
      GravitasTextureResource *texture_resource;
      if (!resource_manager_->GetTextureResource(texture, &texture_resource))
      {
         outstream_uint64 << texture;
         CGravitasLog(GRAV_LOG_ERROR) << "Failed finding Material resource " << create_info->name
                                      << " texture resource " << outstream_uint64.str();
         goto out;
      }
      resource->textures.push_back(texture_resource);
   }
   success = true;

out:
   if (!success)
   {
      delete resource;
   }
   else
   {
      *material_resource = resource;
   }
   return success;
}

bool
CGravitasMaterialLibrary::FindMatchingMaterial(const GravitasMaterialResource *resource_struct,
                                               uint64_t                       &resource_id)
{
   bool found = false;

   for (auto &resource : resources_)
   {
      GravitasMaterialResource *material_resource =
        reinterpret_cast<GravitasMaterialResource *>(resource.second);
      if (material_resource->ambient_color.r != resource_struct->ambient_color.r ||
          material_resource->ambient_color.g != resource_struct->ambient_color.g ||
          material_resource->ambient_color.b != resource_struct->ambient_color.b ||
          material_resource->diffuse_color.r != resource_struct->diffuse_color.r ||
          material_resource->diffuse_color.g != resource_struct->diffuse_color.g ||
          material_resource->diffuse_color.b != resource_struct->diffuse_color.b ||
          material_resource->specular_color.r != resource_struct->specular_color.r ||
          material_resource->specular_color.g != resource_struct->specular_color.g ||
          material_resource->specular_color.b != resource_struct->specular_color.b ||
          material_resource->emissive_color.r != resource_struct->emissive_color.r ||
          material_resource->emissive_color.g != resource_struct->emissive_color.g)
      {
         continue;
      }
      if (resource_struct->shaders.size() != material_resource->shaders.size() ||
          resource_struct->textures.size() != material_resource->textures.size())
      {
         continue;
      }
      bool all_shaders_found = true;
      for (auto &res_shd : material_resource->shaders)
      {
         bool shader_found = false;
         for (auto &in_shd : resource_struct->shaders)
         {
            if (res_shd == in_shd)
            {
               shader_found = true;
               break;
            }
         }
         if (!shader_found)
         {
            all_shaders_found = false;
            break;
         }
      }
      if (!all_shaders_found)
      {
         continue;
      }
      if (resource_struct->textures.size() > 0)
      {
         bool texture_found = false;
         for (auto &res_text : material_resource->textures)
         {
            for (auto &in_text : resource_struct->textures)
            {
               if (res_text == in_text)
               {
                  texture_found = true;
                  break;
               }
            }
         }
         if (!texture_found)
         {
            continue;
         }
      }
      resource_id = material_resource->resource.resource_id;
      return true;
   }

   return false;
}

// Finds if already present and bumps up ref-counts.  If not present, creates and sets ref-count
// to 1 before returning values.
bool
CGravitasMaterialLibrary::AddMaterial(const GravitasMaterialResourceCreateInfo *create_info,
                                      uint64_t                                 &resource_id)
{
   bool                            success                 = false;
   bool                            found                   = false;
   VkDevice                        vk_device               = gravitas_device_->GetVulkanDevice();
   VkDescriptorSetLayoutBinding    desc_set_layout_binding = {};
   VkDescriptorSetLayoutCreateInfo desc_set_layout_create  = {};
   VkSamplerCreateInfo             sampler_create          = {};

   resource_id = 0ULL;

   GravitasMaterialResource *material_resource;
   if (!GenMaterialResourceInfo(create_info, &material_resource))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating Material resource for " << create_info->name;
      goto out;
   }

   found = FindMatchingMaterial(material_resource, resource_id);
   if (!found)
   {
      material_resource->resource.imported_ref_count = 0;
      material_resource->resource.upload_ref_count   = 0;
      if (!AddResource(&material_resource->resource, resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed adding Material resource for material " << material_resource->name;
         goto out;
      }

      if (!create_info->texture_resource_ids.empty())
      {
         // Create a layout for a texture sampler
         desc_set_layout_binding.binding         = 0;
         desc_set_layout_binding.descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
         desc_set_layout_binding.descriptorCount = 1;
         desc_set_layout_binding.stageFlags      = VK_SHADER_STAGE_FRAGMENT_BIT;
         desc_set_layout_create.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
         desc_set_layout_create.pNext        = nullptr;
         desc_set_layout_create.flags        = 0;
         desc_set_layout_create.bindingCount = 1;
         desc_set_layout_create.pBindings    = &desc_set_layout_binding;
         if (VK_SUCCESS !=
             vulkan_dev_dispatch_->CreateDescriptorSetLayout(
               vk_device, &desc_set_layout_create, nullptr, &material_resource->desc_set_layout))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Failed creating texture sampler descriptor set layout for material "
              << material_resource->name;
            return false;
         }

         sampler_create.sType        = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
         sampler_create.pNext        = nullptr;
         sampler_create.magFilter    = VK_FILTER_LINEAR;
         sampler_create.minFilter    = VK_FILTER_LINEAR;
         sampler_create.mipmapMode   = VK_SAMPLER_MIPMAP_MODE_LINEAR;
         sampler_create.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
         sampler_create.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
         sampler_create.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
         if (gravitas_device_->GetFeatures()->samplerAnisotropy)
         {
            sampler_create.anisotropyEnable = VK_TRUE;
            sampler_create.maxAnisotropy    = gravitas_device_->GetLimits()->maxSamplerAnisotropy;
         }
         if (VK_SUCCESS != vulkan_dev_dispatch_->CreateSampler(
                             vk_device, &sampler_create, nullptr, &material_resource->sampler))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Failed creating sampler for material " << material_resource->name;
            goto out;
         }

         VkDescriptorSetAllocateInfo desc_set_alloc = {};
         desc_set_alloc.pNext                       = nullptr;
         desc_set_alloc.sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
         desc_set_alloc.descriptorPool     = mat_lib_desc_pool_;
         desc_set_alloc.descriptorSetCount = 1;
         desc_set_alloc.pSetLayouts        = &material_resource->desc_set_layout;
         if (VK_SUCCESS != vulkan_dev_dispatch_->AllocateDescriptorSets(
                             vk_device, &desc_set_alloc, &material_resource->desc_set))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Failed creating descriptor set for material " << material_resource->name;
            goto out;
         }
      }
   }

   // Bump ref count for each shader and texture
   for (auto &shader : create_info->shader_resource_ids)
   {
      if (!resource_manager_->IncrementImportRef(shader))
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed incrementing shader resource refs for material " << material_resource->name;
         goto out;
      }
   }
   for (auto &texture : create_info->texture_resource_ids)
   {
      if (!resource_manager_->IncrementImportRef(texture))
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed incrementing texture resource refs for material " << material_resource->name;
         goto out;
      }
   }

   ImportRef(resource_id);
   success = true;

out:
   if (!success || found)
   {
      delete material_resource;
   }

   return success;
}

// Drops ref-count, and deletes if 0.
bool
CGravitasMaterialLibrary::RemoveMaterial(uint64_t &resource_id)
{
   VkDevice           vk_dev = gravitas_device_->GetVulkanDevice();
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed releasing Material resource "
                                   << outstream_uint64.str() << " because it does not exist!";
      return false;
   }
   GravitasMaterialResource *material_resource =
     reinterpret_cast<GravitasMaterialResource *>(resources_[resource_id]);

   // Drop ref count for mesh and material
   for (auto &shader : material_resource->shaders)
   {
      if (!resource_manager_->DecrementImportRef(shader->resource.resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed decrementing shader resource refs";
         return false;
      }
   }
   for (auto &texture : material_resource->textures)
   {
      if (!resource_manager_->DecrementImportRef(texture->resource.resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed decrementing texture resource refs";
         return false;
      }
   }

   if (!ImportUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed decrementing import ref for Material resource " << outstream_uint64.str() << "!";
      return false;
   }

   if (material_resource->resource.imported_ref_count == 0)
   {
      if (VK_NULL_HANDLE != material_resource->desc_set)
      {
         vulkan_dev_dispatch_->FreeDescriptorSets(
           vk_dev, mat_lib_desc_pool_, 1, &material_resource->desc_set);
      }
      if (VK_NULL_HANDLE != material_resource->sampler)
      {
         vulkan_dev_dispatch_->DestroySampler(vk_dev, material_resource->sampler, nullptr);
         material_resource->sampler = VK_NULL_HANDLE;
      }
      if (VK_NULL_HANDLE != material_resource->desc_set_layout)
      {
         vulkan_dev_dispatch_->DestroyDescriptorSetLayout(
           vk_dev, material_resource->desc_set_layout, nullptr);
         material_resource->desc_set_layout = VK_NULL_HANDLE;
      }
      delete material_resource;
      resources_.erase(resource_id);
   }
   return true;
}

void
CGravitasMaterialLibrary::FreeInternals()
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   for (auto &resource : resources_)
   {
      GravitasMaterialResource *material_resource =
        reinterpret_cast<GravitasMaterialResource *>(resource.second);
      delete material_resource;
   }
   resources_.clear();
}

bool
CGravitasMaterialLibrary::Upload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed uploading material resource "
                                   << outstream_uint64.str() << " because it does not exist!";
      return false;
   }
   GravitasMaterialResource *mat_resource =
     reinterpret_cast<GravitasMaterialResource *>(resources_[resource_id]);
   if (mat_resource->resource.upload_ref_count == 0)
   {
      for (uint32_t shad = 0; shad < static_cast<uint32_t>(mat_resource->shaders.size()); ++shad)
      {
         resource_manager_->UploadResource(mat_resource->shaders[shad]->resource.resource_id);
      }
      if (!mat_resource->textures.empty())
      {
         VkDevice vk_device = gravitas_device_->GetVulkanDevice();

         std::vector<VkDescriptorImageInfo> image_descriptors;
         std::vector<VkWriteDescriptorSet>  write_desc_sets;
         image_descriptors.resize(mat_resource->textures.size());
         write_desc_sets.resize(mat_resource->textures.size());
         for (uint32_t tex = 0; tex < static_cast<uint32_t>(mat_resource->textures.size()); ++tex)
         {
            resource_manager_->UploadResource(mat_resource->textures[tex]->resource.resource_id);
            image_descriptors[tex].sampler = mat_resource->sampler;
            image_descriptors[tex].imageView =
              mat_resource->textures[tex]->texture->GetVkImageView();
            image_descriptors[tex].imageLayout   = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            write_desc_sets[tex].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            write_desc_sets[tex].pNext           = nullptr;
            write_desc_sets[tex].dstBinding      = 0;
            write_desc_sets[tex].dstSet          = mat_resource->desc_set;
            write_desc_sets[tex].descriptorCount = 1;
            write_desc_sets[tex].descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            write_desc_sets[tex].pImageInfo      = &image_descriptors[tex];
         }
         vulkan_dev_dispatch_->UpdateDescriptorSets(vk_device,
                                                    static_cast<uint32_t>(write_desc_sets.size()),
                                                    write_desc_sets.data(),
                                                    0,
                                                    nullptr);
      }
   }
   return UploadRef(resource_id);
}

bool
CGravitasMaterialLibrary::Unload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (!UploadUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed unloading material resource " << outstream_uint64.str() << "!";
   }
   GravitasMaterialResource *mat_resource =
     reinterpret_cast<GravitasMaterialResource *>(resources_[resource_id]);
   if (mat_resource->resource.upload_ref_count == 0)
   {
      for (uint32_t shad = 0; shad < static_cast<uint32_t>(mat_resource->shaders.size()); ++shad)
      {
         resource_manager_->UnloadResource(mat_resource->shaders[shad]->resource.resource_id);
      }
      for (uint32_t tex = 0; tex < static_cast<uint32_t>(mat_resource->textures.size()); ++tex)
      {
         resource_manager_->UnloadResource(mat_resource->textures[tex]->resource.resource_id);
      }
   }
   return true;
}
