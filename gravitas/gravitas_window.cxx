//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_window.hxx"

#include "gravitas_log.hxx"

#include <SDL_vulkan.h>
#include <iostream>

CGravitasWindow::CGravitasWindow(
  const std::string &window_name, bool fullscreen, uint32_t width, uint32_t height, VkFormat format)
  : width_(width)
  , height_(height)
  , vk_format_(format)
  , is_fullscreen_(fullscreen)
{
   int32_t window_flags = SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN;

   // Initialize the SDL.  If this fails, nothing else can be done.
   if (0 != SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "SDL_Init failed with error: " << SDL_GetError();
      exit(-1);
   }

   if (fullscreen)
   {
      window_flags |= SDL_WINDOW_FULLSCREEN;
   }
   else
   {
      window_flags |= SDL_WINDOW_RESIZABLE;
   }

   sdl_window_ = SDL_CreateWindow(window_name.c_str(),
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  width_,
                                  height_,
                                  window_flags);
   if (nullptr == sdl_window_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "SDL_CreateWindow failed with error: " << SDL_GetError();
      exit(-1);
   }
}

CGravitasWindow::~CGravitasWindow()
{
   if (is_fullscreen_)
   {
      SDL_SetWindowFullscreen(sdl_window_, false);
      SDL_ShowCursor(true);
   }

   if (sdl_window_)
   {
      SDL_DestroyWindow(sdl_window_);
      sdl_window_ = nullptr;
   }
   SDL_Quit();
}

PFN_vkGetInstanceProcAddr
CGravitasWindow::GetInstanceProcAddr()
{
   return (PFN_vkGetInstanceProcAddr)SDL_Vulkan_GetVkGetInstanceProcAddr();
}
uint32_t
CGravitasWindow::GetRequiredExtensions(std::vector<const char *> &extensions)
{
   uint32_t count = 0;
   SDL_Vulkan_GetInstanceExtensions(sdl_window_, &count, nullptr);
   extensions.resize(count);
   SDL_Vulkan_GetInstanceExtensions(sdl_window_, &count, extensions.data());
   return count;
}

bool
CGravitasWindow::CreateWindowSurface(VkInstance instance)
{
   FreeWindowSurface(instance);
   if (SDL_FALSE == SDL_Vulkan_CreateSurface(sdl_window_, instance, &vk_surface_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "CreateWindowSurface: Failed to create SDL_Vulkan_Surface!";
      return false;
   }
   return true;
}

void
CGravitasWindow::FreeWindowSurface(VkInstance instance)
{
   if (VK_NULL_HANDLE != vk_surface_)
   {
      vkDestroySurfaceKHR(instance, vk_surface_, nullptr);
      vk_surface_ = VK_NULL_HANDLE;
   }
}

bool
CGravitasWindow::ResizeWindow(VkInstance instance, uint32_t width, uint32_t height)
{
   width_  = width;
   height_ = height;
   return CreateWindowSurface(instance);
}

void
CGravitasWindow::GetDrawableSize(VkExtent2D &size)
{
   SDL_Vulkan_GetDrawableSize(sdl_window_, (int32_t *)&size.width, (int32_t *)&size.height);
}
