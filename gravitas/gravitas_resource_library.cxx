//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_resource_library.hxx"

#include "gravitas_device.hxx"
#include "gravitas_device_dispatch.hxx"
#include "gravitas_log.hxx"
#include "gravitas_resource_manager.hxx"

#include <sstream>

CGravitasResourceLibrary::CGravitasResourceLibrary(CGravitasResourceManager *resource_mgr,
                                                   CGravitasDevice          *gravitas_device,
                                                   uint64_t                  resource_type)
  : resource_type_(resource_type)
  , resource_manager_(resource_mgr)
  , gravitas_device_(gravitas_device)
  , vulkan_dev_dispatch_(gravitas_device_->GetDispatchTable())
{
   next_index_ = resource_type_ | 1ULL;
}

CGravitasResourceLibrary::~CGravitasResourceLibrary()
{
   RemoveAll();
}

bool
CGravitasResourceLibrary::AddResource(GravitasResource *resource, uint64_t &resource_id)
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   resource_id             = next_index_++;
   resource->resource_id   = resource_id;
   resources_[resource_id] = resource;
   return true;
}

bool
CGravitasResourceLibrary::ImportRef(const uint64_t &resource_id)
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   if (resources_.find(resource_id) == resources_.end())
   {
      std::ostringstream outstream_uint64;
      outstream_uint64 << resource_id;
      CGravitasLog(GRAV_LOG_ERROR)
        << "Attempting to increment import ref count for resource " << outstream_uint64.str()
        << " failed, Resource not found in this library!";
      return false;
   }
   resources_[resource_id]->imported_ref_count++;
   return true;
}

bool
CGravitasResourceLibrary::ImportUnref(const uint64_t &resource_id)
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   std::ostringstream           outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Attempting to decrement import ref count for resource " << outstream_uint64.str()
        << " failed, Resource not found in this library!";
      return false;
   }
   if (resources_[resource_id]->imported_ref_count == 0)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Attempting to decrement import ref count for resource " << outstream_uint64.str()
        << " failed because count is already 0!";
      return false;
   }
   resources_[resource_id]->imported_ref_count--;
   return true;
}

bool
CGravitasResourceLibrary::UploadRef(const uint64_t &resource_id)
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   if (resources_.find(resource_id) == resources_.end())
   {
      std::ostringstream outstream_uint64;
      outstream_uint64 << resource_id;
      CGravitasLog(GRAV_LOG_ERROR)
        << "Attempting to increment upload ref count for resource " << outstream_uint64.str()
        << " failed, Resource not found in this library!";
      return false;
   }
   resources_[resource_id]->upload_ref_count++;
   return true;
}

bool
CGravitasResourceLibrary::UploadUnref(const uint64_t &resource_id)
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   std::ostringstream           outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Attempting to decrement upload ref count for resource " << outstream_uint64.str()
        << " failed, Resource not found in this library!";
      return false;
   }
   if (resources_[resource_id]->upload_ref_count == 0)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Attempting to decrement upload ref count for resource " << outstream_uint64.str()
        << " failed because count is already 0!";
      return false;
   }
   resources_[resource_id]->upload_ref_count--;
   return true;
}

bool
CGravitasResourceLibrary::RemoveResource(uint64_t &resource_id)
{
   bool                         result = false;
   std::unique_lock<std::mutex> lck(resource_mutex_);
   std::ostringstream           outstream_uint64;

   outstream_uint64 << resource_id;

   if ((resource_id & resource_type_) != resource_type_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Removing resource " << outstream_uint64.str()
                                   << " failed, type not valid for this library!";
   }
   else
   {
      if (resources_.find(resource_id) != resources_.end())
      {
         GravitasResource *res = resources_[resource_id];
         if (res->imported_ref_count > 0 || res->upload_ref_count > 0)
         {
            CGravitasLog(GRAV_LOG_ERROR) << "Removing resource " << outstream_uint64.str()
                                         << " which has non-zero references remaining!";
         }
         delete res;
         resources_.erase(resource_id);
         result = true;
      }
      else
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Removing resource " << outstream_uint64.str() << " failed, unknown resource!";
      }
   }
   return result;
}

void
CGravitasResourceLibrary::RemoveAll()
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   for (auto &resource : resources_)
   {
      delete resource.second;
   }
   resources_.clear();
}
