//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_pipeline_library.hxx"
#include "gravitas_resource.hxx"

#include <glm/gtx/transform.hpp>

class CGravitasScene;
class CGravitasDevice;
class CGravitasResourceManager;

struct GravitasShaderRequest
{
   VkShaderStageFlagBits stage;
   std::string           file_name;
};
// Need default shaders
//   Vertex                       basic_scene_object_vert.h
//   Fragment
//       Color + normal only      basic_color_lighting_frag.h
//       Texture                  basic_texture_frag.h
//       Texture + normal only    basic_texture_frag.h
//       Texture * color + normal
struct GravitasRenderObjectInitInfo
{
   std::string                        name;
   glm::vec3                          scale = {1.f, 1.f, 1.f};
   glm::vec3                          color = {1.f, 1.f, 1.f};
   glm::mat4                          base_orientation {1.f};
   std::string                        texture_directory;
   std::string                        texture_file;
   std::string                        shader_directory;
   std::vector<GravitasShaderRequest> shaders;
};

class CGravitasRenderObject
{
 public:
   CGravitasRenderObject(CGravitasDevice *device, CGravitasResourceManager *resource_mgr);
   ~CGravitasRenderObject();

   void SetOrientation(glm::vec3 rots)
   {
      rotations_ = rots;
   }
   void SetPosition(glm::vec3 pos)
   {
      position_ = pos;
   }
   const VkBuffer *VkVertexBuffer()
   {
      return &vertex_buffer_.vk_buffer;
   }

   bool Upload();
   void Unload();
   bool HasBeenUploaded() const
   {
      return uploaded_;
   }
   void SetTransferUploadComplete(bool uploaded);
   bool GetPushConstantInfo(std::vector<VkPushConstantRange> &push_constants);
   bool ResizeEvent();
   void SetResourceId(uint64_t resource_id)
   {
      resource_id_ = resource_id;
   }
   uint64_t GetResourceId() const
   {
      return resource_id_;
   }

   bool CreateQuad(const GravitasRenderObjectInitInfo &init_info);
   bool CreateCube(const GravitasRenderObjectInitInfo &init_info);
   bool CreateSphere(const GravitasRenderObjectInitInfo &init_info);
   bool CreatePyramid(const GravitasRenderObjectInitInfo &init_info);
   bool LoadFromObjFile(const GravitasRenderObjectInitInfo &init_info,
                        const std::string                  &directory,
                        const std::string                  &filename,
                        bool                                center = true);
   bool LoadFromGltfFile(const GravitasRenderObjectInitInfo &init_info,
                         const std::string                  &directory,
                         const std::string                  &filename,
                         bool                                center = true);
   void Draw(VkCommandBuffer &vk_cmd_buf,
             VkBuffer        &last_vertex_buffer,
             VkBuffer        &last_index_buffer,
             VkPipeline      &last_pipeline,
             VkDescriptorSet &last_model_desc_set,
             VkDescriptorSet  scene_desc_set,
             uint32_t         uniform_frame_offset);

 private:
   enum RenderObjectVertexComponent
   {
      RENDER_OBJ_VERTEX_POSITION     = 0x00,
      RENDER_OBJ_VERTEX_NORMAL       = 0x01,
      RENDER_OBJ_VERTEX_TEXCOORD     = 0x02,
      RENDER_OBJ_VERTEX_BONE0        = 0x04,
      RENDER_OBJ_VERTEX_BONE0_WEIGHT = 0x08,
      RENDER_OBJ_VERTEX_TANGENT      = 0x10,
   };

   struct RenderObjectVertex
   {
      glm::vec3 position;
      glm::vec3 normal;
      glm::vec2 texcoord;
      glm::vec4 bone0;
      glm::vec4 bone0weight;
      glm::vec4 tangent;
   };

   struct RenderObjectShader
   {
      VkShaderStageFlagBits stage;
      std::string           name;
      std::string           directory_string;
      std::string           file_string;
      uint64_t              resource_id;
   };
   struct RenderObjectTexture
   {
      std::string name;
      std::string directory_string;
      std::string file_string;
      uint64_t    resource_id;
   };
   struct RenderObjectPushConstants
   {
      glm::mat4 transform_matrix;
   };

   struct RenderObjectMaterial
   {
      std::string                      name;
      float                            specular_comp = 0.f;
      glm::vec3                        ambient_color {1.f};
      glm::vec3                        diffuse_color {1.f};
      glm::vec3                        specular_color {1.f};
      glm::vec3                        emissive_color {0.f};
      float                            refractive_index = 10.f;
      float                            opacity          = 1.f;
      std::string                      texture_directory;
      std::string                      ambient_texture;
      std::string                      diffuse_texture;
      std::string                      specular_texture;
      std::string                      shader_directory;
      uint64_t                         resource_id;
      std::vector<RenderObjectShader>  shaders;
      std::vector<RenderObjectTexture> textures;
      uint64_t                         pipeline_resource_id;
      VkPipeline                       vk_pipeline;
      VkPipelineLayout                 vk_pipeline_layout;
      VkDescriptorSet                  vk_desc_set;
   };
   struct RenderObjectMesh
   {
      uint32_t            vertex_flags    = 0;
      VkPrimitiveTopology vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
      std::vector<VkVertexInputBindingDescription>   vertex_input_binding_descs;
      std::vector<VkVertexInputAttributeDescription> vertex_input_attrib_descs;
      VkPipelineVertexInputStateCreateFlags          pipeline_vertex_input_state_flags;
      std::vector<RenderObjectVertex>                vertices;
      std::vector<uint16_t>                          indices;
      glm::vec3                                      aa_extremes[2];
      uint32_t                                       material_index = 0;
      uint32_t                                       verts_per_face = 3;
   };

   bool PrepareMaterialProperties(RenderObjectMaterial &material);
   bool ReleaseMaterialProperties();
   bool GenVertexInfo(RenderObjectMesh &mesh);
   bool CalculatePipelines(RenderObjectMesh &mesh);
   void CalculateMidpoint(glm::vec3 &midpoint) const;
   void UpdateAxisAlignedBoxExtremes(glm::vec3   aa_extremes[2],
                                     const float x_vert,
                                     const float y_vert,
                                     const float z_vert) const;

   const GravitasDeviceDispatchTable *vulkan_dev_dispatch_   = nullptr;
   CGravitasDevice                   *gravitas_device_       = nullptr;
   CGravitasResourceManager          *gravitas_resource_mgr_ = nullptr;
   std::string                        name_;
   uint64_t                           resource_id_ = 0;
   std::string                        directory_;
   glm::vec3                          position_ {0.f};
   glm::mat4                          base_orientation_ {1.f};
   glm::vec3                          rotations_ {0.f};
   glm::vec3                          axis_aligned_extremes_[2];
   bool                               allocated_ = false;
   bool                               uploaded_  = false;
   GravitasAllocatedBuffer            vertex_buffer_;
   GravitasAllocatedBuffer            vertex_transfer_buffer_;
   bool                               index_buffer_allocated_ = false;
   GravitasAllocatedBuffer            index_buffer_;
   GravitasAllocatedBuffer            index_transfer_buffer_;
   std::vector<RenderObjectMesh>      meshes_;
   std::vector<RenderObjectMaterial>  materials_;
   uint32_t                           total_vertex_count_ = 0;
   uint32_t                           total_index_count_  = 0;
};
