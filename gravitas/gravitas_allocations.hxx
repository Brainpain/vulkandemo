//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include <vk_mem_alloc.h>

struct GravitasAllocatedBuffer
{
   VkBuffer      vk_buffer      = VK_NULL_HANDLE;
   VmaAllocation vma_allocation = nullptr;
};

struct GravitasAllocatedImage
{
   VkImage       vk_image       = VK_NULL_HANDLE;
   VmaAllocation vma_allocation = nullptr;
};

class CGravitasDevice;

bool
CreateBuffer(const CGravitasDevice   *gravitas_device,
             VkDeviceSize             size,
             VkBufferUsageFlags       buffer_usage,
             VmaMemoryUsage           memory_usage,
             VmaAllocationCreateFlags allocation_flags,
             GravitasAllocatedBuffer &allocated_buffer);
bool
CreateImage(const CGravitasDevice  *gravitas_device,
            VkFormat                format,
            uint32_t                width,
            uint32_t                height,
            VkImageUsageFlags       image_usage,
            uint32_t                num_mipmap_levels,
            GravitasAllocatedImage &allocated_image);
