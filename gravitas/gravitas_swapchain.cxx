//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_swapchain.hxx"

#include "gravitas_device.hxx"
#include "gravitas_log.hxx"
#include "gravitas_pipeline_library.hxx"

#include <algorithm>
#include <iostream>

CGravitasSwapchain::CGravitasSwapchain(const IVulkanInstanceOwner *instance_owner,
                                       const VkExtent2D           *window_extents,
                                       const CGravitasDevice      *device,
                                       VkFormat                    format,
                                       uint8_t                     num_images)
  : instance_owner_(instance_owner)
  , window_extents_(*window_extents)
  , gravitas_device_(device)
  , vk_device_(device->GetVulkanDevice())
  , vk_swapchain_format_(format)
  , num_swapchain_images_(num_images)
{

   if (0 == num_images || !InitVkSwapchain())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed initializing swapchain";
      abort();
   }
}

CGravitasSwapchain::~CGravitasSwapchain()
{
   FreeVkSwapchain();
}

bool
CGravitasSwapchain::InitVkSwapchain()
{
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();
   uint32_t                           queue_index = gravitas_device_->GetGraphicsQueueIndex();

   // Setup swapchain, assume a real GPU so don't bother querying the
   // capabilities, just get what we want
   VkSwapchainCreateInfoKHR create_info = {};
   create_info.sType                    = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
   create_info.pNext                    = nullptr;
   create_info.flags                    = 0;
   create_info.surface                  = gravitas_device_->GetCurrentSurface();
   create_info.minImageCount            = num_swapchain_images_;
   create_info.imageFormat              = vk_swapchain_format_;
   create_info.imageColorSpace          = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
   create_info.imageExtent              = window_extents_;
   create_info.imageArrayLayers         = 1;
   create_info.imageUsage               = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
   create_info.imageSharingMode         = VK_SHARING_MODE_EXCLUSIVE;
   create_info.queueFamilyIndexCount    = 1;
   create_info.pQueueFamilyIndices      = &queue_index;
   create_info.preTransform             = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
   create_info.compositeAlpha           = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
#if defined(TRACK_FRAMERATE) || defined(ENABLE_BENCHMARK_MODE)
   create_info.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
#else
   create_info.presentMode = VK_PRESENT_MODE_FIFO_KHR;
#endif
   create_info.clipped      = true;
   create_info.oldSwapchain = VK_NULL_HANDLE;

   VkResult result =
     vulkan_dev_dispatch->CreateSwapchainKHR(vk_device_, &create_info, nullptr, &vk_swapchain_);
   if (VK_SUCCESS != result)
   {
      return false;
   }

   // Get the swap chain images
   uint32_t num_swapchain_imgs = 0;
   vulkan_dev_dispatch->GetSwapchainImagesKHR(
     vk_device_, vk_swapchain_, &num_swapchain_imgs, nullptr);
   vk_swapchain_images_.resize(num_swapchain_imgs);
   vulkan_dev_dispatch->GetSwapchainImagesKHR(
     vk_device_, vk_swapchain_, &num_swapchain_imgs, vk_swapchain_images_.data());

   for (const auto &img : vk_swapchain_images_)
   {
      VkImageViewCreateInfo view_create_info = {};
      view_create_info.sType                 = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
      view_create_info.image                 = img;
      view_create_info.viewType              = VK_IMAGE_VIEW_TYPE_2D;
      view_create_info.format                = vk_swapchain_format_;

      view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
      view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
      view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
      view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

      view_create_info.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
      view_create_info.subresourceRange.baseMipLevel   = 0;
      view_create_info.subresourceRange.levelCount     = 1;
      view_create_info.subresourceRange.baseArrayLayer = 0;
      view_create_info.subresourceRange.layerCount     = 1;

      VkImageView img_view;
      if (VK_SUCCESS !=
          vulkan_dev_dispatch->CreateImageView(vk_device_, &view_create_info, nullptr, &img_view))
      {
         return false;
      }
      vk_swapchain_imageviews_.push_back(img_view);
   }
   return true;
}

void
CGravitasSwapchain::FreeVkSwapchain()
{
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();

   for (const auto &view : vk_swapchain_imageviews_)
   {
      vulkan_dev_dispatch->DestroyImageView(vk_device_, view, nullptr);
   }
   vk_swapchain_imageviews_.clear();
   vk_swapchain_images_.clear();

   if (VK_NULL_HANDLE != vk_swapchain_)
   {
      vulkan_dev_dispatch->DestroySwapchainKHR(vk_device_, vk_swapchain_, nullptr);
   }
}

bool
CGravitasSwapchain::GetNextIndex(VkSemaphore present_sem, uint64_t timeout, uint32_t *index)
{
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();

   VkResult res = vulkan_dev_dispatch->AcquireNextImageKHR(
     vk_device_, vk_swapchain_, timeout, present_sem, nullptr, index);
   if (VK_SUCCESS != res)
   {
      if (VK_ERROR_OUT_OF_DATE_KHR == res || VK_SUBOPTIMAL_KHR == res)
      {
         CGravitasLog(GRAV_LOG_WARNING) << "Swapchain acquire failed because it is out of date!";
      }
      else
      {
         CGravitasLog(GRAV_LOG_WARNING)
           << "Failed to acquire next swapchain image index (" << res << ")!";
      }
      return false;
   }
   return true;
}
