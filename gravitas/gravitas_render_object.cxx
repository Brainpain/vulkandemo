//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_render_object.hxx"

#include "gravitas_device.hxx"
#include "gravitas_log.hxx"
#include "gravitas_pipeline_library.hxx"
#include "gravitas_resource_manager.hxx"
#include "gravitas_scene.hxx"

#include <fstream>
#include <iostream>
#include <limits>
#include <math.h>
#include <tiny_gltf.h>

#define TINYOBJLOADER_IMPLEMENTATION
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <iostream>
#include <tiny_obj_loader.h>

CGravitasRenderObject::CGravitasRenderObject(CGravitasDevice          *device,
                                             CGravitasResourceManager *resource_mgr)
  : gravitas_device_(device)
  , gravitas_resource_mgr_(resource_mgr)
{
   vulkan_dev_dispatch_        = device->GetDispatchTable();
   axis_aligned_extremes_[0].x = -std::numeric_limits<float>::max();
   axis_aligned_extremes_[0].y = -std::numeric_limits<float>::max();
   axis_aligned_extremes_[0].z = -std::numeric_limits<float>::max();
   axis_aligned_extremes_[1].x = std::numeric_limits<float>::max();
   axis_aligned_extremes_[1].y = std::numeric_limits<float>::max();
   axis_aligned_extremes_[1].z = std::numeric_limits<float>::max();
}

CGravitasRenderObject::~CGravitasRenderObject()
{
   Unload();
}

void
CGravitasRenderObject::Draw(VkCommandBuffer &vk_cmd_buf,
                            VkBuffer        &last_vertex_buffer,
                            VkBuffer        &last_index_buffer,
                            VkPipeline      &last_pipeline,
                            VkDescriptorSet &last_model_desc_set,
                            VkDescriptorSet  scene_desc_set,
                            uint32_t         uniform_frame_offset)
{
   VkDeviceSize                       offset              = 0;
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();

   CGravitasRenderObject::RenderObjectPushConstants constants;
   glm::mat4 euler_mat = glm::eulerAngleXYZ(rotations_.x, rotations_.y, rotations_.z);
   constants.transform_matrix =
     euler_mat * base_orientation_ * glm::translate(glm::mat4(1.f), position_);

   assert(vertex_buffer_.vk_buffer != nullptr);
   if (last_vertex_buffer != vertex_buffer_.vk_buffer)
   {
      vulkan_dev_dispatch->CmdBindVertexBuffers(
        vk_cmd_buf, 0, 1, &(vertex_buffer_.vk_buffer), &offset);
      last_vertex_buffer = vertex_buffer_.vk_buffer;
   }
   if (index_buffer_allocated_ > 0)
   {
      assert(index_buffer_.vk_buffer != nullptr);
      if (last_index_buffer != index_buffer_.vk_buffer)
      {
         vulkan_dev_dispatch->CmdBindIndexBuffer(
           vk_cmd_buf, index_buffer_.vk_buffer, 0, VK_INDEX_TYPE_UINT16);
         last_index_buffer = index_buffer_.vk_buffer;
      }
   }
   uint32_t first_start = 0;
   for (const auto &mesh : meshes_)
   {
      auto &material = materials_[mesh.material_index];
      if (last_pipeline != material.vk_pipeline)
      {
         vulkan_dev_dispatch_->CmdBindPipeline(
           vk_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, material.vk_pipeline);
         last_pipeline = material.vk_pipeline;

         vulkan_dev_dispatch_->CmdBindDescriptorSets(vk_cmd_buf,
                                                     VK_PIPELINE_BIND_POINT_GRAPHICS,
                                                     material.vk_pipeline_layout,
                                                     0,
                                                     1,
                                                     &scene_desc_set,
                                                     1,
                                                     &uniform_frame_offset);
      }
      if (VK_NULL_HANDLE != material.vk_desc_set && last_model_desc_set != material.vk_desc_set)
      {
         vulkan_dev_dispatch_->CmdBindDescriptorSets(vk_cmd_buf,
                                                     VK_PIPELINE_BIND_POINT_GRAPHICS,
                                                     material.vk_pipeline_layout,
                                                     1,
                                                     1,
                                                     &material.vk_desc_set,
                                                     0,
                                                     nullptr);
         last_model_desc_set = material.vk_desc_set;
      }

      // Upload the object matrix via push constants
      vulkan_dev_dispatch_->CmdPushConstants(
        vk_cmd_buf,
        material.vk_pipeline_layout,
        VK_SHADER_STAGE_VERTEX_BIT,
        0,
        sizeof(CGravitasRenderObject::RenderObjectPushConstants),
        &constants);

      if (mesh.indices.size() > 0)
      {
         uint32_t count = static_cast<uint32_t>(mesh.indices.size());
         vulkan_dev_dispatch->CmdDrawIndexed(vk_cmd_buf, count, 1, first_start, 0, 0);
         first_start += count;
      }
      else
      {
         uint32_t count = static_cast<uint32_t>(mesh.vertices.size());
         vulkan_dev_dispatch->CmdDraw(vk_cmd_buf, count, 1, first_start, 0);
         first_start += count;
      }
   }
}

bool
CGravitasRenderObject::Upload()
{
   VkDeviceSize vertex_copy_size = total_vertex_count_ * sizeof(RenderObjectVertex);
   VkDeviceSize index_copy_size  = total_index_count_ * sizeof(uint16_t);

   // Create a buffer we will copy from the CPU into that will then be used to transfer
   // the vertex data to device memory on the GPU.
   if (!CreateBuffer(gravitas_device_,
                     vertex_copy_size,
                     VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                     VMA_MEMORY_USAGE_AUTO_PREFER_HOST,
                     VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                     vertex_transfer_buffer_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Mesh failed allocating vertex transfer buffer!";
      return false;
   }

   // Copy the mesh vertex data over to the transfer buffer using the system mappable pointer
   void        *vma_ptr;
   VmaAllocator vma_allocator = gravitas_device_->GetAllocator();
   vmaMapMemory(vma_allocator, vertex_transfer_buffer_.vma_allocation, &vma_ptr);
   uint8_t *cur_ptr = reinterpret_cast<uint8_t *>(vma_ptr);
   for (const auto &shape : meshes_)
   {
      size_t shape_vert_size = shape.vertices.size() * sizeof(RenderObjectVertex);
      memcpy(cur_ptr, shape.vertices.data(), shape_vert_size);
      cur_ptr += shape_vert_size;
   }
   vmaUnmapMemory(vma_allocator, vertex_transfer_buffer_.vma_allocation);

   if (!CreateBuffer(gravitas_device_,
                     vertex_copy_size,
                     VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                     VMA_MEMORY_USAGE_GPU_ONLY,
                     0,
                     vertex_buffer_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Mesh failed allocating vertex buffer!";
      return false;
   }

   // If index data exists, copy it over as well
   if (index_copy_size > 0)
   {
      if (!CreateBuffer(gravitas_device_,
                        index_copy_size,
                        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                        VMA_MEMORY_USAGE_AUTO_PREFER_HOST,
                        VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                        index_transfer_buffer_))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Mesh failed allocating index transfer buffer!";
         return false;
      }

      // Copy the mesh index data over to the transfer buffer using the system mappable pointer
      void        *vma_ptr;
      VmaAllocator vma_allocator = gravitas_device_->GetAllocator();
      vmaMapMemory(vma_allocator, index_transfer_buffer_.vma_allocation, &vma_ptr);
      uint8_t *cur_ptr = reinterpret_cast<uint8_t *>(vma_ptr);
      for (const auto &shape : meshes_)
      {
         size_t shape_index_size = shape.indices.size() * sizeof(uint16_t);
         memcpy(cur_ptr, shape.indices.data(), shape_index_size);
         cur_ptr += shape_index_size;
      }
      vmaUnmapMemory(vma_allocator, index_transfer_buffer_.vma_allocation);

      if (!CreateBuffer(gravitas_device_,
                        index_copy_size,
                        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                        VMA_MEMORY_USAGE_GPU_ONLY,
                        0,
                        index_buffer_))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Mesh failed allocating index buffer!";
         return false;
      }
      index_buffer_allocated_ = true;
   }

   allocated_ = true;
   if (!gravitas_device_->SubmitBufferTransferWork(
         this, vertex_transfer_buffer_.vk_buffer, vertex_buffer_.vk_buffer, vertex_copy_size))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Mesh failed transferring vertex buffer data!";
      return false;
   }
   if (index_copy_size > 0)
   {
      if (!gravitas_device_->SubmitBufferTransferWork(
            this, index_transfer_buffer_.vk_buffer, index_buffer_.vk_buffer, index_copy_size))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Mesh failed transferring index buffer data!";
         return false;
      }
   }
   for (auto &mesh : meshes_)
   {
      auto &material = materials_[mesh.material_index];
      gravitas_resource_mgr_->UploadResource(material.pipeline_resource_id);
      if (!gravitas_resource_mgr_->UploadResource(material.resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed unloading material " << material.name;
         return false;
      }
      for (auto &texture : material.textures)
      {
         if (!gravitas_resource_mgr_->UploadResource(texture.resource_id))
         {
            CGravitasLog(GRAV_LOG_ERROR) << "Failed unloading texture " << texture.name;
            return false;
         }
      }
      for (auto &shader : material.shaders)
      {
         if (!gravitas_resource_mgr_->UploadResource(shader.resource_id))
         {
            CGravitasLog(GRAV_LOG_ERROR) << "Failed unloading shader " << shader.name;
            return false;
         }
      }
   }

   return true;
}

void
CGravitasRenderObject::SetTransferUploadComplete(bool uploaded)
{
   vmaDestroyBuffer(gravitas_device_->GetAllocator(),
                    vertex_transfer_buffer_.vk_buffer,
                    vertex_transfer_buffer_.vma_allocation);
   if (VK_NULL_HANDLE != index_transfer_buffer_.vk_buffer)
   {
      vmaDestroyBuffer(gravitas_device_->GetAllocator(),
                       index_transfer_buffer_.vk_buffer,
                       index_transfer_buffer_.vma_allocation);
      index_transfer_buffer_.vk_buffer = VK_NULL_HANDLE;
   }
   uploaded_ = uploaded;
}

void
CGravitasRenderObject::Unload()
{
   if (allocated_)
   {
      if (!ReleaseMaterialProperties())
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed releasing materials";
      }

      // Decrease ref-count on pipeline, materials, textures and shaders
      if (index_buffer_allocated_)
      {
         vmaDestroyBuffer(
           gravitas_device_->GetAllocator(), index_buffer_.vk_buffer, index_buffer_.vma_allocation);
         index_buffer_allocated_ = false;
      }
      vmaDestroyBuffer(
        gravitas_device_->GetAllocator(), vertex_buffer_.vk_buffer, vertex_buffer_.vma_allocation);
      allocated_ = false;
      uploaded_  = false;
   }
   meshes_.clear();
}

float
half_way(const float &a, const float &b)
{
   return ((a - b) * 0.5f) + b;
}

bool
CGravitasRenderObject::GetPushConstantInfo(std::vector<VkPushConstantRange> &push_constants)
{
   VkPushConstantRange pcrange;
   pcrange.offset     = 0;
   pcrange.size       = sizeof(RenderObjectPushConstants);
   pcrange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
   push_constants.push_back(pcrange);
   return true;
}

void
CGravitasRenderObject::CalculateMidpoint(glm::vec3 &midpoint) const
{
   midpoint.x = half_way(axis_aligned_extremes_[0].x, axis_aligned_extremes_[1].x);
   midpoint.y = half_way(axis_aligned_extremes_[0].y, axis_aligned_extremes_[1].y);
   midpoint.z = half_way(axis_aligned_extremes_[0].z, axis_aligned_extremes_[1].z);
}

bool
CGravitasRenderObject::CalculatePipelines(RenderObjectMesh &mesh)
{
   uint64_t cur_resource_id = 0ULL;
   auto    &material        = materials_[mesh.material_index];

   assert(mesh.vertex_input_binding_descs.size() > 0);
   assert(mesh.vertex_input_attrib_descs.size() > 0);

   // Create pipeline
   VkExtent2D                         win_extents = gravitas_device_->GetWindowDimensions();
   GravitasPipelineResourceCreateInfo pipeline_create_info {};
   pipeline_create_info.resource_info.flag                = GRAV_RESOURCE_PIPELINE;
   pipeline_create_info.material_resource_id              = material.resource_id;
   pipeline_create_info.pipeline_vertex_input_state_flags = mesh.pipeline_vertex_input_state_flags;
   pipeline_create_info.vertex_input_binding_descs        = mesh.vertex_input_binding_descs;
   pipeline_create_info.vertex_input_attrib_descs         = mesh.vertex_input_attrib_descs;
   pipeline_create_info.render_pass = gravitas_resource_mgr_->GetFrameManager()->GetVkRenderPass();
   pipeline_create_info.vertex_topology   = mesh.vertex_topology;
   pipeline_create_info.viewport.x        = 0.f;
   pipeline_create_info.viewport.y        = 0.f;
   pipeline_create_info.viewport.width    = (float)win_extents.width;
   pipeline_create_info.viewport.height   = (float)win_extents.height;
   pipeline_create_info.viewport.minDepth = 0.f;
   pipeline_create_info.viewport.maxDepth = 1.f;
   pipeline_create_info.scissor.offset.x  = 0;
   pipeline_create_info.scissor.offset.y  = 0;
   pipeline_create_info.scissor.extent    = win_extents;

   GetPushConstantInfo(pipeline_create_info.push_constants);
   pipeline_create_info.desc_set_layouts.push_back(
     gravitas_device_->GetGlobalSceneDescriptorLayout());
   if (!gravitas_resource_mgr_->CreateResource(&pipeline_create_info.resource_info,
                                               material.pipeline_resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating pipeline resource!";
      return false;
   }
   GravitasPipelineResource *pipeline_resource;
   if (!gravitas_resource_mgr_->GetPipelineResource(material.pipeline_resource_id,
                                                    &pipeline_resource))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed accessing pipeline resource!";
      return false;
   }
   material.vk_pipeline        = pipeline_resource->vk_pipeline;
   material.vk_pipeline_layout = pipeline_resource->vk_layout;
   return true;
}

bool
CGravitasRenderObject::PrepareMaterialProperties(RenderObjectMaterial &material)
{
   bool     success         = false;
   uint64_t cur_resource_id = 0ULL;

   GravitasShaderResourceCreateInfo shader_create_info = {};
   shader_create_info.resource_info                    = {.flag = GRAV_RESOURCE_SHADER};

   // Reduce the textures down to the unique ones
   for (uint8_t i = 0; i < 3; ++i)
   {
      std::string cur_tex;
      switch (i)
      {
         case 0:
            cur_tex = material.ambient_texture;
            break;
         case 1:
            cur_tex = material.diffuse_texture;
            break;
         case 2:
            cur_tex = material.specular_texture;
            break;
      }
      if (!cur_tex.empty())
      {
         bool found = false;
         for (const auto &texture : material.textures)
         {
            if (texture.file_string == cur_tex)
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            RenderObjectTexture new_tex;
            if (material.texture_directory.empty())
            {
               new_tex.directory_string = directory_;
            }
            else
            {
               new_tex.directory_string = material.texture_directory;
            }
            new_tex.name        = cur_tex;
            new_tex.file_string = cur_tex;
            new_tex.resource_id = 0;
            material.textures.push_back(new_tex);
         }
      }
   }

   // Create the material
   GravitasMaterialResourceCreateInfo material_create_info = {};
   material_create_info.resource_info                      = {.flag = GRAV_RESOURCE_MATERIAL};
   material_create_info.name                               = material.name;
   material_create_info.ambient_color                      = material.ambient_color;
   material_create_info.diffuse_color                      = material.diffuse_color;
   material_create_info.specular_color                     = material.specular_color;
   material_create_info.emissive_color                     = material.emissive_color;

   // Create the appropriate shaders
   // If no shaders provided, figure out what common ones can be used
   if (material.shaders.empty())
   {
      const std::string basic_vertex_shader           = "basic_scene_object_vert";
      const std::string basic_color_fragment_shader   = "basic_color_lighting_frag";
      const std::string basic_texture_fragment_shader = "basic_color_lighting_texture_frag";

      // Load the common vertex shader
      shader_create_info.shader_name  = basic_vertex_shader;
      shader_create_info.shader_stage = VK_SHADER_STAGE_VERTEX_BIT;
      if (!gravitas_resource_mgr_->CreateResource(&shader_create_info.resource_info,
                                                  cur_resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed loading common shader " << basic_vertex_shader;
         return false;
      }
      material_create_info.shader_resource_ids.push_back(cur_resource_id);

      // Store it in this material's shader struct
      RenderObjectShader ro_shader;
      ro_shader.name        = shader_create_info.shader_name;
      ro_shader.stage       = shader_create_info.shader_stage;
      ro_shader.resource_id = cur_resource_id;
      material.shaders.push_back(ro_shader);

      // If no textures, just use a color/lighting fragment shader
      if (material.textures.empty())
      {
         cur_resource_id                 = 0ULL;
         shader_create_info.shader_name  = basic_color_fragment_shader;
         shader_create_info.shader_stage = VK_SHADER_STAGE_FRAGMENT_BIT;
         if (!gravitas_resource_mgr_->CreateResource(&shader_create_info.resource_info,
                                                     cur_resource_id))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Failed loading common shader " << shader_create_info.shader_name;
            return false;
         }
      }
      else
      {
         // TODO: Eventually support multitexturing
         assert(material.textures.size() == 1);

         cur_resource_id                 = 0ULL;
         shader_create_info.shader_name  = basic_texture_fragment_shader;
         shader_create_info.shader_stage = VK_SHADER_STAGE_FRAGMENT_BIT;
         if (!gravitas_resource_mgr_->CreateResource(&shader_create_info.resource_info,
                                                     cur_resource_id))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Failed loading common shader " << shader_create_info.shader_name;
            return false;
         }
      }
      material_create_info.shader_resource_ids.push_back(cur_resource_id);

      // Store it in this material's shader struct
      ro_shader.name        = shader_create_info.shader_name;
      ro_shader.stage       = shader_create_info.shader_stage;
      ro_shader.resource_id = cur_resource_id;
      material.shaders.push_back(ro_shader);
   }
   else
   {
      for (auto &shader : material.shaders)
      {
         // Load the common vertex shader
         shader_create_info.shader_name = shader.name;
         if (!shader.directory_string.empty())
         {
            shader_create_info.directory_string = shader.directory_string;
         }
         else
         {
            shader_create_info.directory_string = directory_;
         }
         shader_create_info.file_string = shader.file_string;
         if (shader_create_info.shader_name.empty())
         {
            if (shader.file_string.find_first_of("\\/") != shader.file_string.npos)
            {
               shader_create_info.shader_name =
                 shader.file_string.substr(shader.file_string.find_last_of("\\/") + 1);
            }
            else
            {
               shader_create_info.shader_name = shader.file_string;
            }
         }
         cur_resource_id                 = 0ULL;
         shader_create_info.shader_stage = shader.stage;
         if (!gravitas_resource_mgr_->CreateResource(&shader_create_info.resource_info,
                                                     cur_resource_id))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Failed loading OBJ file shader " << shader.name << "from "
              << shader.directory_string << "/" << shader.file_string;
            return false;
         }
         material_create_info.shader_resource_ids.push_back(cur_resource_id);

         // Store it in this material's shader struct
         shader.resource_id = cur_resource_id;
      }
   }

   // Create the appropriate textures
   GravitasTextureResourceCreateInfo texture_create_info = {};
   texture_create_info.resource_info                     = {.flag = GRAV_RESOURCE_TEXTURE};
   for (auto &texture : material.textures)
   {
      texture_create_info.name             = texture.name;
      texture_create_info.file_string      = texture.file_string;
      texture_create_info.directory_string = texture.directory_string;
      texture_create_info.generate_mipmaps = true;
      cur_resource_id                      = 0ULL;
      if (!gravitas_resource_mgr_->CreateResource(&texture_create_info.resource_info,
                                                  cur_resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed loading OBJ file texture " << texture_create_info.name;
         return false;
      }
      material_create_info.texture_resource_ids.push_back(cur_resource_id);

      // Store it in this material's texture struct
      texture.resource_id = cur_resource_id;
   }

   cur_resource_id = 0ULL;
   if (!gravitas_resource_mgr_->CreateResource(&material_create_info.resource_info,
                                               cur_resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed loading OBJ file material " << material.name;
      return false;
   }
   material.resource_id = cur_resource_id;

   GravitasMaterialResource *material_resource;
   if (!gravitas_resource_mgr_->GetMaterialResource(material.resource_id, &material_resource))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed accessing material resource!";
      return false;
   }
   material.vk_desc_set = material_resource->desc_set;

   return true;
}

bool
CGravitasRenderObject::ReleaseMaterialProperties()
{
   for (const auto &material : materials_)
   {
      gravitas_resource_mgr_->ReleaseResource(material.pipeline_resource_id);
      for (auto &texture : material.textures)
      {
         if (!gravitas_resource_mgr_->ReleaseResource(texture.resource_id))
         {
            CGravitasLog(GRAV_LOG_ERROR) << "Failed unloading texture " << texture.name;
            return false;
         }
      }
      for (auto &shader : material.shaders)
      {
         if (!gravitas_resource_mgr_->ReleaseResource(shader.resource_id))
         {
            CGravitasLog(GRAV_LOG_ERROR) << "Failed unloading shader " << shader.name;
            return false;
         }
      }
      if (!gravitas_resource_mgr_->ReleaseResource(material.resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed unloading material " << material.name;
         return false;
      }
   }
   materials_.clear();

   return true;
}

bool
CGravitasRenderObject::ResizeEvent()
{
   for (auto &mesh : meshes_)
   {
      if (!CalculatePipelines(mesh))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed calculating pipeline for mesh ";
         return false;
      }
   }
   return true;
}

bool
CGravitasRenderObject::GenVertexInfo(RenderObjectMesh &mesh)
{
   mesh.pipeline_vertex_input_state_flags           = 0;
   VkVertexInputBindingDescription cur_binding_desc = {};
   cur_binding_desc.binding                         = 0;
   cur_binding_desc.stride                          = sizeof(RenderObjectVertex);
   cur_binding_desc.inputRate                       = VK_VERTEX_INPUT_RATE_VERTEX;
   mesh.vertex_input_binding_descs.push_back(cur_binding_desc);

   uint32_t                          location        = 0;
   VkVertexInputAttributeDescription cur_attrib_desc = {};
   cur_attrib_desc.location                          = location++;
   cur_attrib_desc.binding                           = 0;
   cur_attrib_desc.format                            = VK_FORMAT_R32G32B32_SFLOAT;
   cur_attrib_desc.offset                            = offsetof(RenderObjectVertex, position);
   mesh.vertex_input_attrib_descs.push_back(cur_attrib_desc);

   for (uint32_t index = 0; index < 6; ++index)
   {
      VkFormat                    attrib_format;
      uint32_t                    attrib_offset = 0;
      RenderObjectVertexComponent cur_component =
        static_cast<RenderObjectVertexComponent>((mesh.vertex_flags & 1 << index));
      if (cur_component)
      {
         switch (cur_component)
         {
            case RENDER_OBJ_VERTEX_NORMAL:
               attrib_format = VK_FORMAT_R32G32B32_SFLOAT;
               attrib_offset = offsetof(RenderObjectVertex, normal);
               break;
            case RENDER_OBJ_VERTEX_TEXCOORD:
               attrib_format = VK_FORMAT_R32G32_SFLOAT;
               attrib_offset = offsetof(RenderObjectVertex, texcoord);
               break;
            case RENDER_OBJ_VERTEX_BONE0:
               attrib_format = VK_FORMAT_R32G32B32A32_SFLOAT;
               attrib_offset = offsetof(RenderObjectVertex, bone0);
               break;
            case RENDER_OBJ_VERTEX_BONE0_WEIGHT:
               attrib_format = VK_FORMAT_R32G32B32A32_SFLOAT;
               attrib_offset = offsetof(RenderObjectVertex, bone0weight);
               break;
            case RENDER_OBJ_VERTEX_TANGENT:
               attrib_format = VK_FORMAT_R32G32B32A32_SFLOAT;
               attrib_offset = offsetof(RenderObjectVertex, tangent);
               break;
         }
         cur_attrib_desc.location = location++;
         cur_attrib_desc.format   = attrib_format;
         cur_attrib_desc.offset   = attrib_offset;
         mesh.vertex_input_attrib_descs.push_back(cur_attrib_desc);
      }
   }

   if (!CalculatePipelines(mesh))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed calculating pipeline for mesh";
      return false;
   }

   return true;
}

bool
CGravitasRenderObject::CreateQuad(const GravitasRenderObjectInitInfo &init_info)
{
   if (init_info.name.empty())
   {
      name_ = "quad";
   }
   else
   {
      name_ = init_info.name;
   }

   meshes_.resize(1);
   meshes_[0].verts_per_face = 3;

   // Set the max and min boundaries
   meshes_[0].aa_extremes[0].x = 0.5f * init_info.scale.x;
   meshes_[0].aa_extremes[0].y = 0.5f * init_info.scale.y;
   meshes_[0].aa_extremes[0].z = 0.0f;
   meshes_[0].aa_extremes[1].x = -0.5f * init_info.scale.x;
   meshes_[0].aa_extremes[1].y = -0.5f * init_info.scale.y;
   meshes_[0].aa_extremes[1].z = 0.0f;

   glm::vec3 quad[4];
   quad[0]          = glm::vec3(-0.5f * init_info.scale.x, 0.5f * init_info.scale.y, 0.0f);
   quad[1]          = glm::vec3(-0.5f * init_info.scale.x, -0.5f * init_info.scale.y, 0.0f);
   quad[2]          = glm::vec3(0.5f * init_info.scale.x, -0.5f * init_info.scale.y, 0.0f);
   quad[3]          = glm::vec3(0.5f * init_info.scale.x, 0.5f * init_info.scale.y, 0.0f);
   glm::vec3 normal = glm::vec3(0.0f, 0.0f, 1.0f);
   glm::vec2 texture_coords[4];
   texture_coords[0] = glm::vec2(0.f, 0.f);
   texture_coords[1] = glm::vec2(0.f, 1.f);
   texture_coords[2] = glm::vec2(1.f, 1.f);
   texture_coords[3] = glm::vec2(1.f, 0.f);

   // Define the materials
   materials_.resize(1);
   materials_[0].ambient_color     = init_info.color;
   materials_[0].diffuse_color     = init_info.color;
   materials_[0].specular_color    = init_info.color;
   materials_[0].emissive_color    = init_info.color;
   materials_[0].texture_directory = init_info.texture_directory;
   materials_[0].ambient_texture   = init_info.texture_file;
   if (!PrepareMaterialProperties(materials_[0]))
   {
      meshes_.pop_back();
      materials_.pop_back();
      return false;
   }

   RenderObjectVertex new_vertex {};
   new_vertex.position = quad[0];
   new_vertex.normal   = normal;
   new_vertex.texcoord = texture_coords[0];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = quad[1];
   new_vertex.normal   = normal;
   new_vertex.texcoord = texture_coords[1];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = quad[2];
   new_vertex.normal   = normal;
   new_vertex.texcoord = texture_coords[2];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = quad[0];
   new_vertex.normal   = normal;
   new_vertex.texcoord = texture_coords[0];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = quad[2];
   new_vertex.normal   = normal;
   new_vertex.texcoord = texture_coords[2];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = quad[3];
   new_vertex.normal   = normal;
   new_vertex.texcoord = texture_coords[3];
   meshes_[0].vertices.push_back(new_vertex);
   meshes_[0].vertex_flags =
     RENDER_OBJ_VERTEX_POSITION | RENDER_OBJ_VERTEX_NORMAL | RENDER_OBJ_VERTEX_TEXCOORD;
   meshes_[0].vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
   if (!GenVertexInfo(meshes_[0]))
   {
      meshes_.pop_back();
      return false;
   }

   // Define the render object extremes and counts
   axis_aligned_extremes_[0] = meshes_[0].aa_extremes[0];
   axis_aligned_extremes_[1] = meshes_[0].aa_extremes[1];
   total_vertex_count_       = meshes_[0].vertices.size();
   total_index_count_        = meshes_[0].indices.size();
   base_orientation_         = init_info.base_orientation;
   return true;
}

bool
CGravitasRenderObject::CreateCube(const GravitasRenderObjectInitInfo &init_info)
{
   if (init_info.name.empty())
   {
      name_ = "cube";
   }
   else
   {
      name_ = init_info.name;
   }

   meshes_.resize(1);
   meshes_[0].verts_per_face = 3;

   // Set the max and min boundaries
   meshes_[0].aa_extremes[0].x = 0.5f * init_info.scale.x;
   meshes_[0].aa_extremes[0].y = 0.5f * init_info.scale.y;
   meshes_[0].aa_extremes[0].z = 0.5f * init_info.scale.z;
   meshes_[0].aa_extremes[1].x = -0.5f * init_info.scale.x;
   meshes_[0].aa_extremes[1].y = -0.5f * init_info.scale.y;
   meshes_[0].aa_extremes[1].z = -0.5f * init_info.scale.z;

   glm::vec3 cube[8];
   cube[0] =
     glm::vec3(-0.5f * init_info.scale.x, 0.5f * init_info.scale.y, 0.5f * init_info.scale.z);
   cube[1] =
     glm::vec3(-0.5f * init_info.scale.x, -0.5f * init_info.scale.y, 0.5f * init_info.scale.z);
   cube[2] =
     glm::vec3(0.5f * init_info.scale.x, -0.5f * init_info.scale.y, 0.5f * init_info.scale.z);
   cube[3] =
     glm::vec3(0.5f * init_info.scale.x, 0.5f * init_info.scale.y, 0.5f * init_info.scale.z);
   cube[4] =
     glm::vec3(0.5f * init_info.scale.x, 0.5f * init_info.scale.y, -0.5f * init_info.scale.z);
   cube[5] =
     glm::vec3(0.5f * init_info.scale.x, -0.5f * init_info.scale.y, -0.5f * init_info.scale.z);
   cube[6] =
     glm::vec3(-0.5f * init_info.scale.x, -0.5f * init_info.scale.y, -0.5f * init_info.scale.z);
   cube[7] =
     glm::vec3(-0.5f * init_info.scale.x, 0.5f * init_info.scale.y, -0.5f * init_info.scale.z);
   glm::vec3 normals[6];
   normals[0] = glm::vec3(0.0f, 1.0f, 0.0f);  // +y
   normals[1] = glm::vec3(0.0f, 0.0f, 1.0f);  // +z
   normals[2] = glm::vec3(1.0f, 0.0f, 0.0f);  // +x
   normals[3] = glm::vec3(0.0f, 0.0f, -1.0f); // -z
   normals[4] = glm::vec3(-1.0f, 0.0f, 0.0f); // -x
   normals[5] = glm::vec3(0.0f, -1.0f, 0.0f); // -y
   glm::vec2 texture_coords[14];
   texture_coords[0]  = glm::vec2(0.f, 0.333f);
   texture_coords[1]  = glm::vec2(0.f, 0.667f);
   texture_coords[2]  = glm::vec2(0.25f, 0.0f);
   texture_coords[3]  = glm::vec2(0.25f, 0.333f);
   texture_coords[4]  = glm::vec2(0.25f, 0.667f);
   texture_coords[5]  = glm::vec2(0.25f, 1.0f);
   texture_coords[6]  = glm::vec2(0.5f, 0.0f);
   texture_coords[7]  = glm::vec2(0.5f, 0.333f);
   texture_coords[8]  = glm::vec2(0.5f, 0.667f);
   texture_coords[9]  = glm::vec2(0.5f, 1.0f);
   texture_coords[10] = glm::vec2(0.75f, 0.333f);
   texture_coords[11] = glm::vec2(0.75f, 0.667f);
   texture_coords[12] = glm::vec2(1.f, 0.333f);
   texture_coords[13] = glm::vec2(1.f, 0.667f);

   // Define the materials
   materials_.resize(1);
   materials_[0].ambient_color     = init_info.color;
   materials_[0].diffuse_color     = init_info.color;
   materials_[0].specular_color    = init_info.color;
   materials_[0].emissive_color    = init_info.color;
   materials_[0].texture_directory = init_info.texture_directory;
   materials_[0].ambient_texture   = init_info.texture_file;
   if (!PrepareMaterialProperties(materials_[0]))
   {
      meshes_.pop_back();
      materials_.pop_back();
      return false;
   }

   RenderObjectVertex new_vertex {};
   // +y face
   new_vertex.position = cube[0];
   new_vertex.normal   = normals[0];
   new_vertex.texcoord = texture_coords[3];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[3];
   new_vertex.normal   = normals[0];
   new_vertex.texcoord = texture_coords[7];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[4];
   new_vertex.normal   = normals[0];
   new_vertex.texcoord = texture_coords[6];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[0];
   new_vertex.normal   = normals[0];
   new_vertex.texcoord = texture_coords[3];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[4];
   new_vertex.normal   = normals[0];
   new_vertex.texcoord = texture_coords[6];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[7];
   new_vertex.normal   = normals[0];
   new_vertex.texcoord = texture_coords[2];
   meshes_[0].vertices.push_back(new_vertex);
   // +z face
   new_vertex.position = cube[0];
   new_vertex.normal   = normals[1];
   new_vertex.texcoord = texture_coords[3];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[1];
   new_vertex.normal   = normals[1];
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[2];
   new_vertex.normal   = normals[1];
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[0];
   new_vertex.normal   = normals[1];
   new_vertex.texcoord = texture_coords[3];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[2];
   new_vertex.normal   = normals[1];
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[3];
   new_vertex.normal   = normals[1];
   new_vertex.texcoord = texture_coords[7];
   meshes_[0].vertices.push_back(new_vertex);
   // +x face
   new_vertex.position = cube[3];
   new_vertex.normal   = normals[2];
   new_vertex.texcoord = texture_coords[7];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[2];
   new_vertex.normal   = normals[2];
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[5];
   new_vertex.normal   = normals[2];
   new_vertex.texcoord = texture_coords[11];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[3];
   new_vertex.normal   = normals[2];
   new_vertex.texcoord = texture_coords[7];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[5];
   new_vertex.normal   = normals[2];
   new_vertex.texcoord = texture_coords[11];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[4];
   new_vertex.normal   = normals[2];
   new_vertex.texcoord = texture_coords[10];
   meshes_[0].vertices.push_back(new_vertex);
   // -z face
   new_vertex.position = cube[4];
   new_vertex.normal   = normals[3];
   new_vertex.texcoord = texture_coords[10];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[5];
   new_vertex.normal   = normals[3];
   new_vertex.texcoord = texture_coords[11];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[6];
   new_vertex.normal   = normals[3];
   new_vertex.texcoord = texture_coords[13];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[4];
   new_vertex.normal   = normals[3];
   new_vertex.texcoord = texture_coords[10];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[6];
   new_vertex.normal   = normals[3];
   new_vertex.texcoord = texture_coords[13];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[7];
   new_vertex.normal   = normals[3];
   new_vertex.texcoord = texture_coords[12];
   meshes_[0].vertices.push_back(new_vertex);
   // -x face
   new_vertex.position = cube[7];
   new_vertex.normal   = normals[4];
   new_vertex.texcoord = texture_coords[0];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[6];
   new_vertex.normal   = normals[4];
   new_vertex.texcoord = texture_coords[1];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[1];
   new_vertex.normal   = normals[4];
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[7];
   new_vertex.normal   = normals[4];
   new_vertex.texcoord = texture_coords[0];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[1];
   new_vertex.normal   = normals[4];
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[0];
   new_vertex.normal   = normals[4];
   new_vertex.texcoord = texture_coords[3];
   meshes_[0].vertices.push_back(new_vertex);
   // -y face
   new_vertex.position = cube[1];
   new_vertex.normal   = normals[5];
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[6];
   new_vertex.normal   = normals[5];
   new_vertex.texcoord = texture_coords[5];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[5];
   new_vertex.normal   = normals[5];
   new_vertex.texcoord = texture_coords[9];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[1];
   new_vertex.normal   = normals[5];
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[5];
   new_vertex.normal   = normals[5];
   new_vertex.texcoord = texture_coords[9];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = cube[2];
   new_vertex.normal   = normals[5];
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);

   meshes_[0].vertex_flags =
     RENDER_OBJ_VERTEX_POSITION | RENDER_OBJ_VERTEX_NORMAL | RENDER_OBJ_VERTEX_TEXCOORD;
   meshes_[0].vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
   if (!GenVertexInfo(meshes_[0]))
   {
      meshes_.pop_back();
      return false;
   }

   // Define the render object extremes and counts
   axis_aligned_extremes_[0] = meshes_[0].aa_extremes[0];
   axis_aligned_extremes_[1] = meshes_[0].aa_extremes[1];
   total_vertex_count_       = meshes_[0].vertices.size();
   total_index_count_        = meshes_[0].indices.size();
   base_orientation_         = init_info.base_orientation;
   return true;
}

void
CGravitasRenderObject::UpdateAxisAlignedBoxExtremes(glm::vec3   aa_extremes[2],
                                                    const float x_vert,
                                                    const float y_vert,
                                                    const float z_vert) const
{
   if (x_vert > aa_extremes[0].x)
   {
      aa_extremes[0].x = x_vert;
   }
   if (x_vert < aa_extremes[1].x)
   {
      aa_extremes[1].x = x_vert;
   }

   if (y_vert > aa_extremes[0].y)
   {
      aa_extremes[0].y = y_vert;
   }
   if (y_vert < aa_extremes[1].y)
   {
      aa_extremes[1].y = y_vert;
   }

   if (z_vert > aa_extremes[0].z)
   {
      aa_extremes[0].z = z_vert;
   }
   if (z_vert < aa_extremes[1].z)
   {
      aa_extremes[1].z = z_vert;
   }
}

bool
CGravitasRenderObject::CreateSphere(const GravitasRenderObjectInitInfo &init_info)
{
   if (init_info.name.empty())
   {
      name_ = "sphere";
   }
   else
   {
      name_ = init_info.name;
   }
   base_orientation_ = init_info.base_orientation;

   // Generate the sphere vertices
   glm::vec3                       ray = glm::vec3(0.f, 1.f, 0.f);
   RenderObjectVertex              new_vertex {};
   std::vector<RenderObjectVertex> sphere_verts;

   new_vertex.position = ray * glm::vec3(.5f, .5f, .5f) * init_info.scale;
   new_vertex.normal   = ray;
   new_vertex.texcoord = glm::vec2(0.5f, 0.f);
   sphere_verts.push_back(new_vertex);
   const float pi = 3.14159265f;
   for (uint32_t lat = 0; lat < 10; ++lat)
   {
      ray = glm::rotateZ(ray, glm::radians(15.f));
      for (uint32_t lon = 0; lon < 24; ++lon)
      {
         new_vertex.position   = ray * glm::vec3(.5f, .5f, .5f) * init_info.scale;
         new_vertex.normal     = ray;
         new_vertex.texcoord.x = asinf(ray.x) / pi + 0.5f;
         new_vertex.texcoord.y = 1.f - (asinf(ray.y) / pi + 0.5f);
         sphere_verts.push_back(new_vertex);
         ray = glm::rotateY(ray, glm::radians(15.f));
      }
   }
   ray                 = glm::vec3(0.f, -1.f, 0.f);
   new_vertex.position = ray * glm::vec3(.5f, .5f, .5f) * init_info.scale;
   new_vertex.normal   = ray;
   new_vertex.texcoord = glm::vec2(0.5f, 1.f);
   sphere_verts.push_back(new_vertex);

   meshes_.resize(11);
   for (auto &mesh : meshes_)
   {
      mesh.aa_extremes[0].x = -std::numeric_limits<float>::max();
      mesh.aa_extremes[0].y = -std::numeric_limits<float>::max();
      mesh.aa_extremes[0].z = -std::numeric_limits<float>::max();
      mesh.aa_extremes[1].x = std::numeric_limits<float>::max();
      mesh.aa_extremes[1].y = std::numeric_limits<float>::max();
      mesh.aa_extremes[1].z = std::numeric_limits<float>::max();
   }

   // Define the materials (we'll need one for triangle fans and another for triangle strips)
   materials_.resize(2);
   materials_[0].ambient_color     = init_info.color;
   materials_[0].diffuse_color     = init_info.color;
   materials_[0].specular_color    = init_info.color;
   materials_[0].emissive_color    = init_info.color;
   materials_[0].texture_directory = init_info.texture_directory;
   materials_[0].ambient_texture   = init_info.texture_file;
   if (!PrepareMaterialProperties(materials_[0]))
   {
      meshes_.clear();
      materials_.pop_back();
      return false;
   }
   materials_[1].ambient_color     = init_info.color;
   materials_[1].diffuse_color     = init_info.color;
   materials_[1].specular_color    = init_info.color;
   materials_[1].emissive_color    = init_info.color;
   materials_[1].texture_directory = init_info.texture_directory;
   materials_[1].ambient_texture   = init_info.texture_file;
   if (!PrepareMaterialProperties(materials_[1]))
   {
      meshes_.clear();
      materials_.pop_back();
      return false;
   }

   // Top (using triangle fan)
   meshes_[0].verts_per_face = 3;
   meshes_[0].vertex_flags =
     RENDER_OBJ_VERTEX_POSITION | RENDER_OBJ_VERTEX_NORMAL | RENDER_OBJ_VERTEX_TEXCOORD;
   meshes_[0].vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
   for (uint32_t vert_index = 0; vert_index < 25; ++vert_index)
   {
      UpdateAxisAlignedBoxExtremes(meshes_[0].aa_extremes,
                                   sphere_verts[vert_index].position.x,
                                   sphere_verts[vert_index].position.y,
                                   sphere_verts[vert_index].position.z);
      meshes_[0].vertices.push_back(sphere_verts[vert_index]);
   }
   meshes_[0].vertices.push_back(sphere_verts[1]);

   // Bottom (using triangle fan)
   meshes_[1].verts_per_face = 3;
   meshes_[1].vertex_flags =
     RENDER_OBJ_VERTEX_POSITION | RENDER_OBJ_VERTEX_NORMAL | RENDER_OBJ_VERTEX_TEXCOORD;
   meshes_[1].vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
   uint32_t index             = sphere_verts.size() - 1;
   for (uint32_t count = 0; count < 25; ++count)
   {
      UpdateAxisAlignedBoxExtremes(meshes_[1].aa_extremes,
                                   sphere_verts[index].position.x,
                                   sphere_verts[index].position.y,
                                   sphere_verts[index].position.z);
      meshes_[1].vertices.push_back(sphere_verts[index--]);
   }
   meshes_[1].vertices.push_back(sphere_verts[sphere_verts.size() - 2]);

   // Middle rings (tri strips)
   for (uint32_t mesh = 2; mesh < 11; ++mesh)
   {
      meshes_[mesh].verts_per_face = 3;
      meshes_[mesh].vertex_flags =
        RENDER_OBJ_VERTEX_POSITION | RENDER_OBJ_VERTEX_NORMAL | RENDER_OBJ_VERTEX_TEXCOORD;
      meshes_[mesh].vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;

      uint32_t top_start    = (24 * (mesh - 2)) + 1;
      uint32_t bottom_start = (24 * (mesh - 1)) + 1;
      uint32_t top_index    = top_start;
      uint32_t bottom_index = bottom_start;
      for (uint32_t pair_count = 0; pair_count < 24; ++pair_count)
      {
         UpdateAxisAlignedBoxExtremes(meshes_[mesh].aa_extremes,
                                      sphere_verts[top_index].position.x,
                                      sphere_verts[top_index].position.y,
                                      sphere_verts[top_index].position.z);
         UpdateAxisAlignedBoxExtremes(meshes_[mesh].aa_extremes,
                                      sphere_verts[bottom_index].position.x,
                                      sphere_verts[bottom_index].position.y,
                                      sphere_verts[bottom_index].position.z);
         meshes_[mesh].vertices.push_back(sphere_verts[top_index++]);
         meshes_[mesh].vertices.push_back(sphere_verts[bottom_index++]);
      }
      meshes_[mesh].vertices.push_back(sphere_verts[top_start]);
      meshes_[mesh].vertices.push_back(sphere_verts[bottom_start]);
   }

   // Set the global max and min boundaries
   total_vertex_count_ = 0;
   total_index_count_  = 0;
   for (uint32_t mesh = 0; mesh < meshes_.size(); ++mesh)
   {
      if (mesh < 2)
      {
         meshes_[mesh].material_index = 0;
      }
      else
      {
         meshes_[mesh].material_index = 1;
      }
      total_vertex_count_ += meshes_[mesh].vertices.size();
      total_index_count_ += meshes_[mesh].indices.size();
      UpdateAxisAlignedBoxExtremes(axis_aligned_extremes_,
                                   meshes_[mesh].aa_extremes[0].x,
                                   meshes_[mesh].aa_extremes[0].y,
                                   meshes_[mesh].aa_extremes[0].z);
      UpdateAxisAlignedBoxExtremes(axis_aligned_extremes_,
                                   meshes_[mesh].aa_extremes[1].x,
                                   meshes_[mesh].aa_extremes[1].y,
                                   meshes_[mesh].aa_extremes[1].z);
      if (!GenVertexInfo(meshes_[mesh]))
      {
         meshes_.clear();
         materials_.pop_back();
         return false;
      }
   }
   return true;
}

bool
CGravitasRenderObject::CreatePyramid(const GravitasRenderObjectInitInfo &init_info)
{
   if (init_info.name.empty())
   {
      name_ = "pyramid";
   }
   else
   {
      name_ = init_info.name;
   }

   meshes_.resize(1);
   meshes_[0].verts_per_face = 3;

   // Set the max and min boundaries
   meshes_[0].aa_extremes[0].x = 0.5f * init_info.scale.x;
   meshes_[0].aa_extremes[0].y = 0.5f * init_info.scale.y;
   meshes_[0].aa_extremes[0].z = 0.5f * init_info.scale.z;
   meshes_[0].aa_extremes[1].x = -0.5f * init_info.scale.x;
   meshes_[0].aa_extremes[1].y = -0.5f * init_info.scale.y;
   meshes_[0].aa_extremes[1].z = -0.5f * init_info.scale.z;

   const glm::vec3 top = glm::vec3(0.0f * init_info.scale.x, 0.5f * init_info.scale.y, 0.0f);
   const glm::vec3 nx_pz =
     glm::vec3(-0.5f * init_info.scale.x, -0.5f * init_info.scale.y, 0.5f * init_info.scale.z);
   const glm::vec3 px_pz =
     glm::vec3(0.5f * init_info.scale.x, -0.5f * init_info.scale.y, 0.5f * init_info.scale.z);
   const glm::vec3 px_nz =
     glm::vec3(0.5f * init_info.scale.x, -0.5f * init_info.scale.y, -0.5f * init_info.scale.z);
   const glm::vec3 nx_nz =
     glm::vec3(-0.5f * init_info.scale.x, -0.5f * init_info.scale.y, -0.5f * init_info.scale.z);

   glm::vec2 texture_coords[11];
   texture_coords[0]  = glm::vec2(0.f, 0.5f);
   texture_coords[1]  = glm::vec2(0.125f, 0.0f);
   texture_coords[2]  = glm::vec2(0.25f, 0.5f);
   texture_coords[3]  = glm::vec2(0.25f, 1.0f);
   texture_coords[4]  = glm::vec2(0.375f, 0.0f);
   texture_coords[5]  = glm::vec2(0.5f, 0.5f);
   texture_coords[6]  = glm::vec2(0.5f, 1.0f);
   texture_coords[7]  = glm::vec2(0.625f, 0.0f);
   texture_coords[8]  = glm::vec2(0.75f, 0.5f);
   texture_coords[9]  = glm::vec2(0.875f, 0.0f);
   texture_coords[10] = glm::vec2(1.0f, 0.5f);

   // Define the materials
   materials_.resize(1);
   materials_[0].ambient_color     = init_info.color;
   materials_[0].diffuse_color     = init_info.color;
   materials_[0].specular_color    = init_info.color;
   materials_[0].emissive_color    = init_info.color;
   materials_[0].texture_directory = init_info.texture_directory;
   materials_[0].ambient_texture   = init_info.texture_file;
   if (!PrepareMaterialProperties(materials_[0]))
   {
      meshes_.pop_back();
      materials_.pop_back();
      return false;
   }

   RenderObjectVertex new_vertex {};
   // +z face
   new_vertex.position = top;
   new_vertex.normal   = glm::normalize(glm::cross(nx_pz - top, px_pz - top));
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = nx_pz;
   new_vertex.normal   = glm::normalize(glm::cross(px_pz - nx_pz, top - nx_pz));
   new_vertex.texcoord = texture_coords[2];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = px_pz;
   new_vertex.normal   = glm::normalize(glm::cross(top - px_pz, nx_pz - px_pz));
   new_vertex.texcoord = texture_coords[5];
   meshes_[0].vertices.push_back(new_vertex);
   // +x face
   new_vertex.position = top;
   new_vertex.normal   = glm::normalize(glm::cross(px_pz - top, px_nz - top));
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = px_pz;
   new_vertex.normal   = glm::normalize(glm::cross(px_nz - px_pz, top - px_pz));
   new_vertex.texcoord = texture_coords[5];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = px_nz;
   new_vertex.normal   = glm::normalize(glm::cross(top - px_nz, px_pz - px_nz));
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   // -z face
   new_vertex.position = top;
   new_vertex.normal   = glm::normalize(glm::cross(px_nz - top, nx_nz - top));
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = px_nz;
   new_vertex.normal   = glm::normalize(glm::cross(nx_nz - px_nz, top - px_nz));
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = nx_nz;
   new_vertex.normal   = glm::normalize(glm::cross(top - nx_nz, px_nz - nx_nz));
   new_vertex.texcoord = texture_coords[10];
   meshes_[0].vertices.push_back(new_vertex);
   // -x face
   new_vertex.position = top;
   new_vertex.normal   = glm::normalize(glm::cross(nx_nz - top, nx_pz - top));
   new_vertex.texcoord = texture_coords[4];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = nx_nz;
   new_vertex.normal   = glm::normalize(glm::cross(nx_pz - nx_nz, top - nx_nz));
   new_vertex.texcoord = texture_coords[0];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = nx_pz;
   new_vertex.normal   = glm::normalize(glm::cross(top - nx_pz, nx_nz - nx_pz));
   new_vertex.texcoord = texture_coords[2];
   meshes_[0].vertices.push_back(new_vertex);
   // -y face
   new_vertex.position = nx_pz;
   new_vertex.normal   = glm::vec3(0.f, -1.f, 0.f);
   new_vertex.texcoord = texture_coords[2];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = nx_nz;
   new_vertex.normal   = glm::vec3(0.f, -1.f, 0.f);
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = px_nz;
   new_vertex.normal   = glm::vec3(0.f, -1.f, 0.f);
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = nx_pz;
   new_vertex.normal   = glm::vec3(0.f, -1.f, 0.f);
   new_vertex.texcoord = texture_coords[2];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = px_nz;
   new_vertex.normal   = glm::vec3(0.f, -1.f, 0.f);
   new_vertex.texcoord = texture_coords[8];
   meshes_[0].vertices.push_back(new_vertex);
   new_vertex.position = px_pz;
   new_vertex.normal   = glm::vec3(0.f, -1.f, 0.f);
   new_vertex.texcoord = texture_coords[5];
   meshes_[0].vertices.push_back(new_vertex);

   meshes_[0].vertex_flags =
     RENDER_OBJ_VERTEX_POSITION | RENDER_OBJ_VERTEX_NORMAL | RENDER_OBJ_VERTEX_TEXCOORD;
   meshes_[0].vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
   if (!GenVertexInfo(meshes_[0]))
   {
      meshes_.pop_back();
      return false;
   }

   // Define the render object extremes and counts
   axis_aligned_extremes_[0] = meshes_[0].aa_extremes[0];
   axis_aligned_extremes_[1] = meshes_[0].aa_extremes[1];
   total_vertex_count_       = meshes_[0].vertices.size();
   total_index_count_        = meshes_[0].indices.size();
   base_orientation_         = init_info.base_orientation;
   return true;
}

bool
CGravitasRenderObject::LoadFromObjFile(const GravitasRenderObjectInitInfo &init_info,
                                       const std::string                  &directory,
                                       const std::string                  &filename,
                                       bool                                center)
{
   tinyobj::attrib_t                obj_attrib {};
   std::vector<tinyobj::shape_t>    obj_shapes {};
   std::vector<tinyobj::material_t> obj_materials {};
   std::string                      warning_str;
   std::string                      error_str;
   glm::vec3                        midpoint(0.f);

   directory_ = directory;
   if (init_info.name.empty())
   {
      name_ = filename;
   }
   else
   {
      name_ = init_info.name;
   }

   // Load the OBJ file using the tiny obj loader
   std::string full_name = directory + "/" + filename;
   if (!tinyobj::LoadObj(&obj_attrib,
                         &obj_shapes,
                         &obj_materials,
                         &warning_str,
                         &error_str,
                         full_name.c_str(),
                         directory.c_str()))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed loading OBJ file " << full_name;
      if (!error_str.empty())
      {
         CGravitasLog(GRAV_LOG_ERROR) << error_str;
      }
      if (!warning_str.empty())
      {
         CGravitasLog(GRAV_LOG_WARNING) << warning_str;
         return false;
      }
      return false;
   }
   if (!error_str.empty())
   {
      CGravitasLog(GRAV_LOG_ERROR) << error_str;
   }
   if (!warning_str.empty())
   {
      CGravitasLog(GRAV_LOG_WARNING) << warning_str;
      return false;
   }

   uint32_t cur_mat = 0;
   materials_.resize(obj_materials.size());
   for (const auto &obj_mat : obj_materials)
   {
      materials_[cur_mat].name = obj_mat.name;
      materials_[cur_mat].ambient_color =
        glm::vec3(obj_mat.ambient[0], obj_mat.ambient[1], obj_mat.ambient[2]);
      materials_[cur_mat].diffuse_color =
        glm::vec3(obj_mat.diffuse[0], obj_mat.diffuse[1], obj_mat.diffuse[2]);
      materials_[cur_mat].specular_color =
        glm::vec3(obj_mat.specular[0], obj_mat.specular[1], obj_mat.specular[2]);
      materials_[cur_mat].emissive_color =
        glm::vec3(obj_mat.emission[0], obj_mat.emission[1], obj_mat.emission[2]);
      materials_[cur_mat].specular_comp    = obj_mat.shininess;
      materials_[cur_mat].refractive_index = obj_mat.ior;
      materials_[cur_mat].opacity          = obj_mat.dissolve;
      if (!init_info.texture_directory.empty())
      {
         materials_[cur_mat].texture_directory = init_info.texture_directory;
      }
      else
      {
         materials_[cur_mat].texture_directory = directory_;
      }
      materials_[cur_mat].ambient_texture  = obj_mat.ambient_texname;
      materials_[cur_mat].diffuse_texture  = obj_mat.diffuse_texname;
      materials_[cur_mat].specular_texture = obj_mat.specular_texname;
      if (!PrepareMaterialProperties(materials_[cur_mat]))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed calculating material " << cur_mat << " properties";
         return false;
      }
      cur_mat++;
   }

   // NOTE: We have to figure out the actual number of meshes because some meshes from OBJ will
   //       switch materials mid-mesh and we don't want that.
   uint32_t actual_num_meshes = 0;
   for (const auto &obj_shape : obj_shapes)
   {
      uint32_t cur_mesh_mat = obj_shape.mesh.material_ids[0];
      for (size_t face = 0; face < obj_shape.mesh.num_face_vertices.size(); ++face)
      {
         if (obj_shape.mesh.material_ids[face] != cur_mesh_mat)
         {
            actual_num_meshes++;
            cur_mesh_mat = obj_shape.mesh.material_ids[face];
         }
      }
      actual_num_meshes++;
   }

   // Init the bounding box extremes
   meshes_.resize(actual_num_meshes);
   for (auto &mesh : meshes_)
   {
      mesh.aa_extremes[0].x = -std::numeric_limits<float>::max();
      mesh.aa_extremes[0].y = -std::numeric_limits<float>::max();
      mesh.aa_extremes[0].z = -std::numeric_limits<float>::max();
      mesh.aa_extremes[1].x = std::numeric_limits<float>::max();
      mesh.aa_extremes[1].y = std::numeric_limits<float>::max();
      mesh.aa_extremes[1].z = std::numeric_limits<float>::max();
   }

   uint32_t cur_mesh = 0;
   for (const auto &obj_shape : obj_shapes)
   {
      uint32_t cur_mesh_mat = obj_shape.mesh.material_ids[0];
      size_t   index_offset = 0;
      for (size_t face = 0; face < obj_shape.mesh.num_face_vertices.size(); ++face)
      {
         uint32_t verts_per_face = static_cast<uint32_t>(obj_shape.mesh.num_face_vertices[face]);
         uint32_t material_index = obj_shape.mesh.material_ids[face];

         if (material_index != cur_mesh_mat)
         {
            cur_mesh++;
            cur_mesh_mat = material_index;
         }

         meshes_[cur_mesh].vertex_flags =
           RENDER_OBJ_VERTEX_POSITION | RENDER_OBJ_VERTEX_NORMAL | RENDER_OBJ_VERTEX_TEXCOORD;
         meshes_[cur_mesh].vertex_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
         meshes_[cur_mesh].verts_per_face  = verts_per_face;
         meshes_[cur_mesh].material_index  = material_index;
         for (size_t vert = 0; vert < verts_per_face; ++vert)
         {
            tinyobj::index_t idx = obj_shape.mesh.indices[index_offset + vert];

            // Get Vertex info to compare to extremes
            const size_t vertex_index = verts_per_face * idx.vertex_index;
            float        x_vert       = obj_attrib.vertices[vertex_index] * init_info.scale.x;
            float        y_vert       = obj_attrib.vertices[vertex_index + 1] * init_info.scale.y;
            float        z_vert       = obj_attrib.vertices[vertex_index + 2] * init_info.scale.z;
            UpdateAxisAlignedBoxExtremes(meshes_[cur_mesh].aa_extremes, x_vert, y_vert, z_vert);
         }
         index_offset += verts_per_face;
      }
      UpdateAxisAlignedBoxExtremes(axis_aligned_extremes_,
                                   meshes_[cur_mesh].aa_extremes[0].x,
                                   meshes_[cur_mesh].aa_extremes[0].y,
                                   meshes_[cur_mesh].aa_extremes[0].z);
      UpdateAxisAlignedBoxExtremes(axis_aligned_extremes_,
                                   meshes_[cur_mesh].aa_extremes[1].x,
                                   meshes_[cur_mesh].aa_extremes[1].y,
                                   meshes_[cur_mesh].aa_extremes[1].z);
      cur_mesh++;
      assert(cur_mesh <= actual_num_meshes);
   }

   // Calculate the center and adjust the extremes
   if (center)
   {
      CalculateMidpoint(midpoint);
      axis_aligned_extremes_[0].x -= midpoint.x;
      axis_aligned_extremes_[0].y -= midpoint.y;
      axis_aligned_extremes_[0].z -= midpoint.z;
      axis_aligned_extremes_[1].x -= midpoint.x;
      axis_aligned_extremes_[1].y -= midpoint.y;
      axis_aligned_extremes_[1].z -= midpoint.z;
      for (auto &mesh : meshes_)
      {
         mesh.aa_extremes[0].x -= midpoint.x;
         mesh.aa_extremes[1].x -= midpoint.x;
         mesh.aa_extremes[0].y -= midpoint.y;
         mesh.aa_extremes[1].y -= midpoint.y;
         mesh.aa_extremes[0].z -= midpoint.z;
         mesh.aa_extremes[1].z -= midpoint.z;
      }
   }

   cur_mesh = 0;
   for (const auto &obj_shape : obj_shapes)
   {
      uint32_t cur_mesh_mat = obj_shape.mesh.material_ids[0];
      size_t   index_offset = 0;
      for (size_t face = 0; face < obj_shape.mesh.num_face_vertices.size(); ++face)
      {
         if (obj_shape.mesh.material_ids[face] != cur_mesh_mat)
         {
            total_vertex_count_ += meshes_[cur_mesh].vertices.size();
            if (!GenVertexInfo(meshes_[cur_mesh]))
            {
               CGravitasLog(GRAV_LOG_ERROR) << "Failed setting vertex info for " << filename;
               return false;
            }
            cur_mesh++;
            assert(cur_mesh <= actual_num_meshes);
            cur_mesh_mat = obj_shape.mesh.material_ids[face];
         }

         for (size_t vert = 0; vert < meshes_[cur_mesh].verts_per_face; ++vert)
         {
            tinyobj::index_t idx = obj_shape.mesh.indices[index_offset + vert];

            // Get Vertex info
            RenderObjectVertex new_vertex {};
            const size_t       vertex_index   = meshes_[cur_mesh].verts_per_face * idx.vertex_index;
            const size_t       normal_index   = 3 * idx.normal_index;
            const size_t       texcoord_index = 2 * idx.texcoord_index;
            new_vertex.position.x = obj_attrib.vertices[vertex_index] * init_info.scale.x;
            new_vertex.position.y = obj_attrib.vertices[vertex_index + 1] * init_info.scale.y;
            new_vertex.position.z = obj_attrib.vertices[vertex_index + 2] * init_info.scale.z;

            if (center)
            {
               new_vertex.position.x -= midpoint.x;
               new_vertex.position.y -= midpoint.y;
               new_vertex.position.z -= midpoint.z;
            }
            new_vertex.normal.x = obj_attrib.normals[normal_index];
            new_vertex.normal.y = obj_attrib.normals[normal_index + 1];
            new_vertex.normal.z = obj_attrib.normals[normal_index + 2];
            if (init_info.scale.x != init_info.scale.y || init_info.scale.x != init_info.scale.z)
            {
               // TODO: Adjust normal by scale
            }
            if (idx.texcoord_index < 0)
            {
               new_vertex.texcoord.x = 0.f;
               new_vertex.texcoord.y = 0.f;
            }
            else
            {
               new_vertex.texcoord.x = obj_attrib.texcoords[texcoord_index];
               // Need to flip the y component for Vulkan
               new_vertex.texcoord.y = 1.f - obj_attrib.texcoords[texcoord_index + 1];
            }

            meshes_[cur_mesh].vertices.push_back(new_vertex);
         }
         index_offset += meshes_[cur_mesh].verts_per_face;
      }
      total_vertex_count_ += meshes_[cur_mesh].vertices.size();
      if (!GenVertexInfo(meshes_[cur_mesh]))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed setting vertex info for " << filename;
         return false;
      }
      cur_mesh++;
      assert(cur_mesh <= actual_num_meshes);
   }
   base_orientation_ = init_info.base_orientation;
   return true;
}

bool
CGravitasRenderObject::LoadFromGltfFile(const GravitasRenderObjectInitInfo &init_info,
                                        const std::string                  &directory,
                                        const std::string                  &filename,
                                        bool                                center)
{
   directory_ = directory;
   if (init_info.name.empty())
   {
      name_ = filename;
   }
   else
   {
      name_ = init_info.name;
   }
   return false;
}