//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_app.hxx"

#include "gravitas_device.hxx"
#include "gravitas_log.hxx"
#include "gravitas_resource_manager.hxx"
#include "gravitas_scene.hxx"

#include <algorithm>
#include <glm/gtx/transform.hpp>
#if defined(TRACK_FRAMERATE) || defined(ENABLE_BENCHMARK_MODE)
#include <iomanip>
#endif
#include <iostream>

const char std_validation_layer_name[] = "VK_LAYER_KHRONOS_validation";

#ifndef VK_MAKE_API_VERSION
#define VK_MAKE_API_VERSION(a, b, c, d) VK_MAKE_VERSION(b, c, d)
#endif
#ifndef VK_API_VERSION_MAJOR
#define VK_API_VERSION_MAJOR(api_version) VK_VERSION_MAJOR(api_version)
#endif
#ifndef VK_API_VERSION_MINOR
#define VK_API_VERSION_MINOR(api_version) VK_VERSION_MINOR(api_version)
#endif

VKAPI_ATTR VkBool32 VKAPI_CALL
debug_messenger_callback(VkDebugUtilsMessageSeverityFlagBitsEXT      messageSeverity,
                         VkDebugUtilsMessageTypeFlagsEXT             messageType,
                         const VkDebugUtilsMessengerCallbackDataEXT *callbackData,
                         void                                       *userData)
{
   GravitasLogLevel level = GRAV_LOG_INFO;

   switch (messageSeverity)
   {
      case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
         level = GRAV_LOG_ERROR;
         break;
      case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
         level = GRAV_LOG_WARNING;
         break;
      case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
         level = GRAV_LOG_INFO;
         break;
      case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
         level = GRAV_LOG_VERBOSE;
         break;
   }

   CGravitasLog(level) << callbackData->pMessage;

   // Don't bail out, but keep going.
   return false;
}

// Global log config
GavitasLogConfig gravitas_log_config;

CGravitasApp::CGravitasApp(const std::string         &application_name,
                           const uint32_t            &vulkan_major_version,
                           const uint32_t            &vulkan_minor_version,
                           std::vector<const char *> &required_instance_extensions,
                           bool                       debug,
                           bool                       fullscreen,
                           uint32_t                   width,
                           uint32_t                   height,
                           VkFormat                   render_target_format,
                           CGravitasScene            *start_scene)
  : name_(application_name)
  , debug_(debug)
  , render_target_format_(render_target_format)
{
   //   std::ofstream log_output;
   //   log_output.open("gravitas_log.txt", std::ofstream::out | std::ofstream::app);
   gravitas_log_config.output_time   = true;
   gravitas_log_config.min_log_level = GRAV_LOG_INFO;
   //   gravitas_log_config.file_stream   = &log_output;

   VkResult                           res;
   uint32_t                           loader_instance_api_version = 0;
   uint16_t                           instance_major_version      = 1;
   uint16_t                           instance_minor_version      = 0;
   uint32_t                           extension_count             = 0;
   std::vector<const char *>          sdl_required_extensions;
   std::vector<VkExtensionProperties> available_extensions;

   VkApplicationInfo    app_info             = {};
   VkInstanceCreateInfo instance_create_info = {};

   gravitas_window_ =
     new CGravitasWindow(application_name, fullscreen, width, height, render_target_format);
   if (nullptr == gravitas_window_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "CGravitasWindow creation failed!/n";
      exit(-1);
   }

   pfn_vkGetInstanceProcAddr_ = gravitas_window_->GetInstanceProcAddr();
   if (nullptr == pfn_vkGetInstanceProcAddr_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Window->getInstanceProcAddr returned nullptr!/n";
      exit(-1);
   }

   uint32_t desired_version = VK_MAKE_API_VERSION(0, // Variant value (always 0 for standard Vulkan)
                                                  vulkan_major_version,
                                                  vulkan_minor_version,
                                                  0 // Patch not important
   );

   // Only need to check if it's not version 1.0
   if (vulkan_major_version != 1 || vulkan_minor_version != 0)
   {
      // Determine if the new instance version command is available
      PFN_vkEnumerateInstanceVersion p_enum_inst_version =
        (PFN_vkEnumerateInstanceVersion)pfn_vkGetInstanceProcAddr_(VK_NULL_HANDLE,
                                                                   "vkEnumerateInstanceVersion");
      if (nullptr == p_enum_inst_version)
      {
         CGravitasLog(GRAV_LOG_ERROR) << "vkEnumerateInstanceVersion not found!/n";
         exit(-1);
      }

      // It exists, so let's see what we can get
      res = p_enum_inst_version(&loader_instance_api_version);
      if (VK_SUCCESS != res)
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "vkEnumerateInstanceVersion call failed: " << (uint32_t)res << "!/n";
         exit(-1);
      }

      // Translate the version into major/minor for easier comparison
      instance_major_version = VK_API_VERSION_MAJOR(loader_instance_api_version);
      instance_minor_version = VK_API_VERSION_MINOR(loader_instance_api_version);

      // Check current version against what we want to run
      if (instance_major_version != vulkan_major_version ||
          instance_minor_version < vulkan_minor_version)
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "vkEnumerateInstanceVersion returned instance version " << instance_major_version
           << "." << instance_minor_version << ", but wanted version " << vulkan_major_version
           << "." << vulkan_minor_version << "!/n";
         exit(-1);
      }
   }
   api_version_.major = static_cast<uint8_t>(vulkan_major_version);
   api_version_.minor = static_cast<uint8_t>(vulkan_minor_version);
   api_version_.patch = 0;

   // Get the required extensions for SDL windowing
   extension_count = gravitas_window_->GetRequiredExtensions(sdl_required_extensions);

   // Add on any other extensions required
   required_instance_extensions.insert(required_instance_extensions.end(),
                                       sdl_required_extensions.begin(),
                                       sdl_required_extensions.end());
   if (debug_)
   {
      required_instance_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
   }

   // Remove duplicates
   sort(required_instance_extensions.begin(), required_instance_extensions.end());
   required_instance_extensions.erase(
     unique(required_instance_extensions.begin(), required_instance_extensions.end()),
     required_instance_extensions.end());

   // Now, figure out what extensions are actually available
   vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, nullptr);
   available_extensions.resize(extension_count);
   vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, available_extensions.data());
   for (auto req_ext : required_instance_extensions)
   {
      bool found = false;
      for (auto avail_ext : available_extensions)
      {
         if (!strcmp(avail_ext.extensionName, req_ext))
         {
            found = true;
            break;
         }
      }
      if (!found)
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Required Instance Extension \"" << req_ext << "\" not found!/n";
         exit(-1);
      }
   }

   if (debug_)
   {
      bool     validation_found = false;
      uint32_t layer_count      = 0;
      if (VK_SUCCESS != vkEnumerateInstanceLayerProperties(&layer_count, nullptr))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "vkEnumerateInstanceLayerProperties failed on layer count "
                                         "query!/n";
         exit(-1);
      }

      if (layer_count > 0)
      {
         std::vector<VkLayerProperties> instance_layers;
         instance_layers.resize(layer_count);
         if (VK_SUCCESS != vkEnumerateInstanceLayerProperties(&layer_count, instance_layers.data()))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "vkEnumerateInstanceLayerProperties failed on layer query!/n";
            exit(-1);
         }

         bool found = false;
         for (auto layer : instance_layers)
         {
            if (!strcmp(layer.layerName, std_validation_layer_name))
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Required Layer \"" << std_validation_layer_name << "\" not found!/n";
            exit(-1);
         }
         enabled_layers_.push_back(std_validation_layer_name);
      }
   }

   // Initialize the VkApplicationInfo structure with the version
   // of the API we're intending to use
   app_info.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
   app_info.pNext              = nullptr;
   app_info.pApplicationName   = name_.c_str();
   app_info.applicationVersion = 1;
   app_info.pEngineName        = name_.c_str();
   app_info.engineVersion      = 1;
   app_info.apiVersion         = desired_version;

   // Initialize the VkInstanceCreateInfo structure
   instance_create_info.sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
   instance_create_info.pNext                   = nullptr;
   instance_create_info.flags                   = 0;
   instance_create_info.pApplicationInfo        = &app_info;
   instance_create_info.enabledExtensionCount   = required_instance_extensions.size();
   instance_create_info.ppEnabledExtensionNames = required_instance_extensions.data();
   instance_create_info.enabledLayerCount       = enabled_layers_.size();
   instance_create_info.ppEnabledLayerNames     = enabled_layers_.data();

   CGravitasLog(GRAV_LOG_INFO) << "Calling vkCreateInstance with:"
                               << "   API Version " << vulkan_major_version << "."
                               << vulkan_minor_version << ""
                               << "   " << (uint32_t)required_instance_extensions.size()
                               << " Extensions:";
   for (auto ext : required_instance_extensions)
   {
      CGravitasLog(GRAV_LOG_INFO) << "      " << ext << "";
   }
   CGravitasLog(GRAV_LOG_INFO) << "   " << (uint32_t)enabled_layers_.size() << " Layers:";
   for (auto lyr : enabled_layers_)
   {
      CGravitasLog(GRAV_LOG_INFO) << "      " << lyr << "";
   }

   // Attempt to create the instance
   if (VK_SUCCESS != vkCreateInstance(&instance_create_info, NULL, &vk_instance_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Unknown error creating " << vulkan_major_version << "."
                                   << vulkan_minor_version << " Instance";
   }

   if (debug_)
   {
      pfnCreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
        vk_instance_, "vkCreateDebugUtilsMessengerEXT");
      pfnDestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
        vk_instance_, "vkDestroyDebugUtilsMessengerEXT");

      VkDebugUtilsMessengerCreateInfoEXT dbg_messenger_create_info;
      dbg_messenger_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
      dbg_messenger_create_info.pNext = NULL;
      dbg_messenger_create_info.flags = 0;
      dbg_messenger_create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                                  VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
      dbg_messenger_create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                              VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                              VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
      dbg_messenger_create_info.pfnUserCallback = debug_messenger_callback;
      dbg_messenger_create_info.pUserData       = NULL;
      VkResult result                           = pfnCreateDebugUtilsMessengerEXT(
        vk_instance_, &dbg_messenger_create_info, NULL, &debug_utils_messenger_);
   }

   CGravitasLog(GRAV_LOG_INFO) << "Successfully created Vulkan " << vulkan_major_version << "."
                               << vulkan_minor_version << " Instance";

   cur_scene_ = start_scene;
}

CGravitasApp::~CGravitasApp()
{
   if (nullptr != cur_scene_)
   {
      cur_scene_->StopScene(0.f);
      delete cur_scene_;
      cur_scene_ = nullptr;
   }
   if (nullptr != next_scene_ && transit_scenes_)
   {
      next_scene_->StopScene(0.f);
      delete next_scene_;
      next_scene_ = nullptr;
   }

   if (gravitas_resource_manager_)
   {
      delete gravitas_resource_manager_;
      gravitas_resource_manager_ = nullptr;
   }

   // Need to free the window surface before freeing the device.
   gravitas_window_->FreeWindowSurface(vk_instance_);

   if (VK_NULL_HANDLE != scene_desc_set_layout_)
   {
      vkDestroyDescriptorSetLayout(
        gravitas_device_->GetVulkanDevice(), scene_desc_set_layout_, nullptr);
      scene_desc_set_layout_ = VK_NULL_HANDLE;
   }

   if (gravitas_device_)
   {
      delete gravitas_device_;
      gravitas_device_ = nullptr;
   }

   if (gravitas_window_)
   {
      delete gravitas_window_;
      gravitas_window_ = nullptr;
   }

   if (VK_NULL_HANDLE != debug_utils_messenger_)
   {
      pfnDestroyDebugUtilsMessengerEXT(vk_instance_, debug_utils_messenger_, nullptr);
      debug_utils_messenger_ = VK_NULL_HANDLE;
   }

   // Release the instance now
   if (VK_NULL_HANDLE != vk_instance_)
   {
      vkDestroyInstance(vk_instance_, nullptr);
      vk_instance_ = VK_NULL_HANDLE;
   }
}

struct SDeviceInfo
{
   VkPhysicalDevice      physical_device;
   VkPhysicalDeviceType  device_type;
   std::string           device_name;
   GravitasVersionFields api_version;
   uint64_t              device_local_size;
};

bool
CGravitasApp::SelectRendererDevice(std::vector<const char *> &required_device_extensions)
{
   std::vector<VkPhysicalDevice>        available_devices;
   std::vector<VkQueueFamilyProperties> queue_family_props;
   std::vector<SDeviceInfo>             device_info_vec;
   float                                queue_priorities[1] = {1.0};
   VkDeviceQueueCreateInfo              queue_create_info;
   VkDeviceCreateInfo                   device_create_info;
   VkPhysicalDeviceProperties           device_props                       = {};
   uint32_t                             device_major_version               = 1;
   uint32_t                             device_minor_version               = 0;
   uint32_t                             num_devices                        = 0;
   uint32_t                             possible_devices                   = 0;
   int32_t                              selected_device                    = -1;
   int32_t                              possible_secondary_selected_device = -1;
   uint32_t                             queue_count                        = 0;
   bool                                 ret_val                            = false;
   VkDescriptorSetLayoutBinding         desc_set_layout_binding            = {};
   VkDescriptorSetLayoutCreateInfo      desc_set_layout_create             = {};

   // First query the available physical devices
   VkResult res = vkEnumeratePhysicalDevices(vk_instance_, &num_devices, nullptr);
   if (VK_SUCCESS != res || 0 == num_devices)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to query vkEnumeratePhysicalDevices count!";
      goto single_exit;
   }
   available_devices.resize(num_devices);
   device_info_vec.resize(num_devices);
   res = vkEnumeratePhysicalDevices(vk_instance_, &num_devices, available_devices.data());
   if (VK_SUCCESS != res || 0 == num_devices)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to query vkEnumeratePhysicalDevices devices!";
      goto single_exit;
   }

   // Next look for devices that meet the API requirement
   for (uint32_t index = 0; index < available_devices.size(); ++index)
   {
      device_props = {};
      vkGetPhysicalDeviceProperties(available_devices[index], &device_props);
      device_major_version = VK_API_VERSION_MAJOR(device_props.apiVersion);
      device_minor_version = VK_API_VERSION_MINOR(device_props.apiVersion);

      if (device_major_version == api_version_.major && api_version_.minor <= device_minor_version)
      {
         device_info_vec[possible_devices].physical_device   = available_devices[index];
         device_info_vec[possible_devices].device_name       = device_props.deviceName;
         device_info_vec[possible_devices].device_type       = device_props.deviceType;
         device_info_vec[possible_devices].api_version.major = device_major_version;
         device_info_vec[possible_devices].api_version.minor = device_minor_version;
         device_info_vec[possible_devices].device_local_size = 0;

         VkPhysicalDeviceMemoryProperties memory_props = {};
         vkGetPhysicalDeviceMemoryProperties(available_devices[index], &memory_props);
         for (uint32_t heap = 0; heap < memory_props.memoryHeapCount; ++heap)
         {
            if (memory_props.memoryHeaps[heap].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT)
            {
               device_info_vec[possible_devices].device_local_size +=
                 memory_props.memoryHeaps[heap].size;
            }
         }

         // Look for a discrete device with the larges amount of local memory
         if (device_info_vec[possible_devices].device_type == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
         {
            if (selected_device >= 0)
            {
               if (device_info_vec[possible_devices].device_local_size >
                   device_info_vec[selected_device].device_local_size)
               {
                  selected_device = possible_devices;
               }
            }
            else
            {
               selected_device = possible_devices;
            }
         }
         else if (selected_device < 0 && device_info_vec[possible_devices].device_type ==
                                           VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
         {
            if (possible_secondary_selected_device >= 0)
            {
               if (device_info_vec[possible_devices].device_local_size >
                   device_info_vec[possible_secondary_selected_device].device_local_size)
               {
                  possible_secondary_selected_device = possible_devices;
               }
            }
            else
            {
               possible_secondary_selected_device = possible_devices;
            }
         }
         possible_devices++;
      }
   }
   if (0 == possible_devices)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed to find an available physical device that supports Vulkan " << api_version_.major
        << "." << api_version_.minor << "!";
      goto single_exit;
   }

   // Found nothing desired, so stick with first one
   if (0 > selected_device)
   {
      if (possible_secondary_selected_device > 0)
      {
         selected_device = possible_secondary_selected_device;
      }
      else
      {
         selected_device = 0;
      }
   }

   gravitas_device_ = new CGravitasDevice(this,
                                          this,
                                          this,
                                          api_version_,
                                          device_info_vec[selected_device].physical_device,
                                          enabled_layers_,
                                          required_device_extensions);
   if (nullptr == gravitas_device_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create CGravitasDevice!";
      goto single_exit;
   }
   CGravitasLog(GRAV_LOG_INFO) << "Created Vulkan " << (uint32_t)api_version_.major << "."
                               << (uint32_t)api_version_.minor << " logical device using \""
                               << device_info_vec[selected_device].device_name
                               << "\" (Supports up to Vulkan "
                               << (uint32_t)device_info_vec[selected_device].api_version.major
                               << "."
                               << (uint32_t)device_info_vec[selected_device].api_version.minor
                               << ")";

   gravitas_resource_manager_ = new CGravitasResourceManager(gravitas_device_, this, this);
   if (nullptr == gravitas_resource_manager_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create CGravitasResourceManager!";
      goto single_exit;
   }

   // Create a layout for scene uniform buffers
   desc_set_layout_binding.binding         = 0;
   desc_set_layout_binding.descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
   desc_set_layout_binding.descriptorCount = 1;
   desc_set_layout_binding.stageFlags  = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
   desc_set_layout_create.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
   desc_set_layout_create.pNext        = nullptr;
   desc_set_layout_create.flags        = 0;
   desc_set_layout_create.bindingCount = 1;
   desc_set_layout_create.pBindings    = &desc_set_layout_binding;
   if (VK_SUCCESS != vkCreateDescriptorSetLayout(gravitas_device_->GetVulkanDevice(),
                                                 &desc_set_layout_create,
                                                 nullptr,
                                                 &scene_desc_set_layout_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating global scene descriptor set layout!";
      return false;
   }

   cur_scene_->InitScene(this, "startup");
   cur_scene_->UploadScene();
   cur_scene_->StartScene();

   ret_val = true;

single_exit:
   return ret_val;
}

void
CGravitasApp::TogglePause(bool pause)
{
   paused_ = pause;
   if (nullptr != cur_scene_)
   {
      cur_scene_->SetPaused(pause);
   }
   if (nullptr != next_scene_ && transit_scenes_)
   {
      next_scene_->SetPaused(pause);
   }
}

// Poll available SDL events : The return is whether or not application should exit
bool
CGravitasApp::PollSDLEvents()
{
   bool exit = false;

   // Poll the SDL events
   SDL_Event event;
   while (SDL_PollEvent(&event))
   {
      switch (event.type)
      {
         case SDL_QUIT:
#ifdef DEBUG_WINDOWS_EVENTS
            CGravitasLog(GRAV_LOG_INFO) << "SDL Quit";
#endif
            exit = true;
            break;
         case SDL_WINDOWEVENT:
            switch (event.window.event)
            {
               case SDL_WINDOWEVENT_EXPOSED:
#ifdef DEBUG_WINDOWS_EVENTS
                  CGravitasLog(GRAV_LOG_INFO) << "SDL Window exposed";
#endif
                  if (resize_pending_)
                  {
                     if (!ResizeWindow(resize_width_, resize_height_))
                     {
                        CGravitasLog(GRAV_LOG_ERROR) << "Window resize failed!";
                        exit = true;
                     }
                     resize_pending_ = false;
                  }
                  TogglePause(false);
                  break;

               case SDL_WINDOWEVENT_HIDDEN:
#ifdef DEBUG_WINDOWS_EVENTS
                  CGravitasLog(GRAV_LOG_INFO) << "SDL Window hidden";
#endif
                  TogglePause(true);
                  break;

               case SDL_WINDOWEVENT_RESIZED:
#ifdef DEBUG_WINDOWS_EVENTS
                  CGravitasLog(GRAV_LOG_INFO) << "Window resizing to " << event.window.data1
                                              << "w, " << event.window.data2 << "h";
#endif
                  resize_pending_ = true;
                  resize_width_   = event.window.data1;
                  resize_height_  = event.window.data2;
                  TogglePause(true);
                  break;
               case SDL_WINDOWEVENT_SIZE_CHANGED:
#ifdef DEBUG_WINDOWS_EVENTS
                  CGravitasLog(GRAV_LOG_INFO) << "Window size changed to " << event.window.data1
                                              << "w, " << event.window.data2 << "h";
#endif
                  resize_pending_ = true;
                  resize_width_   = event.window.data1;
                  resize_height_  = event.window.data2;
                  TogglePause(true);
                  break;

               case SDL_WINDOWEVENT_MINIMIZED:
                  TogglePause(true);
#ifdef DEBUG_WINDOWS_EVENTS
                  CGravitasLog(GRAV_LOG_INFO) << "Window minimized";
#endif
                  break;

               case SDL_WINDOWEVENT_MAXIMIZED:
                  TogglePause(false);
#ifdef DEBUG_WINDOWS_EVENTS
                  CGravitasLog(GRAV_LOG_INFO) << "Window maximized";
#endif
                  break;

#if 0 // Unused
               case SDL_WINDOWEVENT_ENTER:
                  break;

               case SDL_WINDOWEVENT_LEAVE:
                  break;

               default:
                  ///CGravitasLog(GRAV_LOG_INFO) << "Window message " << (uint32_t)event.window.event << "";
                  break;
#endif
            }
            break;
         case SDL_KEYDOWN:
#ifdef DEBUG_WINDOWS_EVENTS
            CGravitasLog(GRAV_LOG_INFO) << "SDL Keydown";
#endif
            switch (event.key.keysym.sym)
            {
               case SDLK_ESCAPE:
#ifdef DEBUG_WINDOWS_EVENTS
                  CGravitasLog(GRAV_LOG_INFO) << "SDLK_ESCAPE";
#endif
                  exit = true;
                  break;
            }
            break;
         case SDL_MOUSEBUTTONUP:
#ifdef DEBUG_WINDOWS_EVENTS
            CGravitasLog(GRAV_LOG_INFO) << "SDL Mouse up";
#endif
            break;
         case SDL_MOUSEBUTTONDOWN:
#ifdef DEBUG_WINDOWS_EVENTS
            CGravitasLog(GRAV_LOG_INFO) << "SDL Mouse down";
#endif
            break;
         case SDL_MOUSEMOTION:
#ifdef DEBUG_WINDOWS_EVENTS
            // CGravitasLog(GRAV_LOG_INFO) << "SDL MouseMotion";
#endif
            break;
         default:
            break;
      }
   }
   return exit;
}

double
DiffTimeMillisecs(timespec start, timespec end)
{
   double nanosecs = 0.f;
   if ((end.tv_nsec - start.tv_nsec) < 0)
   {
      nanosecs = (float)(((int64_t)end.tv_sec - (int64_t)start.tv_sec - 1LL) * 1000000000LL);
      nanosecs += (float)(1000000000LL + (int64_t)end.tv_nsec - (int64_t)start.tv_nsec);
   }
   else
   {
      nanosecs = (float)(((int64_t)end.tv_sec - (int64_t)start.tv_sec) * 1000000000LL);
      nanosecs += (float)(end.tv_nsec - start.tv_nsec);
   }
   return nanosecs /= 1000000.;
}

bool
CGravitasApp::UpdateAndDrawActiveScenes(SFrameData *frame_data, float time_diff_ms)
{
   bool success = true;
   if (nullptr != cur_scene_)
   {
      bool worked = cur_scene_->Update(frame_data, time_diff_ms);
      if (!worked)
      {
         success = false;
      }
   }
   if (nullptr != next_scene_ && transit_scenes_)
   {
      bool worked = next_scene_->Update(frame_data, time_diff_ms);
      if (!worked)
      {
         success = false;
      }
   }
   if (nullptr != cur_scene_)
   {
      bool worked = cur_scene_->Draw(frame_data);
      if (!worked)
      {
         success = false;
      }
   }
   if (nullptr != next_scene_ && transit_scenes_)
   {
      bool worked = next_scene_->Draw(frame_data);
      if (!worked)
      {
         success = false;
      }
   }
   return success;
}

bool
CGravitasApp::RenderLoop()
{
   bool     exit = false;
   timespec cur;
   timespec last;
   clock_gettime(CLOCK_REALTIME, &last);
#if defined(TRACK_FRAMERATE) || defined(ENABLE_BENCHMARK_MODE)
   uint64_t count              = 0;
   timespec last_frame_capture = last;
#endif

   while (!exit)
   {
      exit = PollSDLEvents();
      if (!exit && !paused_)
      {
         SFrameData *frame_data = gravitas_resource_manager_->GetFrameManager()->StartFrame();
         if (nullptr != frame_data)
         {
            clock_gettime(CLOCK_REALTIME, &cur);
            float diff = DiffTimeMillisecs(last, cur);
            last       = cur;
            if (nullptr != next_scene_ && transit_scenes_)
            {
               transition_time_remaining_ -= diff;
               if (transition_time_remaining_ < 0.f)
               {
                  DestroyOldScene(cur_scene_);
                  cur_scene_                 = next_scene_;
                  next_scene_                = nullptr;
                  transition_time_remaining_ = 0.f;
                  transit_scenes_            = false;
               }
            }
            UpdateAndDrawActiveScenes(frame_data, diff);
            gravitas_resource_manager_->GetFrameManager()->EndFrame();
         }
#if defined(TRACK_FRAMERATE) || defined(ENABLE_BENCHMARK_MODE)
         if ((++count % 5000) == 0)
         {
            float total_diff_sec = DiffTimeMillisecs(last_frame_capture, cur) / 1000.;
            CGravitasLog(GRAV_LOG_INFO)
              << std::setprecision(10) << "FPS = " << ((float)count / total_diff_sec) << " (over "
              << total_diff_sec << " secs)\n";
            last_frame_capture = cur;
            count              = 0;
         }
#endif
      }
   }
   return true;
}

bool
CGravitasApp::ResizeWindow(uint32_t width, uint32_t height)
{
   if (!gravitas_resource_manager_->ResetBeforeResize())
   {
      return false;
   }
   if (!gravitas_window_->ResizeWindow(vk_instance_, width, height))
   {
      return false;
   }
   if (!gravitas_resource_manager_->RestoreAfterResize())
   {
      return false;
   }
   if (nullptr != cur_scene_)
   {
      cur_scene_->ResizeEvent(width, height);
   }
   if (nullptr != next_scene_ && transit_scenes_)
   {
      next_scene_->ResizeEvent(width, height);
   }
   return true;
}

bool
CGravitasApp::ActivateNextScene(CGravitasScene *new_scene, float transition_time_sec)
{
   if (nullptr == new_scene)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Attempting to activate NULL next scene!";
      return false;
   }
   if (nullptr != next_scene_)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Attempting to activate another next scene while one is still transitioning!!";
      return false;
   }
   if (transition_time_sec > 0.f)
   {
      next_scene_ = new_scene;
      cur_scene_->StopScene(transition_time_sec);
      next_scene_->StartScene(transition_time_sec);
      transit_scenes_            = true;
      transition_time_remaining_ = transition_time_sec;
   }
   else
   {
      DestroyOldScene(cur_scene_);
      cur_scene_ = new_scene;
   }
   return true;
}

void
CGravitasApp::DestroyOldScene(CGravitasScene *scene)
{
   // TODO: Put this in a thread
   CGravitasScene *destroy_scene = scene;
   destroy_scene->UnloadScene();
   delete destroy_scene;
}
