//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_app.hxx"
#include "gravitas_device_dispatch.hxx"
#include "vk_mem_alloc.h"

#include <mutex>
#include <string>
#include <thread>
#include <vector>

class CGravitasSwapchain;
class CGravitasRenderObject;
class CGravitasTexture;

class CGravitasDevice
{
 public:
   struct SubmittedCmdBufferInfo
   {
      CGravitasRenderObject *render_object          = nullptr;
      CGravitasTexture      *texture                = nullptr;
      VkCommandPool          cmd_pool               = VK_NULL_HANDLE;
      VkCommandBuffer        cmd_buf                = VK_NULL_HANDLE;
      VkFence                fence                  = VK_NULL_HANDLE;
      VkSemaphore            semaphore              = VK_NULL_HANDLE;
      bool                   shader_read_transition = false;
   };

   CGravitasDevice(IVulkanInstanceOwner      *instance_owner,
                   IGravitasWindowOwner      *window_owner,
                   IGravitasSceneOwner       *scene_owner,
                   GravitasVersionFields      api_version,
                   VkPhysicalDevice           physical_device,
                   std::vector<const char *>  enabled_layers,
                   std::vector<const char *> &required_extensions);
   ~CGravitasDevice();

   inline VkPhysicalDevice GetVulkanPhysicalDevice() const
   {
      return vk_physical_device_;
   }
   inline VkDevice GetVulkanDevice() const
   {
      return vk_device_;
   }
   inline uint32_t GetGraphicsQueueIndex() const
   {
      return graphics_queue_index_;
   }
   inline VkQueue GetGraphicsQueue() const
   {
      return vk_graphics_queue_;
   }
   inline bool HasSeperateTransferQueue() const
   {
      return separate_transfer_queue_;
   }
   inline uint32_t GetTransferQueueIndex() const
   {
      return transfer_queue_index_;
   }
   inline VkQueue GetTransferQueue() const
   {
      return vk_transfer_queue_;
   }
   inline VkSurfaceKHR GetCurrentSurface() const
   {
      return window_owner_->GetWindow()->GetVkSurface();
   }
   inline VkExtent2D GetWindowDimensions() const
   {
      VkExtent2D ext = {.width  = window_owner_->GetWindow()->GetWidth(),
                        .height = window_owner_->GetWindow()->GetHeight()};
      return ext;
   }
   inline bool WaitingForTransfer()
   {
      return !sub_transfer_info_.empty();
   }
   inline bool HasPendingTransferSemaphores()
   {
      return !pending_transfer_sems_.empty();
   }
   inline void AddPendingTransferSemaphore(VkSemaphore sem)
   {
      pending_transfer_sems_.push_back(sem);
   }
   VkDescriptorSetLayout GetGlobalSceneDescriptorLayout()
   {
      return scene_owner_->GetGlobalSceneDescriptorLayout();
   }
   void CopyPendingTransferSemaphore(std::vector<VkSemaphore> &sem);
   bool SubmitBufferTransferWork(CGravitasRenderObject *render_object,
                                 VkBuffer               src_buffer,
                                 VkBuffer               dst_buffer,
                                 VkDeviceSize           size);
   bool SubmitImageTransferWork(CGravitasTexture *texture,
                                VkBuffer          src_buffer,
                                VkImage           dst_image,
                                VkDeviceSize      size,
                                bool              gen_mipmaps = false);
   bool CleanupAfterCompletedTransfer();
   bool TransitionImageToShaderReadable(CGravitasTexture *texture);
   bool ExitTransferThread()
   {
      return exit_transfer_thread_;
   }

   PFN_vkVoidFunction GetVulkanDeviceProcAddr(const char *name) const
   {
      return pfn_vkGetDeviceProcAddr_(vk_device_, name);
   }
   const GravitasDeviceDispatchTable *GetDispatchTable() const
   {
      return &vk_dispatch_table_;
   }
   VmaAllocator GetAllocator() const
   {
      return vma_allocator_;
   }
   const VkPhysicalDeviceLimits *GetLimits()
   {
      return &device_limits_;
   }
   const VkPhysicalDeviceFeatures *GetFeatures()
   {
      return &device_features_;
   }

 private:
   IVulkanInstanceOwner               *instance_owner_          = nullptr;
   IGravitasWindowOwner               *window_owner_            = nullptr;
   IGravitasSceneOwner                *scene_owner_             = nullptr;
   std::thread                        *transfer_delete_thread_  = nullptr;
   bool                                exit_transfer_thread_    = false;
   GravitasVersionFields               api_version_             = {};
   VkPhysicalDevice                    vk_physical_device_      = VK_NULL_HANDLE;
   VkPhysicalDeviceLimits              device_limits_           = {};
   VkPhysicalDeviceFeatures            device_features_         = {};
   VkDevice                            vk_device_               = VK_NULL_HANDLE;
   uint32_t                            graphics_queue_index_    = 0;
   VkQueue                             vk_graphics_queue_       = VK_NULL_HANDLE;
   VkCommandPool                       vk_graphics_cmd_pool_    = VK_NULL_HANDLE;
   bool                                separate_transfer_queue_ = false;
   uint32_t                            transfer_queue_index_    = 0;
   VkQueue                             vk_transfer_queue_       = VK_NULL_HANDLE;
   VkCommandPool                       vk_transfer_cmd_pool_    = VK_NULL_HANDLE;
   std::mutex                          submitted_transfer_mutex_;
   std::vector<SubmittedCmdBufferInfo> sub_transfer_info_       = {};
   std::vector<VkSemaphore>            pending_transfer_sems_   = {};
   PFN_vkGetDeviceProcAddr             pfn_vkGetDeviceProcAddr_ = nullptr;
   GravitasDeviceDispatchTable         vk_dispatch_table_       = {};
   VmaAllocator                        vma_allocator_           = {};
};
