//
// Copyright (c) 2015-2022 The Khronos Group Inc.
// Copyright (c) 2015-2022 Valve Corporation
// Copyright (c) 2015-2022 LunarG, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: Courtney Goeltzenleuchter <courtney@LunarG.com>
// Author: Jon Ashburn <jon@lunarg.com>
// Author: Mark Lobodzinski <mark@lunarg.com>
//

// NOTE: This file is modified from the Vulkan Loader's [found in GitHub:
//       https://github.com/KhronosGroup/Vulkan-Loader]
//       vk_dispatch_table_helper.h file and therefore retains the above
//       copyright since it originated from there.  The main difference
//       from the original file other than the name is:
//         - the removal of the stub functions (and supporting "if" statemtents)
//         - the instance dispatch functionality
//         - the renaming of the VkLayerDispatchTable to GravitasDeviceDispatchTable
//         - the renaming of the init function to GravitasInitDeviceDispatchTable

#include "gravitas_device_dispatch.hxx"

#include <cstring>

void
GravitasInitDeviceDispatchTable(VkDevice                     device,
                                GravitasDeviceDispatchTable *table,
                                PFN_vkGetDeviceProcAddr      gdpa)
{
   std::memset(table, 0, sizeof(*table));

   // Device function pointers
   table->GetDeviceProcAddr = gdpa;
   table->DestroyDevice     = (PFN_vkDestroyDevice)gdpa(device, "vkDestroyDevice");
   table->GetDeviceQueue    = (PFN_vkGetDeviceQueue)gdpa(device, "vkGetDeviceQueue");
   table->QueueSubmit       = (PFN_vkQueueSubmit)gdpa(device, "vkQueueSubmit");
   table->QueueWaitIdle     = (PFN_vkQueueWaitIdle)gdpa(device, "vkQueueWaitIdle");
   table->DeviceWaitIdle    = (PFN_vkDeviceWaitIdle)gdpa(device, "vkDeviceWaitIdle");
   table->AllocateMemory    = (PFN_vkAllocateMemory)gdpa(device, "vkAllocateMemory");
   table->FreeMemory        = (PFN_vkFreeMemory)gdpa(device, "vkFreeMemory");
   table->MapMemory         = (PFN_vkMapMemory)gdpa(device, "vkMapMemory");
   table->UnmapMemory       = (PFN_vkUnmapMemory)gdpa(device, "vkUnmapMemory");
   table->FlushMappedMemoryRanges =
     (PFN_vkFlushMappedMemoryRanges)gdpa(device, "vkFlushMappedMemoryRanges");
   table->InvalidateMappedMemoryRanges =
     (PFN_vkInvalidateMappedMemoryRanges)gdpa(device, "vkInvalidateMappedMemoryRanges");
   table->GetDeviceMemoryCommitment =
     (PFN_vkGetDeviceMemoryCommitment)gdpa(device, "vkGetDeviceMemoryCommitment");
   table->BindBufferMemory = (PFN_vkBindBufferMemory)gdpa(device, "vkBindBufferMemory");
   table->BindImageMemory  = (PFN_vkBindImageMemory)gdpa(device, "vkBindImageMemory");
   table->GetBufferMemoryRequirements =
     (PFN_vkGetBufferMemoryRequirements)gdpa(device, "vkGetBufferMemoryRequirements");
   table->GetImageMemoryRequirements =
     (PFN_vkGetImageMemoryRequirements)gdpa(device, "vkGetImageMemoryRequirements");
   table->GetImageSparseMemoryRequirements =
     (PFN_vkGetImageSparseMemoryRequirements)gdpa(device, "vkGetImageSparseMemoryRequirements");
   table->QueueBindSparse     = (PFN_vkQueueBindSparse)gdpa(device, "vkQueueBindSparse");
   table->CreateFence         = (PFN_vkCreateFence)gdpa(device, "vkCreateFence");
   table->DestroyFence        = (PFN_vkDestroyFence)gdpa(device, "vkDestroyFence");
   table->ResetFences         = (PFN_vkResetFences)gdpa(device, "vkResetFences");
   table->GetFenceStatus      = (PFN_vkGetFenceStatus)gdpa(device, "vkGetFenceStatus");
   table->WaitForFences       = (PFN_vkWaitForFences)gdpa(device, "vkWaitForFences");
   table->CreateSemaphore     = (PFN_vkCreateSemaphore)gdpa(device, "vkCreateSemaphore");
   table->DestroySemaphore    = (PFN_vkDestroySemaphore)gdpa(device, "vkDestroySemaphore");
   table->CreateEvent         = (PFN_vkCreateEvent)gdpa(device, "vkCreateEvent");
   table->DestroyEvent        = (PFN_vkDestroyEvent)gdpa(device, "vkDestroyEvent");
   table->GetEventStatus      = (PFN_vkGetEventStatus)gdpa(device, "vkGetEventStatus");
   table->SetEvent            = (PFN_vkSetEvent)gdpa(device, "vkSetEvent");
   table->ResetEvent          = (PFN_vkResetEvent)gdpa(device, "vkResetEvent");
   table->CreateQueryPool     = (PFN_vkCreateQueryPool)gdpa(device, "vkCreateQueryPool");
   table->DestroyQueryPool    = (PFN_vkDestroyQueryPool)gdpa(device, "vkDestroyQueryPool");
   table->GetQueryPoolResults = (PFN_vkGetQueryPoolResults)gdpa(device, "vkGetQueryPoolResults");
   table->CreateBuffer        = (PFN_vkCreateBuffer)gdpa(device, "vkCreateBuffer");
   table->DestroyBuffer       = (PFN_vkDestroyBuffer)gdpa(device, "vkDestroyBuffer");
   table->CreateBufferView    = (PFN_vkCreateBufferView)gdpa(device, "vkCreateBufferView");
   table->DestroyBufferView   = (PFN_vkDestroyBufferView)gdpa(device, "vkDestroyBufferView");
   table->CreateImage         = (PFN_vkCreateImage)gdpa(device, "vkCreateImage");
   table->DestroyImage        = (PFN_vkDestroyImage)gdpa(device, "vkDestroyImage");
   table->GetImageSubresourceLayout =
     (PFN_vkGetImageSubresourceLayout)gdpa(device, "vkGetImageSubresourceLayout");
   table->CreateImageView      = (PFN_vkCreateImageView)gdpa(device, "vkCreateImageView");
   table->DestroyImageView     = (PFN_vkDestroyImageView)gdpa(device, "vkDestroyImageView");
   table->CreateShaderModule   = (PFN_vkCreateShaderModule)gdpa(device, "vkCreateShaderModule");
   table->DestroyShaderModule  = (PFN_vkDestroyShaderModule)gdpa(device, "vkDestroyShaderModule");
   table->CreatePipelineCache  = (PFN_vkCreatePipelineCache)gdpa(device, "vkCreatePipelineCache");
   table->DestroyPipelineCache = (PFN_vkDestroyPipelineCache)gdpa(device, "vkDestroyPipelineCache");
   table->GetPipelineCacheData = (PFN_vkGetPipelineCacheData)gdpa(device, "vkGetPipelineCacheData");
   table->MergePipelineCaches  = (PFN_vkMergePipelineCaches)gdpa(device, "vkMergePipelineCaches");
   table->CreateGraphicsPipelines =
     (PFN_vkCreateGraphicsPipelines)gdpa(device, "vkCreateGraphicsPipelines");
   table->CreateComputePipelines =
     (PFN_vkCreateComputePipelines)gdpa(device, "vkCreateComputePipelines");
   table->DestroyPipeline      = (PFN_vkDestroyPipeline)gdpa(device, "vkDestroyPipeline");
   table->CreatePipelineLayout = (PFN_vkCreatePipelineLayout)gdpa(device, "vkCreatePipelineLayout");
   table->DestroyPipelineLayout =
     (PFN_vkDestroyPipelineLayout)gdpa(device, "vkDestroyPipelineLayout");
   table->CreateSampler  = (PFN_vkCreateSampler)gdpa(device, "vkCreateSampler");
   table->DestroySampler = (PFN_vkDestroySampler)gdpa(device, "vkDestroySampler");
   table->CreateDescriptorSetLayout =
     (PFN_vkCreateDescriptorSetLayout)gdpa(device, "vkCreateDescriptorSetLayout");
   table->DestroyDescriptorSetLayout =
     (PFN_vkDestroyDescriptorSetLayout)gdpa(device, "vkDestroyDescriptorSetLayout");
   table->CreateDescriptorPool = (PFN_vkCreateDescriptorPool)gdpa(device, "vkCreateDescriptorPool");
   table->DestroyDescriptorPool =
     (PFN_vkDestroyDescriptorPool)gdpa(device, "vkDestroyDescriptorPool");
   table->ResetDescriptorPool = (PFN_vkResetDescriptorPool)gdpa(device, "vkResetDescriptorPool");
   table->AllocateDescriptorSets =
     (PFN_vkAllocateDescriptorSets)gdpa(device, "vkAllocateDescriptorSets");
   table->FreeDescriptorSets   = (PFN_vkFreeDescriptorSets)gdpa(device, "vkFreeDescriptorSets");
   table->UpdateDescriptorSets = (PFN_vkUpdateDescriptorSets)gdpa(device, "vkUpdateDescriptorSets");
   table->CreateFramebuffer    = (PFN_vkCreateFramebuffer)gdpa(device, "vkCreateFramebuffer");
   table->DestroyFramebuffer   = (PFN_vkDestroyFramebuffer)gdpa(device, "vkDestroyFramebuffer");
   table->CreateRenderPass     = (PFN_vkCreateRenderPass)gdpa(device, "vkCreateRenderPass");
   table->DestroyRenderPass    = (PFN_vkDestroyRenderPass)gdpa(device, "vkDestroyRenderPass");
   table->GetRenderAreaGranularity =
     (PFN_vkGetRenderAreaGranularity)gdpa(device, "vkGetRenderAreaGranularity");
   table->CreateCommandPool  = (PFN_vkCreateCommandPool)gdpa(device, "vkCreateCommandPool");
   table->DestroyCommandPool = (PFN_vkDestroyCommandPool)gdpa(device, "vkDestroyCommandPool");
   table->ResetCommandPool   = (PFN_vkResetCommandPool)gdpa(device, "vkResetCommandPool");
   table->AllocateCommandBuffers =
     (PFN_vkAllocateCommandBuffers)gdpa(device, "vkAllocateCommandBuffers");
   table->FreeCommandBuffers   = (PFN_vkFreeCommandBuffers)gdpa(device, "vkFreeCommandBuffers");
   table->BeginCommandBuffer   = (PFN_vkBeginCommandBuffer)gdpa(device, "vkBeginCommandBuffer");
   table->EndCommandBuffer     = (PFN_vkEndCommandBuffer)gdpa(device, "vkEndCommandBuffer");
   table->ResetCommandBuffer   = (PFN_vkResetCommandBuffer)gdpa(device, "vkResetCommandBuffer");
   table->CmdBindPipeline      = (PFN_vkCmdBindPipeline)gdpa(device, "vkCmdBindPipeline");
   table->CmdSetViewport       = (PFN_vkCmdSetViewport)gdpa(device, "vkCmdSetViewport");
   table->CmdSetScissor        = (PFN_vkCmdSetScissor)gdpa(device, "vkCmdSetScissor");
   table->CmdSetLineWidth      = (PFN_vkCmdSetLineWidth)gdpa(device, "vkCmdSetLineWidth");
   table->CmdSetDepthBias      = (PFN_vkCmdSetDepthBias)gdpa(device, "vkCmdSetDepthBias");
   table->CmdSetBlendConstants = (PFN_vkCmdSetBlendConstants)gdpa(device, "vkCmdSetBlendConstants");
   table->CmdSetDepthBounds    = (PFN_vkCmdSetDepthBounds)gdpa(device, "vkCmdSetDepthBounds");
   table->CmdSetStencilCompareMask =
     (PFN_vkCmdSetStencilCompareMask)gdpa(device, "vkCmdSetStencilCompareMask");
   table->CmdSetStencilWriteMask =
     (PFN_vkCmdSetStencilWriteMask)gdpa(device, "vkCmdSetStencilWriteMask");
   table->CmdSetStencilReference =
     (PFN_vkCmdSetStencilReference)gdpa(device, "vkCmdSetStencilReference");
   table->CmdBindDescriptorSets =
     (PFN_vkCmdBindDescriptorSets)gdpa(device, "vkCmdBindDescriptorSets");
   table->CmdBindIndexBuffer   = (PFN_vkCmdBindIndexBuffer)gdpa(device, "vkCmdBindIndexBuffer");
   table->CmdBindVertexBuffers = (PFN_vkCmdBindVertexBuffers)gdpa(device, "vkCmdBindVertexBuffers");
   table->CmdDraw              = (PFN_vkCmdDraw)gdpa(device, "vkCmdDraw");
   table->CmdDrawIndexed       = (PFN_vkCmdDrawIndexed)gdpa(device, "vkCmdDrawIndexed");
   table->CmdDrawIndirect      = (PFN_vkCmdDrawIndirect)gdpa(device, "vkCmdDrawIndirect");
   table->CmdDrawIndexedIndirect =
     (PFN_vkCmdDrawIndexedIndirect)gdpa(device, "vkCmdDrawIndexedIndirect");
   table->CmdDispatch          = (PFN_vkCmdDispatch)gdpa(device, "vkCmdDispatch");
   table->CmdDispatchIndirect  = (PFN_vkCmdDispatchIndirect)gdpa(device, "vkCmdDispatchIndirect");
   table->CmdCopyBuffer        = (PFN_vkCmdCopyBuffer)gdpa(device, "vkCmdCopyBuffer");
   table->CmdCopyImage         = (PFN_vkCmdCopyImage)gdpa(device, "vkCmdCopyImage");
   table->CmdBlitImage         = (PFN_vkCmdBlitImage)gdpa(device, "vkCmdBlitImage");
   table->CmdCopyBufferToImage = (PFN_vkCmdCopyBufferToImage)gdpa(device, "vkCmdCopyBufferToImage");
   table->CmdCopyImageToBuffer = (PFN_vkCmdCopyImageToBuffer)gdpa(device, "vkCmdCopyImageToBuffer");
   table->CmdUpdateBuffer      = (PFN_vkCmdUpdateBuffer)gdpa(device, "vkCmdUpdateBuffer");
   table->CmdFillBuffer        = (PFN_vkCmdFillBuffer)gdpa(device, "vkCmdFillBuffer");
   table->CmdClearColorImage   = (PFN_vkCmdClearColorImage)gdpa(device, "vkCmdClearColorImage");
   table->CmdClearDepthStencilImage =
     (PFN_vkCmdClearDepthStencilImage)gdpa(device, "vkCmdClearDepthStencilImage");
   table->CmdClearAttachments = (PFN_vkCmdClearAttachments)gdpa(device, "vkCmdClearAttachments");
   table->CmdResolveImage     = (PFN_vkCmdResolveImage)gdpa(device, "vkCmdResolveImage");
   table->CmdSetEvent         = (PFN_vkCmdSetEvent)gdpa(device, "vkCmdSetEvent");
   table->CmdResetEvent       = (PFN_vkCmdResetEvent)gdpa(device, "vkCmdResetEvent");
   table->CmdWaitEvents       = (PFN_vkCmdWaitEvents)gdpa(device, "vkCmdWaitEvents");
   table->CmdPipelineBarrier  = (PFN_vkCmdPipelineBarrier)gdpa(device, "vkCmdPipelineBarrier");
   table->CmdBeginQuery       = (PFN_vkCmdBeginQuery)gdpa(device, "vkCmdBeginQuery");
   table->CmdEndQuery         = (PFN_vkCmdEndQuery)gdpa(device, "vkCmdEndQuery");
   table->CmdResetQueryPool   = (PFN_vkCmdResetQueryPool)gdpa(device, "vkCmdResetQueryPool");
   table->CmdWriteTimestamp   = (PFN_vkCmdWriteTimestamp)gdpa(device, "vkCmdWriteTimestamp");
   table->CmdCopyQueryPoolResults =
     (PFN_vkCmdCopyQueryPoolResults)gdpa(device, "vkCmdCopyQueryPoolResults");
   table->CmdPushConstants   = (PFN_vkCmdPushConstants)gdpa(device, "vkCmdPushConstants");
   table->CmdBeginRenderPass = (PFN_vkCmdBeginRenderPass)gdpa(device, "vkCmdBeginRenderPass");
   table->CmdNextSubpass     = (PFN_vkCmdNextSubpass)gdpa(device, "vkCmdNextSubpass");
   table->CmdEndRenderPass   = (PFN_vkCmdEndRenderPass)gdpa(device, "vkCmdEndRenderPass");
   table->CmdExecuteCommands = (PFN_vkCmdExecuteCommands)gdpa(device, "vkCmdExecuteCommands");
   table->BindBufferMemory2  = (PFN_vkBindBufferMemory2)gdpa(device, "vkBindBufferMemory2");
   table->BindImageMemory2   = (PFN_vkBindImageMemory2)gdpa(device, "vkBindImageMemory2");
   table->GetDeviceGroupPeerMemoryFeatures =
     (PFN_vkGetDeviceGroupPeerMemoryFeatures)gdpa(device, "vkGetDeviceGroupPeerMemoryFeatures");
   table->CmdSetDeviceMask = (PFN_vkCmdSetDeviceMask)gdpa(device, "vkCmdSetDeviceMask");
   table->CmdDispatchBase  = (PFN_vkCmdDispatchBase)gdpa(device, "vkCmdDispatchBase");
   table->GetImageMemoryRequirements2 =
     (PFN_vkGetImageMemoryRequirements2)gdpa(device, "vkGetImageMemoryRequirements2");
   table->GetBufferMemoryRequirements2 =
     (PFN_vkGetBufferMemoryRequirements2)gdpa(device, "vkGetBufferMemoryRequirements2");
   table->GetImageSparseMemoryRequirements2 =
     (PFN_vkGetImageSparseMemoryRequirements2)gdpa(device, "vkGetImageSparseMemoryRequirements2");
   table->TrimCommandPool = (PFN_vkTrimCommandPool)gdpa(device, "vkTrimCommandPool");
   table->GetDeviceQueue2 = (PFN_vkGetDeviceQueue2)gdpa(device, "vkGetDeviceQueue2");
   table->CreateSamplerYcbcrConversion =
     (PFN_vkCreateSamplerYcbcrConversion)gdpa(device, "vkCreateSamplerYcbcrConversion");
   table->DestroySamplerYcbcrConversion =
     (PFN_vkDestroySamplerYcbcrConversion)gdpa(device, "vkDestroySamplerYcbcrConversion");
   table->CreateDescriptorUpdateTemplate =
     (PFN_vkCreateDescriptorUpdateTemplate)gdpa(device, "vkCreateDescriptorUpdateTemplate");
   table->DestroyDescriptorUpdateTemplate =
     (PFN_vkDestroyDescriptorUpdateTemplate)gdpa(device, "vkDestroyDescriptorUpdateTemplate");
   table->UpdateDescriptorSetWithTemplate =
     (PFN_vkUpdateDescriptorSetWithTemplate)gdpa(device, "vkUpdateDescriptorSetWithTemplate");
   table->GetDescriptorSetLayoutSupport =
     (PFN_vkGetDescriptorSetLayoutSupport)gdpa(device, "vkGetDescriptorSetLayoutSupport");
   table->CmdDrawIndirectCount = (PFN_vkCmdDrawIndirectCount)gdpa(device, "vkCmdDrawIndirectCount");
   table->CmdDrawIndexedIndirectCount =
     (PFN_vkCmdDrawIndexedIndirectCount)gdpa(device, "vkCmdDrawIndexedIndirectCount");
   table->CreateRenderPass2   = (PFN_vkCreateRenderPass2)gdpa(device, "vkCreateRenderPass2");
   table->CmdBeginRenderPass2 = (PFN_vkCmdBeginRenderPass2)gdpa(device, "vkCmdBeginRenderPass2");
   table->CmdNextSubpass2     = (PFN_vkCmdNextSubpass2)gdpa(device, "vkCmdNextSubpass2");
   table->CmdEndRenderPass2   = (PFN_vkCmdEndRenderPass2)gdpa(device, "vkCmdEndRenderPass2");
   table->ResetQueryPool      = (PFN_vkResetQueryPool)gdpa(device, "vkResetQueryPool");
   table->GetSemaphoreCounterValue =
     (PFN_vkGetSemaphoreCounterValue)gdpa(device, "vkGetSemaphoreCounterValue");
   table->WaitSemaphores  = (PFN_vkWaitSemaphores)gdpa(device, "vkWaitSemaphores");
   table->SignalSemaphore = (PFN_vkSignalSemaphore)gdpa(device, "vkSignalSemaphore");
   table->GetBufferDeviceAddress =
     (PFN_vkGetBufferDeviceAddress)gdpa(device, "vkGetBufferDeviceAddress");
   table->GetBufferOpaqueCaptureAddress =
     (PFN_vkGetBufferOpaqueCaptureAddress)gdpa(device, "vkGetBufferOpaqueCaptureAddress");
   table->GetDeviceMemoryOpaqueCaptureAddress = (PFN_vkGetDeviceMemoryOpaqueCaptureAddress)gdpa(
     device, "vkGetDeviceMemoryOpaqueCaptureAddress");
   table->CreatePrivateDataSlot =
     (PFN_vkCreatePrivateDataSlot)gdpa(device, "vkCreatePrivateDataSlot");
   table->DestroyPrivateDataSlot =
     (PFN_vkDestroyPrivateDataSlot)gdpa(device, "vkDestroyPrivateDataSlot");
   table->SetPrivateData      = (PFN_vkSetPrivateData)gdpa(device, "vkSetPrivateData");
   table->GetPrivateData      = (PFN_vkGetPrivateData)gdpa(device, "vkGetPrivateData");
   table->CmdSetEvent2        = (PFN_vkCmdSetEvent2)gdpa(device, "vkCmdSetEvent2");
   table->CmdResetEvent2      = (PFN_vkCmdResetEvent2)gdpa(device, "vkCmdResetEvent2");
   table->CmdWaitEvents2      = (PFN_vkCmdWaitEvents2)gdpa(device, "vkCmdWaitEvents2");
   table->CmdPipelineBarrier2 = (PFN_vkCmdPipelineBarrier2)gdpa(device, "vkCmdPipelineBarrier2");
   table->CmdWriteTimestamp2  = (PFN_vkCmdWriteTimestamp2)gdpa(device, "vkCmdWriteTimestamp2");
   table->QueueSubmit2        = (PFN_vkQueueSubmit2)gdpa(device, "vkQueueSubmit2");
   table->CmdCopyBuffer2      = (PFN_vkCmdCopyBuffer2)gdpa(device, "vkCmdCopyBuffer2");
   table->CmdCopyImage2       = (PFN_vkCmdCopyImage2)gdpa(device, "vkCmdCopyImage2");
   table->CmdCopyBufferToImage2 =
     (PFN_vkCmdCopyBufferToImage2)gdpa(device, "vkCmdCopyBufferToImage2");
   table->CmdCopyImageToBuffer2 =
     (PFN_vkCmdCopyImageToBuffer2)gdpa(device, "vkCmdCopyImageToBuffer2");
   table->CmdBlitImage2     = (PFN_vkCmdBlitImage2)gdpa(device, "vkCmdBlitImage2");
   table->CmdResolveImage2  = (PFN_vkCmdResolveImage2)gdpa(device, "vkCmdResolveImage2");
   table->CmdBeginRendering = (PFN_vkCmdBeginRendering)gdpa(device, "vkCmdBeginRendering");
   table->CmdEndRendering   = (PFN_vkCmdEndRendering)gdpa(device, "vkCmdEndRendering");
   table->CmdSetCullMode    = (PFN_vkCmdSetCullMode)gdpa(device, "vkCmdSetCullMode");
   table->CmdSetFrontFace   = (PFN_vkCmdSetFrontFace)gdpa(device, "vkCmdSetFrontFace");
   table->CmdSetPrimitiveTopology =
     (PFN_vkCmdSetPrimitiveTopology)gdpa(device, "vkCmdSetPrimitiveTopology");
   table->CmdSetViewportWithCount =
     (PFN_vkCmdSetViewportWithCount)gdpa(device, "vkCmdSetViewportWithCount");
   table->CmdSetScissorWithCount =
     (PFN_vkCmdSetScissorWithCount)gdpa(device, "vkCmdSetScissorWithCount");
   table->CmdBindVertexBuffers2 =
     (PFN_vkCmdBindVertexBuffers2)gdpa(device, "vkCmdBindVertexBuffers2");
   table->CmdSetDepthTestEnable =
     (PFN_vkCmdSetDepthTestEnable)gdpa(device, "vkCmdSetDepthTestEnable");
   table->CmdSetDepthWriteEnable =
     (PFN_vkCmdSetDepthWriteEnable)gdpa(device, "vkCmdSetDepthWriteEnable");
   table->CmdSetDepthCompareOp = (PFN_vkCmdSetDepthCompareOp)gdpa(device, "vkCmdSetDepthCompareOp");
   table->CmdSetDepthBoundsTestEnable =
     (PFN_vkCmdSetDepthBoundsTestEnable)gdpa(device, "vkCmdSetDepthBoundsTestEnable");
   table->CmdSetStencilTestEnable =
     (PFN_vkCmdSetStencilTestEnable)gdpa(device, "vkCmdSetStencilTestEnable");
   table->CmdSetStencilOp = (PFN_vkCmdSetStencilOp)gdpa(device, "vkCmdSetStencilOp");
   table->CmdSetRasterizerDiscardEnable =
     (PFN_vkCmdSetRasterizerDiscardEnable)gdpa(device, "vkCmdSetRasterizerDiscardEnable");
   table->CmdSetDepthBiasEnable =
     (PFN_vkCmdSetDepthBiasEnable)gdpa(device, "vkCmdSetDepthBiasEnable");
   table->CmdSetPrimitiveRestartEnable =
     (PFN_vkCmdSetPrimitiveRestartEnable)gdpa(device, "vkCmdSetPrimitiveRestartEnable");
   table->GetDeviceBufferMemoryRequirements =
     (PFN_vkGetDeviceBufferMemoryRequirements)gdpa(device, "vkGetDeviceBufferMemoryRequirements");
   table->GetDeviceImageMemoryRequirements =
     (PFN_vkGetDeviceImageMemoryRequirements)gdpa(device, "vkGetDeviceImageMemoryRequirements");
   table->GetDeviceImageSparseMemoryRequirements =
     (PFN_vkGetDeviceImageSparseMemoryRequirements)gdpa(device,
                                                        "vkGetDeviceImageSparseMemoryRequirements");
   table->CreateSwapchainKHR  = (PFN_vkCreateSwapchainKHR)gdpa(device, "vkCreateSwapchainKHR");
   table->DestroySwapchainKHR = (PFN_vkDestroySwapchainKHR)gdpa(device, "vkDestroySwapchainKHR");
   table->GetSwapchainImagesKHR =
     (PFN_vkGetSwapchainImagesKHR)gdpa(device, "vkGetSwapchainImagesKHR");
   table->AcquireNextImageKHR = (PFN_vkAcquireNextImageKHR)gdpa(device, "vkAcquireNextImageKHR");
   table->QueuePresentKHR     = (PFN_vkQueuePresentKHR)gdpa(device, "vkQueuePresentKHR");
   table->GetDeviceGroupPresentCapabilitiesKHR = (PFN_vkGetDeviceGroupPresentCapabilitiesKHR)gdpa(
     device, "vkGetDeviceGroupPresentCapabilitiesKHR");
   table->GetDeviceGroupSurfacePresentModesKHR = (PFN_vkGetDeviceGroupSurfacePresentModesKHR)gdpa(
     device, "vkGetDeviceGroupSurfacePresentModesKHR");
   table->AcquireNextImage2KHR = (PFN_vkAcquireNextImage2KHR)gdpa(device, "vkAcquireNextImage2KHR");
   table->CreateSharedSwapchainsKHR =
     (PFN_vkCreateSharedSwapchainsKHR)gdpa(device, "vkCreateSharedSwapchainsKHR");
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->CreateVideoSessionKHR =
     (PFN_vkCreateVideoSessionKHR)gdpa(device, "vkCreateVideoSessionKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->DestroyVideoSessionKHR =
     (PFN_vkDestroyVideoSessionKHR)gdpa(device, "vkDestroyVideoSessionKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->GetVideoSessionMemoryRequirementsKHR = (PFN_vkGetVideoSessionMemoryRequirementsKHR)gdpa(
     device, "vkGetVideoSessionMemoryRequirementsKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->BindVideoSessionMemoryKHR =
     (PFN_vkBindVideoSessionMemoryKHR)gdpa(device, "vkBindVideoSessionMemoryKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->CreateVideoSessionParametersKHR =
     (PFN_vkCreateVideoSessionParametersKHR)gdpa(device, "vkCreateVideoSessionParametersKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->UpdateVideoSessionParametersKHR =
     (PFN_vkUpdateVideoSessionParametersKHR)gdpa(device, "vkUpdateVideoSessionParametersKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->DestroyVideoSessionParametersKHR =
     (PFN_vkDestroyVideoSessionParametersKHR)gdpa(device, "vkDestroyVideoSessionParametersKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->CmdBeginVideoCodingKHR =
     (PFN_vkCmdBeginVideoCodingKHR)gdpa(device, "vkCmdBeginVideoCodingKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->CmdEndVideoCodingKHR = (PFN_vkCmdEndVideoCodingKHR)gdpa(device, "vkCmdEndVideoCodingKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->CmdControlVideoCodingKHR =
     (PFN_vkCmdControlVideoCodingKHR)gdpa(device, "vkCmdControlVideoCodingKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->CmdDecodeVideoKHR = (PFN_vkCmdDecodeVideoKHR)gdpa(device, "vkCmdDecodeVideoKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
   table->CmdBeginRenderingKHR = (PFN_vkCmdBeginRenderingKHR)gdpa(device, "vkCmdBeginRenderingKHR");
   table->CmdEndRenderingKHR   = (PFN_vkCmdEndRenderingKHR)gdpa(device, "vkCmdEndRenderingKHR");
   table->GetDeviceGroupPeerMemoryFeaturesKHR = (PFN_vkGetDeviceGroupPeerMemoryFeaturesKHR)gdpa(
     device, "vkGetDeviceGroupPeerMemoryFeaturesKHR");
   table->CmdSetDeviceMaskKHR = (PFN_vkCmdSetDeviceMaskKHR)gdpa(device, "vkCmdSetDeviceMaskKHR");
   table->CmdDispatchBaseKHR  = (PFN_vkCmdDispatchBaseKHR)gdpa(device, "vkCmdDispatchBaseKHR");
   table->TrimCommandPoolKHR  = (PFN_vkTrimCommandPoolKHR)gdpa(device, "vkTrimCommandPoolKHR");
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->GetMemoryWin32HandleKHR =
     (PFN_vkGetMemoryWin32HandleKHR)gdpa(device, "vkGetMemoryWin32HandleKHR");
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->GetMemoryWin32HandlePropertiesKHR =
     (PFN_vkGetMemoryWin32HandlePropertiesKHR)gdpa(device, "vkGetMemoryWin32HandlePropertiesKHR");
#endif // VK_USE_PLATFORM_WIN32_KHR
   table->GetMemoryFdKHR = (PFN_vkGetMemoryFdKHR)gdpa(device, "vkGetMemoryFdKHR");
   table->GetMemoryFdPropertiesKHR =
     (PFN_vkGetMemoryFdPropertiesKHR)gdpa(device, "vkGetMemoryFdPropertiesKHR");
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->ImportSemaphoreWin32HandleKHR =
     (PFN_vkImportSemaphoreWin32HandleKHR)gdpa(device, "vkImportSemaphoreWin32HandleKHR");
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->GetSemaphoreWin32HandleKHR =
     (PFN_vkGetSemaphoreWin32HandleKHR)gdpa(device, "vkGetSemaphoreWin32HandleKHR");
#endif // VK_USE_PLATFORM_WIN32_KHR
   table->ImportSemaphoreFdKHR = (PFN_vkImportSemaphoreFdKHR)gdpa(device, "vkImportSemaphoreFdKHR");
   table->GetSemaphoreFdKHR    = (PFN_vkGetSemaphoreFdKHR)gdpa(device, "vkGetSemaphoreFdKHR");
   table->CmdPushDescriptorSetKHR =
     (PFN_vkCmdPushDescriptorSetKHR)gdpa(device, "vkCmdPushDescriptorSetKHR");
   table->CmdPushDescriptorSetWithTemplateKHR = (PFN_vkCmdPushDescriptorSetWithTemplateKHR)gdpa(
     device, "vkCmdPushDescriptorSetWithTemplateKHR");
   table->CreateDescriptorUpdateTemplateKHR =
     (PFN_vkCreateDescriptorUpdateTemplateKHR)gdpa(device, "vkCreateDescriptorUpdateTemplateKHR");
   table->DestroyDescriptorUpdateTemplateKHR =
     (PFN_vkDestroyDescriptorUpdateTemplateKHR)gdpa(device, "vkDestroyDescriptorUpdateTemplateKHR");
   table->UpdateDescriptorSetWithTemplateKHR =
     (PFN_vkUpdateDescriptorSetWithTemplateKHR)gdpa(device, "vkUpdateDescriptorSetWithTemplateKHR");
   table->CreateRenderPass2KHR = (PFN_vkCreateRenderPass2KHR)gdpa(device, "vkCreateRenderPass2KHR");
   table->CmdBeginRenderPass2KHR =
     (PFN_vkCmdBeginRenderPass2KHR)gdpa(device, "vkCmdBeginRenderPass2KHR");
   table->CmdNextSubpass2KHR   = (PFN_vkCmdNextSubpass2KHR)gdpa(device, "vkCmdNextSubpass2KHR");
   table->CmdEndRenderPass2KHR = (PFN_vkCmdEndRenderPass2KHR)gdpa(device, "vkCmdEndRenderPass2KHR");
   table->GetSwapchainStatusKHR =
     (PFN_vkGetSwapchainStatusKHR)gdpa(device, "vkGetSwapchainStatusKHR");
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->ImportFenceWin32HandleKHR =
     (PFN_vkImportFenceWin32HandleKHR)gdpa(device, "vkImportFenceWin32HandleKHR");
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->GetFenceWin32HandleKHR =
     (PFN_vkGetFenceWin32HandleKHR)gdpa(device, "vkGetFenceWin32HandleKHR");
#endif // VK_USE_PLATFORM_WIN32_KHR
   table->ImportFenceFdKHR = (PFN_vkImportFenceFdKHR)gdpa(device, "vkImportFenceFdKHR");
   table->GetFenceFdKHR    = (PFN_vkGetFenceFdKHR)gdpa(device, "vkGetFenceFdKHR");
   table->AcquireProfilingLockKHR =
     (PFN_vkAcquireProfilingLockKHR)gdpa(device, "vkAcquireProfilingLockKHR");
   table->ReleaseProfilingLockKHR =
     (PFN_vkReleaseProfilingLockKHR)gdpa(device, "vkReleaseProfilingLockKHR");
   table->GetImageMemoryRequirements2KHR =
     (PFN_vkGetImageMemoryRequirements2KHR)gdpa(device, "vkGetImageMemoryRequirements2KHR");
   table->GetBufferMemoryRequirements2KHR =
     (PFN_vkGetBufferMemoryRequirements2KHR)gdpa(device, "vkGetBufferMemoryRequirements2KHR");
   table->GetImageSparseMemoryRequirements2KHR = (PFN_vkGetImageSparseMemoryRequirements2KHR)gdpa(
     device, "vkGetImageSparseMemoryRequirements2KHR");
   table->CreateSamplerYcbcrConversionKHR =
     (PFN_vkCreateSamplerYcbcrConversionKHR)gdpa(device, "vkCreateSamplerYcbcrConversionKHR");
   table->DestroySamplerYcbcrConversionKHR =
     (PFN_vkDestroySamplerYcbcrConversionKHR)gdpa(device, "vkDestroySamplerYcbcrConversionKHR");
   table->BindBufferMemory2KHR = (PFN_vkBindBufferMemory2KHR)gdpa(device, "vkBindBufferMemory2KHR");
   table->BindImageMemory2KHR  = (PFN_vkBindImageMemory2KHR)gdpa(device, "vkBindImageMemory2KHR");
   table->GetDescriptorSetLayoutSupportKHR =
     (PFN_vkGetDescriptorSetLayoutSupportKHR)gdpa(device, "vkGetDescriptorSetLayoutSupportKHR");
   table->CmdDrawIndirectCountKHR =
     (PFN_vkCmdDrawIndirectCountKHR)gdpa(device, "vkCmdDrawIndirectCountKHR");
   table->CmdDrawIndexedIndirectCountKHR =
     (PFN_vkCmdDrawIndexedIndirectCountKHR)gdpa(device, "vkCmdDrawIndexedIndirectCountKHR");
   table->GetSemaphoreCounterValueKHR =
     (PFN_vkGetSemaphoreCounterValueKHR)gdpa(device, "vkGetSemaphoreCounterValueKHR");
   table->WaitSemaphoresKHR  = (PFN_vkWaitSemaphoresKHR)gdpa(device, "vkWaitSemaphoresKHR");
   table->SignalSemaphoreKHR = (PFN_vkSignalSemaphoreKHR)gdpa(device, "vkSignalSemaphoreKHR");
   table->CmdSetFragmentShadingRateKHR =
     (PFN_vkCmdSetFragmentShadingRateKHR)gdpa(device, "vkCmdSetFragmentShadingRateKHR");
   table->WaitForPresentKHR = (PFN_vkWaitForPresentKHR)gdpa(device, "vkWaitForPresentKHR");
   table->GetBufferDeviceAddressKHR =
     (PFN_vkGetBufferDeviceAddressKHR)gdpa(device, "vkGetBufferDeviceAddressKHR");
   table->GetBufferOpaqueCaptureAddressKHR =
     (PFN_vkGetBufferOpaqueCaptureAddressKHR)gdpa(device, "vkGetBufferOpaqueCaptureAddressKHR");
   table->GetDeviceMemoryOpaqueCaptureAddressKHR =
     (PFN_vkGetDeviceMemoryOpaqueCaptureAddressKHR)gdpa(device,
                                                        "vkGetDeviceMemoryOpaqueCaptureAddressKHR");
   table->CreateDeferredOperationKHR =
     (PFN_vkCreateDeferredOperationKHR)gdpa(device, "vkCreateDeferredOperationKHR");
   table->DestroyDeferredOperationKHR =
     (PFN_vkDestroyDeferredOperationKHR)gdpa(device, "vkDestroyDeferredOperationKHR");
   table->GetDeferredOperationMaxConcurrencyKHR = (PFN_vkGetDeferredOperationMaxConcurrencyKHR)gdpa(
     device, "vkGetDeferredOperationMaxConcurrencyKHR");
   table->GetDeferredOperationResultKHR =
     (PFN_vkGetDeferredOperationResultKHR)gdpa(device, "vkGetDeferredOperationResultKHR");
   table->DeferredOperationJoinKHR =
     (PFN_vkDeferredOperationJoinKHR)gdpa(device, "vkDeferredOperationJoinKHR");
   table->GetPipelineExecutablePropertiesKHR =
     (PFN_vkGetPipelineExecutablePropertiesKHR)gdpa(device, "vkGetPipelineExecutablePropertiesKHR");
   table->GetPipelineExecutableStatisticsKHR =
     (PFN_vkGetPipelineExecutableStatisticsKHR)gdpa(device, "vkGetPipelineExecutableStatisticsKHR");
   table->GetPipelineExecutableInternalRepresentationsKHR =
     (PFN_vkGetPipelineExecutableInternalRepresentationsKHR)gdpa(
       device, "vkGetPipelineExecutableInternalRepresentationsKHR");
#ifdef VK_ENABLE_BETA_EXTENSIONS
   table->CmdEncodeVideoKHR = (PFN_vkCmdEncodeVideoKHR)gdpa(device, "vkCmdEncodeVideoKHR");
#endif // VK_ENABLE_BETA_EXTENSIONS
   table->CmdSetEvent2KHR   = (PFN_vkCmdSetEvent2KHR)gdpa(device, "vkCmdSetEvent2KHR");
   table->CmdResetEvent2KHR = (PFN_vkCmdResetEvent2KHR)gdpa(device, "vkCmdResetEvent2KHR");
   table->CmdWaitEvents2KHR = (PFN_vkCmdWaitEvents2KHR)gdpa(device, "vkCmdWaitEvents2KHR");
   table->CmdPipelineBarrier2KHR =
     (PFN_vkCmdPipelineBarrier2KHR)gdpa(device, "vkCmdPipelineBarrier2KHR");
   table->CmdWriteTimestamp2KHR =
     (PFN_vkCmdWriteTimestamp2KHR)gdpa(device, "vkCmdWriteTimestamp2KHR");
   table->QueueSubmit2KHR = (PFN_vkQueueSubmit2KHR)gdpa(device, "vkQueueSubmit2KHR");
   table->CmdWriteBufferMarker2AMD =
     (PFN_vkCmdWriteBufferMarker2AMD)gdpa(device, "vkCmdWriteBufferMarker2AMD");
   table->GetQueueCheckpointData2NV =
     (PFN_vkGetQueueCheckpointData2NV)gdpa(device, "vkGetQueueCheckpointData2NV");
   table->CmdCopyBuffer2KHR = (PFN_vkCmdCopyBuffer2KHR)gdpa(device, "vkCmdCopyBuffer2KHR");
   table->CmdCopyImage2KHR  = (PFN_vkCmdCopyImage2KHR)gdpa(device, "vkCmdCopyImage2KHR");
   table->CmdCopyBufferToImage2KHR =
     (PFN_vkCmdCopyBufferToImage2KHR)gdpa(device, "vkCmdCopyBufferToImage2KHR");
   table->CmdCopyImageToBuffer2KHR =
     (PFN_vkCmdCopyImageToBuffer2KHR)gdpa(device, "vkCmdCopyImageToBuffer2KHR");
   table->CmdBlitImage2KHR    = (PFN_vkCmdBlitImage2KHR)gdpa(device, "vkCmdBlitImage2KHR");
   table->CmdResolveImage2KHR = (PFN_vkCmdResolveImage2KHR)gdpa(device, "vkCmdResolveImage2KHR");
   table->CmdTraceRaysIndirect2KHR =
     (PFN_vkCmdTraceRaysIndirect2KHR)gdpa(device, "vkCmdTraceRaysIndirect2KHR");
   table->GetDeviceBufferMemoryRequirementsKHR = (PFN_vkGetDeviceBufferMemoryRequirementsKHR)gdpa(
     device, "vkGetDeviceBufferMemoryRequirementsKHR");
   table->GetDeviceImageMemoryRequirementsKHR = (PFN_vkGetDeviceImageMemoryRequirementsKHR)gdpa(
     device, "vkGetDeviceImageMemoryRequirementsKHR");
   table->GetDeviceImageSparseMemoryRequirementsKHR =
     (PFN_vkGetDeviceImageSparseMemoryRequirementsKHR)gdpa(
       device, "vkGetDeviceImageSparseMemoryRequirementsKHR");
   table->DebugMarkerSetObjectTagEXT =
     (PFN_vkDebugMarkerSetObjectTagEXT)gdpa(device, "vkDebugMarkerSetObjectTagEXT");
   table->DebugMarkerSetObjectNameEXT =
     (PFN_vkDebugMarkerSetObjectNameEXT)gdpa(device, "vkDebugMarkerSetObjectNameEXT");
   table->CmdDebugMarkerBeginEXT =
     (PFN_vkCmdDebugMarkerBeginEXT)gdpa(device, "vkCmdDebugMarkerBeginEXT");
   table->CmdDebugMarkerEndEXT = (PFN_vkCmdDebugMarkerEndEXT)gdpa(device, "vkCmdDebugMarkerEndEXT");
   table->CmdDebugMarkerInsertEXT =
     (PFN_vkCmdDebugMarkerInsertEXT)gdpa(device, "vkCmdDebugMarkerInsertEXT");
   table->CmdBindTransformFeedbackBuffersEXT =
     (PFN_vkCmdBindTransformFeedbackBuffersEXT)gdpa(device, "vkCmdBindTransformFeedbackBuffersEXT");
   table->CmdBeginTransformFeedbackEXT =
     (PFN_vkCmdBeginTransformFeedbackEXT)gdpa(device, "vkCmdBeginTransformFeedbackEXT");
   table->CmdEndTransformFeedbackEXT =
     (PFN_vkCmdEndTransformFeedbackEXT)gdpa(device, "vkCmdEndTransformFeedbackEXT");
   table->CmdBeginQueryIndexedEXT =
     (PFN_vkCmdBeginQueryIndexedEXT)gdpa(device, "vkCmdBeginQueryIndexedEXT");
   table->CmdEndQueryIndexedEXT =
     (PFN_vkCmdEndQueryIndexedEXT)gdpa(device, "vkCmdEndQueryIndexedEXT");
   table->CmdDrawIndirectByteCountEXT =
     (PFN_vkCmdDrawIndirectByteCountEXT)gdpa(device, "vkCmdDrawIndirectByteCountEXT");
   table->CreateCuModuleNVX    = (PFN_vkCreateCuModuleNVX)gdpa(device, "vkCreateCuModuleNVX");
   table->CreateCuFunctionNVX  = (PFN_vkCreateCuFunctionNVX)gdpa(device, "vkCreateCuFunctionNVX");
   table->DestroyCuModuleNVX   = (PFN_vkDestroyCuModuleNVX)gdpa(device, "vkDestroyCuModuleNVX");
   table->DestroyCuFunctionNVX = (PFN_vkDestroyCuFunctionNVX)gdpa(device, "vkDestroyCuFunctionNVX");
   table->CmdCuLaunchKernelNVX = (PFN_vkCmdCuLaunchKernelNVX)gdpa(device, "vkCmdCuLaunchKernelNVX");
   table->GetImageViewHandleNVX =
     (PFN_vkGetImageViewHandleNVX)gdpa(device, "vkGetImageViewHandleNVX");
   table->GetImageViewAddressNVX =
     (PFN_vkGetImageViewAddressNVX)gdpa(device, "vkGetImageViewAddressNVX");
   table->CmdDrawIndirectCountAMD =
     (PFN_vkCmdDrawIndirectCountAMD)gdpa(device, "vkCmdDrawIndirectCountAMD");
   table->CmdDrawIndexedIndirectCountAMD =
     (PFN_vkCmdDrawIndexedIndirectCountAMD)gdpa(device, "vkCmdDrawIndexedIndirectCountAMD");
   table->GetShaderInfoAMD = (PFN_vkGetShaderInfoAMD)gdpa(device, "vkGetShaderInfoAMD");
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->GetMemoryWin32HandleNV =
     (PFN_vkGetMemoryWin32HandleNV)gdpa(device, "vkGetMemoryWin32HandleNV");
#endif // VK_USE_PLATFORM_WIN32_KHR
   table->CmdBeginConditionalRenderingEXT =
     (PFN_vkCmdBeginConditionalRenderingEXT)gdpa(device, "vkCmdBeginConditionalRenderingEXT");
   table->CmdEndConditionalRenderingEXT =
     (PFN_vkCmdEndConditionalRenderingEXT)gdpa(device, "vkCmdEndConditionalRenderingEXT");
   table->CmdSetViewportWScalingNV =
     (PFN_vkCmdSetViewportWScalingNV)gdpa(device, "vkCmdSetViewportWScalingNV");
   table->DisplayPowerControlEXT =
     (PFN_vkDisplayPowerControlEXT)gdpa(device, "vkDisplayPowerControlEXT");
   table->RegisterDeviceEventEXT =
     (PFN_vkRegisterDeviceEventEXT)gdpa(device, "vkRegisterDeviceEventEXT");
   table->RegisterDisplayEventEXT =
     (PFN_vkRegisterDisplayEventEXT)gdpa(device, "vkRegisterDisplayEventEXT");
   table->GetSwapchainCounterEXT =
     (PFN_vkGetSwapchainCounterEXT)gdpa(device, "vkGetSwapchainCounterEXT");
   table->GetRefreshCycleDurationGOOGLE =
     (PFN_vkGetRefreshCycleDurationGOOGLE)gdpa(device, "vkGetRefreshCycleDurationGOOGLE");
   table->GetPastPresentationTimingGOOGLE =
     (PFN_vkGetPastPresentationTimingGOOGLE)gdpa(device, "vkGetPastPresentationTimingGOOGLE");
   table->CmdSetDiscardRectangleEXT =
     (PFN_vkCmdSetDiscardRectangleEXT)gdpa(device, "vkCmdSetDiscardRectangleEXT");
   table->SetHdrMetadataEXT = (PFN_vkSetHdrMetadataEXT)gdpa(device, "vkSetHdrMetadataEXT");
   table->SetDebugUtilsObjectNameEXT =
     (PFN_vkSetDebugUtilsObjectNameEXT)gdpa(device, "vkSetDebugUtilsObjectNameEXT");
   table->SetDebugUtilsObjectTagEXT =
     (PFN_vkSetDebugUtilsObjectTagEXT)gdpa(device, "vkSetDebugUtilsObjectTagEXT");
   table->QueueBeginDebugUtilsLabelEXT =
     (PFN_vkQueueBeginDebugUtilsLabelEXT)gdpa(device, "vkQueueBeginDebugUtilsLabelEXT");
   table->QueueEndDebugUtilsLabelEXT =
     (PFN_vkQueueEndDebugUtilsLabelEXT)gdpa(device, "vkQueueEndDebugUtilsLabelEXT");
   table->QueueInsertDebugUtilsLabelEXT =
     (PFN_vkQueueInsertDebugUtilsLabelEXT)gdpa(device, "vkQueueInsertDebugUtilsLabelEXT");
   table->CmdBeginDebugUtilsLabelEXT =
     (PFN_vkCmdBeginDebugUtilsLabelEXT)gdpa(device, "vkCmdBeginDebugUtilsLabelEXT");
   table->CmdEndDebugUtilsLabelEXT =
     (PFN_vkCmdEndDebugUtilsLabelEXT)gdpa(device, "vkCmdEndDebugUtilsLabelEXT");
   table->CmdInsertDebugUtilsLabelEXT =
     (PFN_vkCmdInsertDebugUtilsLabelEXT)gdpa(device, "vkCmdInsertDebugUtilsLabelEXT");
#ifdef VK_USE_PLATFORM_ANDROID_KHR
   table->GetAndroidHardwareBufferPropertiesANDROID =
     (PFN_vkGetAndroidHardwareBufferPropertiesANDROID)gdpa(
       device, "vkGetAndroidHardwareBufferPropertiesANDROID");
#endif // VK_USE_PLATFORM_ANDROID_KHR
#ifdef VK_USE_PLATFORM_ANDROID_KHR
   table->GetMemoryAndroidHardwareBufferANDROID = (PFN_vkGetMemoryAndroidHardwareBufferANDROID)gdpa(
     device, "vkGetMemoryAndroidHardwareBufferANDROID");
#endif // VK_USE_PLATFORM_ANDROID_KHR
   table->CmdSetSampleLocationsEXT =
     (PFN_vkCmdSetSampleLocationsEXT)gdpa(device, "vkCmdSetSampleLocationsEXT");
   table->GetImageDrmFormatModifierPropertiesEXT =
     (PFN_vkGetImageDrmFormatModifierPropertiesEXT)gdpa(device,
                                                        "vkGetImageDrmFormatModifierPropertiesEXT");
   table->CreateValidationCacheEXT =
     (PFN_vkCreateValidationCacheEXT)gdpa(device, "vkCreateValidationCacheEXT");
   table->DestroyValidationCacheEXT =
     (PFN_vkDestroyValidationCacheEXT)gdpa(device, "vkDestroyValidationCacheEXT");
   table->MergeValidationCachesEXT =
     (PFN_vkMergeValidationCachesEXT)gdpa(device, "vkMergeValidationCachesEXT");
   table->GetValidationCacheDataEXT =
     (PFN_vkGetValidationCacheDataEXT)gdpa(device, "vkGetValidationCacheDataEXT");
   table->CmdBindShadingRateImageNV =
     (PFN_vkCmdBindShadingRateImageNV)gdpa(device, "vkCmdBindShadingRateImageNV");
   table->CmdSetViewportShadingRatePaletteNV =
     (PFN_vkCmdSetViewportShadingRatePaletteNV)gdpa(device, "vkCmdSetViewportShadingRatePaletteNV");
   table->CmdSetCoarseSampleOrderNV =
     (PFN_vkCmdSetCoarseSampleOrderNV)gdpa(device, "vkCmdSetCoarseSampleOrderNV");
   table->CreateAccelerationStructureNV =
     (PFN_vkCreateAccelerationStructureNV)gdpa(device, "vkCreateAccelerationStructureNV");
   table->DestroyAccelerationStructureNV =
     (PFN_vkDestroyAccelerationStructureNV)gdpa(device, "vkDestroyAccelerationStructureNV");
   table->GetAccelerationStructureMemoryRequirementsNV =
     (PFN_vkGetAccelerationStructureMemoryRequirementsNV)gdpa(
       device, "vkGetAccelerationStructureMemoryRequirementsNV");
   table->BindAccelerationStructureMemoryNV =
     (PFN_vkBindAccelerationStructureMemoryNV)gdpa(device, "vkBindAccelerationStructureMemoryNV");
   table->CmdBuildAccelerationStructureNV =
     (PFN_vkCmdBuildAccelerationStructureNV)gdpa(device, "vkCmdBuildAccelerationStructureNV");
   table->CmdCopyAccelerationStructureNV =
     (PFN_vkCmdCopyAccelerationStructureNV)gdpa(device, "vkCmdCopyAccelerationStructureNV");
   table->CmdTraceRaysNV = (PFN_vkCmdTraceRaysNV)gdpa(device, "vkCmdTraceRaysNV");
   table->CreateRayTracingPipelinesNV =
     (PFN_vkCreateRayTracingPipelinesNV)gdpa(device, "vkCreateRayTracingPipelinesNV");
   table->GetRayTracingShaderGroupHandlesKHR =
     (PFN_vkGetRayTracingShaderGroupHandlesKHR)gdpa(device, "vkGetRayTracingShaderGroupHandlesKHR");
   table->GetRayTracingShaderGroupHandlesNV =
     (PFN_vkGetRayTracingShaderGroupHandlesNV)gdpa(device, "vkGetRayTracingShaderGroupHandlesNV");
   table->GetAccelerationStructureHandleNV =
     (PFN_vkGetAccelerationStructureHandleNV)gdpa(device, "vkGetAccelerationStructureHandleNV");
   table->CmdWriteAccelerationStructuresPropertiesNV =
     (PFN_vkCmdWriteAccelerationStructuresPropertiesNV)gdpa(
       device, "vkCmdWriteAccelerationStructuresPropertiesNV");
   table->CompileDeferredNV = (PFN_vkCompileDeferredNV)gdpa(device, "vkCompileDeferredNV");
   table->GetMemoryHostPointerPropertiesEXT =
     (PFN_vkGetMemoryHostPointerPropertiesEXT)gdpa(device, "vkGetMemoryHostPointerPropertiesEXT");
   table->CmdWriteBufferMarkerAMD =
     (PFN_vkCmdWriteBufferMarkerAMD)gdpa(device, "vkCmdWriteBufferMarkerAMD");
   table->GetCalibratedTimestampsEXT =
     (PFN_vkGetCalibratedTimestampsEXT)gdpa(device, "vkGetCalibratedTimestampsEXT");
   table->CmdDrawMeshTasksNV = (PFN_vkCmdDrawMeshTasksNV)gdpa(device, "vkCmdDrawMeshTasksNV");
   table->CmdDrawMeshTasksIndirectNV =
     (PFN_vkCmdDrawMeshTasksIndirectNV)gdpa(device, "vkCmdDrawMeshTasksIndirectNV");
   table->CmdDrawMeshTasksIndirectCountNV =
     (PFN_vkCmdDrawMeshTasksIndirectCountNV)gdpa(device, "vkCmdDrawMeshTasksIndirectCountNV");
   table->CmdSetExclusiveScissorNV =
     (PFN_vkCmdSetExclusiveScissorNV)gdpa(device, "vkCmdSetExclusiveScissorNV");
   table->CmdSetCheckpointNV = (PFN_vkCmdSetCheckpointNV)gdpa(device, "vkCmdSetCheckpointNV");
   table->GetQueueCheckpointDataNV =
     (PFN_vkGetQueueCheckpointDataNV)gdpa(device, "vkGetQueueCheckpointDataNV");
   table->InitializePerformanceApiINTEL =
     (PFN_vkInitializePerformanceApiINTEL)gdpa(device, "vkInitializePerformanceApiINTEL");
   table->UninitializePerformanceApiINTEL =
     (PFN_vkUninitializePerformanceApiINTEL)gdpa(device, "vkUninitializePerformanceApiINTEL");
   table->CmdSetPerformanceMarkerINTEL =
     (PFN_vkCmdSetPerformanceMarkerINTEL)gdpa(device, "vkCmdSetPerformanceMarkerINTEL");
   table->CmdSetPerformanceStreamMarkerINTEL =
     (PFN_vkCmdSetPerformanceStreamMarkerINTEL)gdpa(device, "vkCmdSetPerformanceStreamMarkerINTEL");
   table->CmdSetPerformanceOverrideINTEL =
     (PFN_vkCmdSetPerformanceOverrideINTEL)gdpa(device, "vkCmdSetPerformanceOverrideINTEL");
   table->AcquirePerformanceConfigurationINTEL = (PFN_vkAcquirePerformanceConfigurationINTEL)gdpa(
     device, "vkAcquirePerformanceConfigurationINTEL");
   table->ReleasePerformanceConfigurationINTEL = (PFN_vkReleasePerformanceConfigurationINTEL)gdpa(
     device, "vkReleasePerformanceConfigurationINTEL");
   table->QueueSetPerformanceConfigurationINTEL = (PFN_vkQueueSetPerformanceConfigurationINTEL)gdpa(
     device, "vkQueueSetPerformanceConfigurationINTEL");
   table->GetPerformanceParameterINTEL =
     (PFN_vkGetPerformanceParameterINTEL)gdpa(device, "vkGetPerformanceParameterINTEL");
   table->SetLocalDimmingAMD = (PFN_vkSetLocalDimmingAMD)gdpa(device, "vkSetLocalDimmingAMD");
   table->GetBufferDeviceAddressEXT =
     (PFN_vkGetBufferDeviceAddressEXT)gdpa(device, "vkGetBufferDeviceAddressEXT");
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->AcquireFullScreenExclusiveModeEXT =
     (PFN_vkAcquireFullScreenExclusiveModeEXT)gdpa(device, "vkAcquireFullScreenExclusiveModeEXT");
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->ReleaseFullScreenExclusiveModeEXT =
     (PFN_vkReleaseFullScreenExclusiveModeEXT)gdpa(device, "vkReleaseFullScreenExclusiveModeEXT");
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   table->GetDeviceGroupSurfacePresentModes2EXT = (PFN_vkGetDeviceGroupSurfacePresentModes2EXT)gdpa(
     device, "vkGetDeviceGroupSurfacePresentModes2EXT");
#endif // VK_USE_PLATFORM_WIN32_KHR
   table->CmdSetLineStippleEXT = (PFN_vkCmdSetLineStippleEXT)gdpa(device, "vkCmdSetLineStippleEXT");
   table->ResetQueryPoolEXT    = (PFN_vkResetQueryPoolEXT)gdpa(device, "vkResetQueryPoolEXT");
   table->CmdSetCullModeEXT    = (PFN_vkCmdSetCullModeEXT)gdpa(device, "vkCmdSetCullModeEXT");
   table->CmdSetFrontFaceEXT   = (PFN_vkCmdSetFrontFaceEXT)gdpa(device, "vkCmdSetFrontFaceEXT");
   table->CmdSetPrimitiveTopologyEXT =
     (PFN_vkCmdSetPrimitiveTopologyEXT)gdpa(device, "vkCmdSetPrimitiveTopologyEXT");
   table->CmdSetViewportWithCountEXT =
     (PFN_vkCmdSetViewportWithCountEXT)gdpa(device, "vkCmdSetViewportWithCountEXT");
   table->CmdSetScissorWithCountEXT =
     (PFN_vkCmdSetScissorWithCountEXT)gdpa(device, "vkCmdSetScissorWithCountEXT");
   table->CmdBindVertexBuffers2EXT =
     (PFN_vkCmdBindVertexBuffers2EXT)gdpa(device, "vkCmdBindVertexBuffers2EXT");
   table->CmdSetDepthTestEnableEXT =
     (PFN_vkCmdSetDepthTestEnableEXT)gdpa(device, "vkCmdSetDepthTestEnableEXT");
   table->CmdSetDepthWriteEnableEXT =
     (PFN_vkCmdSetDepthWriteEnableEXT)gdpa(device, "vkCmdSetDepthWriteEnableEXT");
   table->CmdSetDepthCompareOpEXT =
     (PFN_vkCmdSetDepthCompareOpEXT)gdpa(device, "vkCmdSetDepthCompareOpEXT");
   table->CmdSetDepthBoundsTestEnableEXT =
     (PFN_vkCmdSetDepthBoundsTestEnableEXT)gdpa(device, "vkCmdSetDepthBoundsTestEnableEXT");
   table->CmdSetStencilTestEnableEXT =
     (PFN_vkCmdSetStencilTestEnableEXT)gdpa(device, "vkCmdSetStencilTestEnableEXT");
   table->CmdSetStencilOpEXT = (PFN_vkCmdSetStencilOpEXT)gdpa(device, "vkCmdSetStencilOpEXT");
   table->GetGeneratedCommandsMemoryRequirementsNV =
     (PFN_vkGetGeneratedCommandsMemoryRequirementsNV)gdpa(
       device, "vkGetGeneratedCommandsMemoryRequirementsNV");
   table->CmdPreprocessGeneratedCommandsNV =
     (PFN_vkCmdPreprocessGeneratedCommandsNV)gdpa(device, "vkCmdPreprocessGeneratedCommandsNV");
   table->CmdExecuteGeneratedCommandsNV =
     (PFN_vkCmdExecuteGeneratedCommandsNV)gdpa(device, "vkCmdExecuteGeneratedCommandsNV");
   table->CmdBindPipelineShaderGroupNV =
     (PFN_vkCmdBindPipelineShaderGroupNV)gdpa(device, "vkCmdBindPipelineShaderGroupNV");
   table->CreateIndirectCommandsLayoutNV =
     (PFN_vkCreateIndirectCommandsLayoutNV)gdpa(device, "vkCreateIndirectCommandsLayoutNV");
   table->DestroyIndirectCommandsLayoutNV =
     (PFN_vkDestroyIndirectCommandsLayoutNV)gdpa(device, "vkDestroyIndirectCommandsLayoutNV");
   table->CreatePrivateDataSlotEXT =
     (PFN_vkCreatePrivateDataSlotEXT)gdpa(device, "vkCreatePrivateDataSlotEXT");
   table->DestroyPrivateDataSlotEXT =
     (PFN_vkDestroyPrivateDataSlotEXT)gdpa(device, "vkDestroyPrivateDataSlotEXT");
   table->SetPrivateDataEXT = (PFN_vkSetPrivateDataEXT)gdpa(device, "vkSetPrivateDataEXT");
   table->GetPrivateDataEXT = (PFN_vkGetPrivateDataEXT)gdpa(device, "vkGetPrivateDataEXT");
   table->CmdSetFragmentShadingRateEnumNV =
     (PFN_vkCmdSetFragmentShadingRateEnumNV)gdpa(device, "vkCmdSetFragmentShadingRateEnumNV");
   table->GetImageSubresourceLayout2EXT =
     (PFN_vkGetImageSubresourceLayout2EXT)gdpa(device, "vkGetImageSubresourceLayout2EXT");
   table->CmdSetVertexInputEXT = (PFN_vkCmdSetVertexInputEXT)gdpa(device, "vkCmdSetVertexInputEXT");
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->GetMemoryZirconHandleFUCHSIA =
     (PFN_vkGetMemoryZirconHandleFUCHSIA)gdpa(device, "vkGetMemoryZirconHandleFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->GetMemoryZirconHandlePropertiesFUCHSIA =
     (PFN_vkGetMemoryZirconHandlePropertiesFUCHSIA)gdpa(device,
                                                        "vkGetMemoryZirconHandlePropertiesFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->ImportSemaphoreZirconHandleFUCHSIA =
     (PFN_vkImportSemaphoreZirconHandleFUCHSIA)gdpa(device, "vkImportSemaphoreZirconHandleFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->GetSemaphoreZirconHandleFUCHSIA =
     (PFN_vkGetSemaphoreZirconHandleFUCHSIA)gdpa(device, "vkGetSemaphoreZirconHandleFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->CreateBufferCollectionFUCHSIA =
     (PFN_vkCreateBufferCollectionFUCHSIA)gdpa(device, "vkCreateBufferCollectionFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->SetBufferCollectionImageConstraintsFUCHSIA =
     (PFN_vkSetBufferCollectionImageConstraintsFUCHSIA)gdpa(
       device, "vkSetBufferCollectionImageConstraintsFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->SetBufferCollectionBufferConstraintsFUCHSIA =
     (PFN_vkSetBufferCollectionBufferConstraintsFUCHSIA)gdpa(
       device, "vkSetBufferCollectionBufferConstraintsFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->DestroyBufferCollectionFUCHSIA =
     (PFN_vkDestroyBufferCollectionFUCHSIA)gdpa(device, "vkDestroyBufferCollectionFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
#ifdef VK_USE_PLATFORM_FUCHSIA
   table->GetBufferCollectionPropertiesFUCHSIA = (PFN_vkGetBufferCollectionPropertiesFUCHSIA)gdpa(
     device, "vkGetBufferCollectionPropertiesFUCHSIA");
#endif // VK_USE_PLATFORM_FUCHSIA
   table->GetDeviceSubpassShadingMaxWorkgroupSizeHUAWEI =
     (PFN_vkGetDeviceSubpassShadingMaxWorkgroupSizeHUAWEI)gdpa(
       device, "vkGetDeviceSubpassShadingMaxWorkgroupSizeHUAWEI");
   table->CmdSubpassShadingHUAWEI =
     (PFN_vkCmdSubpassShadingHUAWEI)gdpa(device, "vkCmdSubpassShadingHUAWEI");
   table->CmdBindInvocationMaskHUAWEI =
     (PFN_vkCmdBindInvocationMaskHUAWEI)gdpa(device, "vkCmdBindInvocationMaskHUAWEI");
   table->GetMemoryRemoteAddressNV =
     (PFN_vkGetMemoryRemoteAddressNV)gdpa(device, "vkGetMemoryRemoteAddressNV");
   table->GetPipelinePropertiesEXT =
     (PFN_vkGetPipelinePropertiesEXT)gdpa(device, "vkGetPipelinePropertiesEXT");
   table->CmdSetPatchControlPointsEXT =
     (PFN_vkCmdSetPatchControlPointsEXT)gdpa(device, "vkCmdSetPatchControlPointsEXT");
   table->CmdSetRasterizerDiscardEnableEXT =
     (PFN_vkCmdSetRasterizerDiscardEnableEXT)gdpa(device, "vkCmdSetRasterizerDiscardEnableEXT");
   table->CmdSetDepthBiasEnableEXT =
     (PFN_vkCmdSetDepthBiasEnableEXT)gdpa(device, "vkCmdSetDepthBiasEnableEXT");
   table->CmdSetLogicOpEXT = (PFN_vkCmdSetLogicOpEXT)gdpa(device, "vkCmdSetLogicOpEXT");
   table->CmdSetPrimitiveRestartEnableEXT =
     (PFN_vkCmdSetPrimitiveRestartEnableEXT)gdpa(device, "vkCmdSetPrimitiveRestartEnableEXT");
   table->CmdSetColorWriteEnableEXT =
     (PFN_vkCmdSetColorWriteEnableEXT)gdpa(device, "vkCmdSetColorWriteEnableEXT");
   table->CmdDrawMultiEXT = (PFN_vkCmdDrawMultiEXT)gdpa(device, "vkCmdDrawMultiEXT");
   table->CmdDrawMultiIndexedEXT =
     (PFN_vkCmdDrawMultiIndexedEXT)gdpa(device, "vkCmdDrawMultiIndexedEXT");
   table->SetDeviceMemoryPriorityEXT =
     (PFN_vkSetDeviceMemoryPriorityEXT)gdpa(device, "vkSetDeviceMemoryPriorityEXT");
   table->GetDescriptorSetLayoutHostMappingInfoVALVE =
     (PFN_vkGetDescriptorSetLayoutHostMappingInfoVALVE)gdpa(
       device, "vkGetDescriptorSetLayoutHostMappingInfoVALVE");
   table->GetDescriptorSetHostMappingVALVE =
     (PFN_vkGetDescriptorSetHostMappingVALVE)gdpa(device, "vkGetDescriptorSetHostMappingVALVE");
   table->CreateAccelerationStructureKHR =
     (PFN_vkCreateAccelerationStructureKHR)gdpa(device, "vkCreateAccelerationStructureKHR");
   table->DestroyAccelerationStructureKHR =
     (PFN_vkDestroyAccelerationStructureKHR)gdpa(device, "vkDestroyAccelerationStructureKHR");
   table->CmdBuildAccelerationStructuresKHR =
     (PFN_vkCmdBuildAccelerationStructuresKHR)gdpa(device, "vkCmdBuildAccelerationStructuresKHR");
   table->CmdBuildAccelerationStructuresIndirectKHR =
     (PFN_vkCmdBuildAccelerationStructuresIndirectKHR)gdpa(
       device, "vkCmdBuildAccelerationStructuresIndirectKHR");
   table->BuildAccelerationStructuresKHR =
     (PFN_vkBuildAccelerationStructuresKHR)gdpa(device, "vkBuildAccelerationStructuresKHR");
   table->CopyAccelerationStructureKHR =
     (PFN_vkCopyAccelerationStructureKHR)gdpa(device, "vkCopyAccelerationStructureKHR");
   table->CopyAccelerationStructureToMemoryKHR = (PFN_vkCopyAccelerationStructureToMemoryKHR)gdpa(
     device, "vkCopyAccelerationStructureToMemoryKHR");
   table->CopyMemoryToAccelerationStructureKHR = (PFN_vkCopyMemoryToAccelerationStructureKHR)gdpa(
     device, "vkCopyMemoryToAccelerationStructureKHR");
   table->WriteAccelerationStructuresPropertiesKHR =
     (PFN_vkWriteAccelerationStructuresPropertiesKHR)gdpa(
       device, "vkWriteAccelerationStructuresPropertiesKHR");
   table->CmdCopyAccelerationStructureKHR =
     (PFN_vkCmdCopyAccelerationStructureKHR)gdpa(device, "vkCmdCopyAccelerationStructureKHR");
   table->CmdCopyAccelerationStructureToMemoryKHR =
     (PFN_vkCmdCopyAccelerationStructureToMemoryKHR)gdpa(
       device, "vkCmdCopyAccelerationStructureToMemoryKHR");
   table->CmdCopyMemoryToAccelerationStructureKHR =
     (PFN_vkCmdCopyMemoryToAccelerationStructureKHR)gdpa(
       device, "vkCmdCopyMemoryToAccelerationStructureKHR");
   table->GetAccelerationStructureDeviceAddressKHR =
     (PFN_vkGetAccelerationStructureDeviceAddressKHR)gdpa(
       device, "vkGetAccelerationStructureDeviceAddressKHR");
   table->CmdWriteAccelerationStructuresPropertiesKHR =
     (PFN_vkCmdWriteAccelerationStructuresPropertiesKHR)gdpa(
       device, "vkCmdWriteAccelerationStructuresPropertiesKHR");
   table->GetDeviceAccelerationStructureCompatibilityKHR =
     (PFN_vkGetDeviceAccelerationStructureCompatibilityKHR)gdpa(
       device, "vkGetDeviceAccelerationStructureCompatibilityKHR");
   table->GetAccelerationStructureBuildSizesKHR = (PFN_vkGetAccelerationStructureBuildSizesKHR)gdpa(
     device, "vkGetAccelerationStructureBuildSizesKHR");
   table->CmdTraceRaysKHR = (PFN_vkCmdTraceRaysKHR)gdpa(device, "vkCmdTraceRaysKHR");
   table->CreateRayTracingPipelinesKHR =
     (PFN_vkCreateRayTracingPipelinesKHR)gdpa(device, "vkCreateRayTracingPipelinesKHR");
   table->GetRayTracingCaptureReplayShaderGroupHandlesKHR =
     (PFN_vkGetRayTracingCaptureReplayShaderGroupHandlesKHR)gdpa(
       device, "vkGetRayTracingCaptureReplayShaderGroupHandlesKHR");
   table->CmdTraceRaysIndirectKHR =
     (PFN_vkCmdTraceRaysIndirectKHR)gdpa(device, "vkCmdTraceRaysIndirectKHR");
   table->GetRayTracingShaderGroupStackSizeKHR = (PFN_vkGetRayTracingShaderGroupStackSizeKHR)gdpa(
     device, "vkGetRayTracingShaderGroupStackSizeKHR");
   table->CmdSetRayTracingPipelineStackSizeKHR = (PFN_vkCmdSetRayTracingPipelineStackSizeKHR)gdpa(
     device, "vkCmdSetRayTracingPipelineStackSizeKHR");
}
