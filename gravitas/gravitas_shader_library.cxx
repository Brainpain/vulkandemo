//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_shader_library.hxx"

#include "basic_color_lighting_frag.h"
#include "basic_scene_object_vert.h"
#include "basic_texture_frag.h"
#include "gravitas_device.hxx"
#include "gravitas_device_dispatch.hxx"
#include "gravitas_log.hxx"
#include "gravitas_resource_manager.hxx"

#include <algorithm>
#include <iostream>
#include <sstream>

std::vector<std::string> g_common_shader_names = {"basic_scene_object_vert",
                                                  "basic_color_lighting_frag",
                                                  "basic_texture_frag",
                                                  "basic_color_lighting_texture_frag"};

CGravitasShaderLibrary::CGravitasShaderLibrary(CGravitasResourceManager *resource_mgr,
                                               CGravitasDevice          *gravitas_device)
  : CGravitasResourceLibrary(resource_mgr, gravitas_device, GRAV_RESOURCE_SHADER)
{
   if (!LoadCommonShaders())
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed to load common shaders for CGravitasShaderLibrary init!\n";
      abort();
   }
}

CGravitasShaderLibrary::~CGravitasShaderLibrary()
{
   FreeInternals();
}

bool
CGravitasShaderLibrary::FindMatchingShader(const GravitasShaderResourceCreateInfo *create_info,
                                           uint64_t                               &resource_id)
{
   bool common_shader = IsCommonShader(create_info->shader_name);
   for (auto &resource : resources_)
   {
      GravitasShaderResource *shader_resource =
        reinterpret_cast<GravitasShaderResource *>(resource.second);
      if ((common_shader && shader_resource->shader_name == create_info->shader_name) ||
          (shader_resource->shader_name == create_info->shader_name &&
           shader_resource->entrypoint == create_info->entrypoint &&
           shader_resource->shader_stage == create_info->shader_stage &&
           std::equal(shader_resource->shader_spirv.begin(),
                      shader_resource->shader_spirv.end(),
                      create_info->shader_spirv.begin())))
      {
         resource_id = shader_resource->resource.resource_id;
         return true;
      }
   }
   return false;
}

bool
CGravitasShaderLibrary::IsCommonShader(const std::string &shader_name)
{
   if (std::find(g_common_shader_names.begin(), g_common_shader_names.end(), shader_name) !=
       g_common_shader_names.end())
   {
      return true;
   }
   return false;
}

// Load the limited number of pre-defined common shaders
bool
CGravitasShaderLibrary::LoadCommonShaders()
{
   for (uint32_t cur = 0; cur < g_common_shader_names.size(); ++cur)
   {
      GravitasShaderResourceCreateInfo shader_create_info = {};
      shader_create_info.resource_info                    = {.flag = GRAV_RESOURCE_SHADER};
      shader_create_info.entrypoint                       = "main";
      shader_create_info.shader_name                      = g_common_shader_names[cur];
      if (g_common_shader_names[cur].find("_vert") != std::string::npos)
      {
         shader_create_info.shader_stage = VK_SHADER_STAGE_VERTEX_BIT;
      }
      else
      {
         shader_create_info.shader_stage = VK_SHADER_STAGE_FRAGMENT_BIT;
      }
      switch (cur)
      {
         case 0:
            shader_create_info.shader_spirv = std::vector<uint32_t>(
              basic_scene_object_vert_array,
              basic_scene_object_vert_array + sizeof(basic_scene_object_vert_array));
            break;
         case 1:
            shader_create_info.shader_spirv = std::vector<uint32_t>(
              basic_color_lighting_frag_array,
              basic_color_lighting_frag_array + sizeof(basic_color_lighting_frag_array));
            break;
         case 2:
            shader_create_info.shader_spirv =
              std::vector<uint32_t>(basic_texture_frag_array,
                                    basic_texture_frag_array + sizeof(basic_texture_frag_array));
            break;
         case 3:
            shader_create_info.shader_spirv =
              std::vector<uint32_t>(basic_texture_frag_array,
                                    basic_texture_frag_array + sizeof(basic_texture_frag_array));
            break;
      }
      uint64_t temp_resource_id = 0ULL;
      if (!LoadShader(&shader_create_info, temp_resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed loading common shader " << shader_create_info.shader_name << "\n";
         return false;
      }
   }
   return true;
}

// Finds if already present and bumps up ref-counts.  If not present, creates and sets ref-count
// to 1 before returning values.
bool
CGravitasShaderLibrary::AddShader(const GravitasShaderResourceCreateInfo *create_info,
                                  uint64_t                               &resource_id)
{
   bool                    success         = false;
   GravitasShaderResource *shader_resource = nullptr;

   resource_id = 0ULL;
   if (!FindMatchingShader(create_info, resource_id))
   {
      if (!create_info->directory_string.empty() && !create_info->file_string.empty())
      {
         // TODO: Load shader content from file into create_info->shader_spirv
      }
      return LoadShader(create_info, resource_id);
   }
   else
   {
      ImportRef(resource_id);
   }
   return true;
}

// Finds if already present and bumps up ref-counts.  If not present, creates and sets ref-count
// to 1 before returning values.
bool
CGravitasShaderLibrary::LoadShader(const GravitasShaderResourceCreateInfo *create_info,
                                   uint64_t                               &resource_id)
{
   bool                    success         = false;
   GravitasShaderResource *shader_resource = nullptr;
   VkDevice                vk_dev          = gravitas_device_->GetVulkanDevice();

   VkShaderModuleCreateInfo shader_create_info = {};
   shader_create_info.sType                    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
   shader_create_info.pNext                    = nullptr;
   shader_create_info.codeSize = static_cast<uint32_t>(create_info->shader_spirv.size());
   shader_create_info.pCode    = create_info->shader_spirv.data();

   shader_resource = new GravitasShaderResource();
   if (nullptr == shader_resource)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed creating shader resource for " << create_info->shader_name;
      goto out;
   }

   if (VK_SUCCESS != vulkan_dev_dispatch_->CreateShaderModule(
                       vk_dev, &shader_create_info, nullptr, &shader_resource->vk_module))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed creating shader module for " << create_info->shader_name;
      return false;
   }

   shader_resource->resource.imported_ref_count = 0;
   shader_resource->resource.upload_ref_count   = 0;
   shader_resource->shader_name                 = create_info->shader_name;
   shader_resource->entrypoint                  = create_info->entrypoint;
   shader_resource->shader_stage                = create_info->shader_stage;
   shader_resource->shader_spirv                = create_info->shader_spirv;
   if (!AddResource(&shader_resource->resource, resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed adding shader resource";
      goto out;
   }

   ImportRef(resource_id);
   success = true;

out:
   return success;
}

// Drops ref-count, and deletes if 0.
bool
CGravitasShaderLibrary::RemoveShader(uint64_t &resource_id)
{
   VkDevice           vk_dev = gravitas_device_->GetVulkanDevice();
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed releasing shader resource " << outstream_uint64.str()
                                   << " because it does not exist!";
      return false;
   }
   GravitasShaderResource *shader_resource =
     reinterpret_cast<GravitasShaderResource *>(resources_[resource_id]);
   if (!ImportUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed decrementing import ref for shader resource " << outstream_uint64.str() << "!";
      return false;
   }

   if (shader_resource->resource.imported_ref_count == 0)
   {
      if (VK_NULL_HANDLE != shader_resource->vk_module)
      {
         vulkan_dev_dispatch_->DestroyShaderModule(vk_dev, shader_resource->vk_module, nullptr);
      }
      delete shader_resource;
      resources_.erase(resource_id);
   }
   return true;
}

void
CGravitasShaderLibrary::FreeInternals()
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   VkDevice                     vk_dev = gravitas_device_->GetVulkanDevice();
   for (auto &resource : resources_)
   {
      GravitasShaderResource *shader_resource =
        reinterpret_cast<GravitasShaderResource *>(resource.second);
      if (VK_NULL_HANDLE != shader_resource->vk_module)
      {
         vulkan_dev_dispatch_->DestroyShaderModule(vk_dev, shader_resource->vk_module, nullptr);
      }
      delete shader_resource;
   }
   resources_.clear();
}

bool
CGravitasShaderLibrary::Upload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed uploading shader resource " << outstream_uint64.str()
                                   << " because it does not exist!";
      return false;
   }
   return UploadRef(resource_id);
}

bool
CGravitasShaderLibrary::Unload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (!UploadUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed unloading shader resource " << outstream_uint64.str() << "!";
   }
   return true;
}
