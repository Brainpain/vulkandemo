//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include <SDL.h>
#include <string>
#include <vector>
#include <vulkan/vk_platform.h>
#include <vulkan/vulkan_core.h>

class CGravitasWindow
{
 public:
   CGravitasWindow(const std::string &window_name,
                   bool               fullscreen,
                   uint32_t           width,
                   uint32_t           height,
                   VkFormat           format);
   virtual ~CGravitasWindow();

   inline uint32_t GetWidth()
   {
      return width_;
   }
   inline uint32_t GetHeight()
   {
      return height_;
   }
   inline VkFormat GetFormat()
   {
      return vk_format_;
   }
   inline bool IsFullscreen()
   {
      return is_fullscreen_;
   }
   inline VkSurfaceKHR GetVkSurface() const
   {
      return vk_surface_;
   }
   void GetDrawableSize(VkExtent2D &size);

   bool CreateWindowSurface(VkInstance instance);
   void FreeWindowSurface(VkInstance instance);
   bool ResizeWindow(VkInstance instance, uint32_t width, uint32_t height);

   PFN_vkGetInstanceProcAddr GetInstanceProcAddr();
   uint32_t                  GetRequiredExtensions(std::vector<const char *> &extensions);

 private:
   CGravitasWindow()                  = delete;
   CGravitasWindow(CGravitasWindow &) = delete;

   uint32_t     width_         = 0;
   uint32_t     height_        = 0;
   VkFormat     vk_format_     = VK_FORMAT_UNDEFINED;
   bool         is_fullscreen_ = false;
   SDL_Window  *sdl_window_    = nullptr;
   VkSurfaceKHR vk_surface_    = VK_NULL_HANDLE;
};

class IGravitasWindowOwner
{
 public:
   IGravitasWindowOwner()
   {}
   virtual ~IGravitasWindowOwner()
   {}

   virtual CGravitasWindow *GetWindow() const        = 0;
   virtual VkFormat         GetDesiredFormat() const = 0;
};
