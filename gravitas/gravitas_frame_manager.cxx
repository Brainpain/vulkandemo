//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_frame_manager.hxx"

#include "gravitas_device.hxx"
#include "gravitas_frame_data.hxx"
#include "gravitas_log.hxx"
#include "gravitas_swapchain.hxx"

#include <algorithm>
#include <iostream>

CGravitasFrameManager::CGravitasFrameManager(const IVulkanInstanceOwner *instance_owner,
                                             const VkExtent2D           *window_extents,
                                             CGravitasDevice            *gravitas_device,
                                             uint8_t                     num_swapchain_images,
                                             VkFormat                    swapchain_format)
  : instance_owner_(instance_owner)
  , window_extents_(*window_extents)
  , gravitas_device_(gravitas_device)
  , num_swapchain_images_(num_swapchain_images)
  , vk_swapchain_format_(swapchain_format)
{
   vk_device_ = gravitas_device->GetVulkanDevice();

   if (!CreateSwapchain())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed initializing swapchain";
      abort();
   }

   if (!InitVulkanFrameData())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating frame data";
      abort();
   }
}

CGravitasFrameManager::~CGravitasFrameManager()
{
   FreeVulkanFrameData();
   FreeSwapchain();
}

bool
CGravitasFrameManager::InitVulkanFrameData()
{
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();

   frames_.resize(num_swapchain_images_);
   VkCommandPoolCreateInfo cmd_pool_info    = {};
   cmd_pool_info.sType                      = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
   cmd_pool_info.pNext                      = nullptr;
   cmd_pool_info.flags                      = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
   cmd_pool_info.queueFamilyIndex           = gravitas_device_->GetGraphicsQueueIndex();
   VkCommandBufferAllocateInfo cmd_buf_info = {};
   cmd_buf_info.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
   cmd_buf_info.pNext                       = nullptr;
   cmd_buf_info.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
   cmd_buf_info.commandBufferCount          = 1;

   // Create a fence that's already signalled so we can properly do the loop and
   // not wait in the beginning.
   VkFenceCreateInfo fence_create_info = {};
   fence_create_info.sType             = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
   fence_create_info.pNext             = nullptr;
   fence_create_info.flags             = VK_FENCE_CREATE_SIGNALED_BIT;

   // For the semaphores we don't need any flags
   VkSemaphoreCreateInfo semaphore_create_info = {};
   semaphore_create_info.sType                 = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
   semaphore_create_info.pNext                 = nullptr;
   semaphore_create_info.flags                 = 0;

   // Define the depth attachment reference, when this starts, we expect to be in
   // depth/stencil attach optimal state.
   VkAttachmentReference depth_attach_ref = {};
   depth_attach_ref.attachment            = 1; // Going to use the 2nd attachment for depth
   depth_attach_ref.layout                = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

   uint32_t          graphics_queue_index = gravitas_device_->GetGraphicsQueueIndex();
   VkImageCreateInfo image_create         = {};
   image_create.sType                     = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
   image_create.pNext                     = nullptr;
   image_create.flags                     = 0;
   image_create.imageType                 = VK_IMAGE_TYPE_2D;
   image_create.format                    = gravitas_depth_buffer_.vk_format;
   image_create.extent.width              = window_extents_.width;
   image_create.extent.height             = window_extents_.height;
   image_create.extent.depth              = 1;
   image_create.mipLevels                 = 1;
   image_create.arrayLayers               = 1;
   image_create.samples                   = VK_SAMPLE_COUNT_1_BIT;
   image_create.tiling                    = VK_IMAGE_TILING_OPTIMAL;
   image_create.usage                     = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
   image_create.sharingMode               = VK_SHARING_MODE_EXCLUSIVE;
   image_create.queueFamilyIndexCount     = 1;
   image_create.pQueueFamilyIndices       = &graphics_queue_index;
   image_create.initialLayout             = VK_IMAGE_LAYOUT_UNDEFINED;

   // Create the allocation and image and build the image view

   // Allocate the depth buffer local on the GPU for use only on the GPU
   VmaAllocationCreateInfo depth_allocation_create = {};
   depth_allocation_create.flags                   = 0;
   depth_allocation_create.usage                   = VMA_MEMORY_USAGE_GPU_ONLY;
   depth_allocation_create.requiredFlags =
     VkMemoryPropertyFlags(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
   depth_allocation_create.preferredFlags = 0;
   depth_allocation_create.memoryTypeBits = 0;
   depth_allocation_create.pool           = VK_NULL_HANDLE;
   depth_allocation_create.pUserData      = nullptr;
   depth_allocation_create.priority       = 1.f;
   if (VK_SUCCESS != vmaCreateImage(gravitas_device_->GetAllocator(),
                                    &image_create,
                                    &depth_allocation_create,
                                    &gravitas_depth_buffer_.alloc_image.vk_image,
                                    &gravitas_depth_buffer_.alloc_image.vma_allocation,
                                    nullptr))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating allocation or image for depth buffer";
      return false;
   }

   // Create an image view for the depth buffer
   VkImageViewCreateInfo image_view_create           = {};
   image_view_create.sType                           = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
   image_view_create.pNext                           = nullptr;
   image_view_create.flags                           = 0;
   image_view_create.image                           = gravitas_depth_buffer_.alloc_image.vk_image;
   image_view_create.viewType                        = VK_IMAGE_VIEW_TYPE_2D;
   image_view_create.format                          = gravitas_depth_buffer_.vk_format;
   image_view_create.components.r                    = VK_COMPONENT_SWIZZLE_R;
   image_view_create.components.g                    = VK_COMPONENT_SWIZZLE_G;
   image_view_create.components.b                    = VK_COMPONENT_SWIZZLE_B;
   image_view_create.components.a                    = VK_COMPONENT_SWIZZLE_A;
   image_view_create.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_DEPTH_BIT;
   image_view_create.subresourceRange.baseMipLevel   = 0;
   image_view_create.subresourceRange.levelCount     = 1;
   image_view_create.subresourceRange.baseArrayLayer = 0;
   image_view_create.subresourceRange.layerCount     = 1;
   if (VK_SUCCESS != vulkan_dev_dispatch->CreateImageView(gravitas_device_->GetVulkanDevice(),
                                                          &image_view_create,
                                                          nullptr,
                                                          &gravitas_depth_buffer_.vk_image_view))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating depth buffer image view";
      return false;
   }

   for (uint8_t iii = 0; iii < num_swapchain_images_; ++iii)
   {
      frames_[iii].index = iii;

      // Create a command pool for this frame
      if (VK_SUCCESS != vulkan_dev_dispatch->CreateCommandPool(
                          vk_device_, &cmd_pool_info, nullptr, &frames_[iii].primary_command_pool))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating frame " << iii << " command pool!";
         return false;
      }

      // Allocate a primary command buffer
      cmd_buf_info.commandPool = frames_[iii].primary_command_pool;
      if (VK_SUCCESS != vulkan_dev_dispatch->AllocateCommandBuffers(
                          vk_device_, &cmd_buf_info, &frames_[iii].primary_command_buffer))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating frame " << iii << " command buffer!";
         return false;
      }

      if (VK_SUCCESS !=
          vulkan_dev_dispatch->CreateFence(
            vk_device_, &fence_create_info, nullptr, &frames_[iii].command_buffer_usage_fence))
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed creating frame " << iii << " command buffer usage fence";
         return false;
      }

      if (VK_SUCCESS !=
          vulkan_dev_dispatch->CreateSemaphore(
            vk_device_, &semaphore_create_info, nullptr, &frames_[iii].frame_present_sem))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating frame " << iii << " present semaphore";
         return false;
      }

      if (VK_SUCCESS !=
          vulkan_dev_dispatch->CreateSemaphore(
            vk_device_, &semaphore_create_info, nullptr, &frames_[iii].frame_render_sem))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating frame " << iii << " render semaphore";
         return false;
      }
   }

   // Start with a color attachment.  This is the image we are writing commands
   // into and we want it to be ready to present this when we're done.
   VkAttachmentDescription attach_desc[2];
   attach_desc[0]         = {};
   attach_desc[0].format  = vk_swapchain_format_;
   attach_desc[0].samples = VK_SAMPLE_COUNT_1_BIT;
   attach_desc[0].loadOp  = VK_ATTACHMENT_LOAD_OP_CLEAR;  // Clear when attachment is loaded
   attach_desc[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE; // Keep attachment stored on
                                                          // renderpass ends
   attach_desc[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE; // Don't care about stencil when
                                                                   // attachment is loaded
   attach_desc[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; // Don't care about stencil
                                                                     // when renderpass ends
   attach_desc[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;         // Initial state is undefined
                                                                     // (or we don't care)
   attach_desc[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;     // When we're done, we want it
                                                                 // ready for presenting to display

   // Add a depth attachment.  This is the image we are writing commands
   // into and we want it to be ready to present this when we're done.
   attach_desc[1]         = {};
   attach_desc[1].format  = gravitas_depth_buffer_.vk_format;
   attach_desc[1].samples = VK_SAMPLE_COUNT_1_BIT;
   attach_desc[1].loadOp  = VK_ATTACHMENT_LOAD_OP_CLEAR;        // Clear when attachment is loaded
   attach_desc[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;       // Keep attachment stored on
                                                                // renderpass ends
   attach_desc[1].stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_CLEAR; // Clear when attachment is loaded
   attach_desc[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE; // Don't care about stencil
                                                                     // when renderpass ends
   attach_desc[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
   attach_desc[1].finalLayout   = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

   // Define the color attachment reference, when this starts, we expect to be in
   // color attach optimal state.
   VkAttachmentReference color_attach_ref = {};
   color_attach_ref.attachment            = 0;
   color_attach_ref.layout                = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

   // Only need to create one subpass
   VkSubpassDescription subpass_desc = {};
   subpass_desc.pipelineBindPoint =
     VK_PIPELINE_BIND_POINT_GRAPHICS;                        // Going to use a graphics pipeline
   subpass_desc.colorAttachmentCount    = 1;                 // One attachment
   subpass_desc.pColorAttachments       = &color_attach_ref; // The color attachment reference
   subpass_desc.pDepthStencilAttachment = &depth_attach_ref; // The depth/stencil attachment ref

   VkSubpassDependency subpass_deps[2];
   subpass_deps[0]               = {};
   subpass_deps[0].srcSubpass    = VK_SUBPASS_EXTERNAL;
   subpass_deps[0].dstSubpass    = 0;
   subpass_deps[0].srcStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
   subpass_deps[0].dstStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
   subpass_deps[0].srcAccessMask = 0;
   subpass_deps[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
   subpass_deps[1]               = {};
   subpass_deps[1].srcSubpass    = VK_SUBPASS_EXTERNAL;
   subpass_deps[1].dstSubpass    = 0;
   subpass_deps[1].srcStageMask =
     VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
   subpass_deps[1].dstStageMask =
     VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
   subpass_deps[1].srcAccessMask = 0;
   subpass_deps[1].dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

   VkRenderPassCreateInfo render_pass_create_info = {};
   render_pass_create_info.sType                  = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
   render_pass_create_info.pNext                  = nullptr;
   render_pass_create_info.attachmentCount        = 2;
   render_pass_create_info.pAttachments           = attach_desc;
   render_pass_create_info.subpassCount           = 1;
   render_pass_create_info.pSubpasses             = &subpass_desc;
   render_pass_create_info.dependencyCount        = 2;
   render_pass_create_info.pDependencies          = subpass_deps;

   // We only need one render pass total which can be used for every frame.  This is because it only
   // needs the format information for setting up what it needs to render to.
   if (VK_SUCCESS != vulkan_dev_dispatch->CreateRenderPass(
                       vk_device_, &render_pass_create_info, nullptr, &vk_render_pass_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating render pass";
      return false;
   }

   for (uint8_t iii = 0; iii < num_swapchain_images_; ++iii)
   {
      // Create one framebuffer for each swapchain image.
      // The frame buffer connects the renderpass to the swapchain images for
      // rendering into.
      VkImageView framebuf_attach[2];
      framebuf_attach[0]                              = gravitas_swapchain_->GetImageView(iii);
      framebuf_attach[1]                              = gravitas_depth_buffer_.vk_image_view;
      VkFramebufferCreateInfo framebuffer_create_info = {};
      framebuffer_create_info.sType                   = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
      framebuffer_create_info.pNext                   = nullptr;
      framebuffer_create_info.flags                   = 0;
      framebuffer_create_info.renderPass              = vk_render_pass_;
      framebuffer_create_info.attachmentCount         = 2;
      framebuffer_create_info.pAttachments            = framebuf_attach;
      framebuffer_create_info.width                   = window_extents_.width;
      framebuffer_create_info.height                  = window_extents_.height;
      framebuffer_create_info.layers                  = 1;

      // Going to create one framebuffer each one pointing at one swapchain image view and depth buf
      if (VK_SUCCESS != vulkan_dev_dispatch->CreateFramebuffer(
                          vk_device_, &framebuffer_create_info, nullptr, &frames_[iii].framebuffer))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating framebuffer";
         return false;
      }
   }

   return true;
}

void
CGravitasFrameManager::FreeVulkanFrameData()
{
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();

   vulkan_dev_dispatch->DeviceWaitIdle(vk_device_);

   // Destroy depth buffer items
   vulkan_dev_dispatch->DestroyImageView(vk_device_, gravitas_depth_buffer_.vk_image_view, nullptr);
   vmaDestroyImage(gravitas_device_->GetAllocator(),
                   gravitas_depth_buffer_.alloc_image.vk_image,
                   gravitas_depth_buffer_.alloc_image.vma_allocation);

   // Destroy swapchain items
   for (uint8_t iii = 0; iii < num_swapchain_images_; ++iii)
   {
      if (VK_NULL_HANDLE != frames_[iii].framebuffer)
      {
         vulkan_dev_dispatch->DestroyFramebuffer(vk_device_, frames_[iii].framebuffer, nullptr);
      }
   }
   if (VK_NULL_HANDLE != vk_render_pass_)
   {
      vulkan_dev_dispatch->DestroyRenderPass(vk_device_, vk_render_pass_, nullptr);
      vk_render_pass_ = VK_NULL_HANDLE;
   }
   for (uint8_t iii = 0; iii < num_swapchain_images_; ++iii)
   {
      vulkan_dev_dispatch->DestroySemaphore(vk_device_, frames_[iii].frame_present_sem, nullptr);
      vulkan_dev_dispatch->DestroySemaphore(vk_device_, frames_[iii].frame_render_sem, nullptr);
      vulkan_dev_dispatch->DestroyFence(
        vk_device_, frames_[iii].command_buffer_usage_fence, nullptr);
      vulkan_dev_dispatch->FreeCommandBuffers(
        vk_device_, frames_[iii].primary_command_pool, 1, &frames_[iii].primary_command_buffer);
      vulkan_dev_dispatch->DestroyCommandPool(
        vk_device_, frames_[iii].primary_command_pool, nullptr);
   }
   frames_.clear();
}

SFrameData *
CGravitasFrameManager::StartFrame()
{
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();

   VkResult wait_result = vulkan_dev_dispatch->WaitForFences(
     vk_device_, 1, &frames_[cur_frame_data_].command_buffer_usage_fence, true, 1000000000);
   if (VK_ERROR_DEVICE_LOST == wait_result)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Waiting fence for swapchain image revealed DEVICE_LOST!";
      return nullptr;
   }
   else if (VK_SUCCESS != wait_result)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed waiting fence for swapchain image " << cur_frame_data_ << "";
      return nullptr;
   }

   if (!gravitas_swapchain_->GetNextIndex(
         frames_[cur_frame_data_].frame_present_sem, 1000000000, &swapchain_index_))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed acquiring swapchain image index for " << (uint32_t)cur_frame_data_ << "";
      return nullptr;
   }

   if (VK_SUCCESS != vulkan_dev_dispatch->ResetFences(
                       vk_device_, 1, &frames_[cur_frame_data_].command_buffer_usage_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed resetting fence for swapchain image " << cur_frame_data_ << "";
      return nullptr;
   }

   if (VK_SUCCESS !=
       vulkan_dev_dispatch->ResetCommandBuffer(frames_[cur_frame_data_].primary_command_buffer, 0))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed resetting command buffer for swapchain image "
                                   << (uint32_t)cur_frame_data_ << "";
      return nullptr;
   }

   return &frames_[cur_frame_data_];
}

bool
CGravitasFrameManager::EndFrame()
{
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch = gravitas_device_->GetDispatchTable();

   std::vector<VkPipelineStageFlags> wait_stages;

   std::vector<VkSemaphore> wait_semaphores;
   wait_semaphores.push_back(frames_[cur_frame_data_].frame_present_sem);
   if (gravitas_device_->HasPendingTransferSemaphores())
   {
      gravitas_device_->CopyPendingTransferSemaphore(wait_semaphores);
   }

   wait_stages.resize(wait_semaphores.size());
   for (uint32_t stage = 0; stage < wait_semaphores.size(); ++stage)
   {
      wait_stages[stage] = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
   }
   VkSubmitInfo submit_info         = {};
   submit_info.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
   submit_info.pNext                = nullptr;
   submit_info.pWaitDstStageMask    = wait_stages.data();
   submit_info.waitSemaphoreCount   = static_cast<uint32_t>(wait_semaphores.size());
   submit_info.pWaitSemaphores      = wait_semaphores.data();
   submit_info.signalSemaphoreCount = 1;
   submit_info.pSignalSemaphores    = &frames_[cur_frame_data_].frame_render_sem;
   submit_info.commandBufferCount   = 1;
   submit_info.pCommandBuffers      = &frames_[cur_frame_data_].primary_command_buffer;

   if (VK_SUCCESS !=
       vulkan_dev_dispatch->QueueSubmit(gravitas_device_->GetGraphicsQueue(),
                                        1,
                                        &submit_info,
                                        frames_[cur_frame_data_].command_buffer_usage_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to submit primary command buffer to graphics queue";
      return false;
   }

   // this will put the image we just rendered into the visible window.
   // we want to wait on the _renderSemaphore for that,
   // as it's necessary that drawing commands have finished before the image is displayed to the
   // user
   VkPresentInfoKHR present_info = {};
   present_info.sType            = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
   present_info.pNext            = nullptr;

   VkSwapchainKHR vk_swapchain = gravitas_swapchain_->GetVkSwapchain();
   present_info.pSwapchains    = &vk_swapchain;
   present_info.swapchainCount = 1;

   present_info.pWaitSemaphores    = &frames_[cur_frame_data_].frame_render_sem;
   present_info.waitSemaphoreCount = 1;

   present_info.pImageIndices = &swapchain_index_;

   VkResult present_res =
     vulkan_dev_dispatch->QueuePresentKHR(gravitas_device_->GetGraphicsQueue(), &present_info);
   if (VK_SUCCESS != present_res)
   {
      if (VK_ERROR_OUT_OF_DATE_KHR == present_res)
      {
         CGravitasLog(GRAV_LOG_WARNING) << "Queue present failed because swapchain is out of date!";
      }
      else
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed to present queue contents to swapchain image";
      }
      return false;
   }
   cur_frame_data_ = ++cur_frame_data_ % num_swapchain_images_;

   return true;
}

void
CGravitasFrameManager::FreeSwapchain()
{
   if (nullptr != gravitas_swapchain_)
   {
      delete gravitas_swapchain_;
      gravitas_swapchain_ = nullptr;
   }
}

bool
CGravitasFrameManager::CreateSwapchain()
{
   // Free any current swapchain
   FreeSwapchain();

   // Create a new swapchain
   gravitas_swapchain_ = new CGravitasSwapchain(instance_owner_,
                                                &window_extents_,
                                                gravitas_device_,
                                                vk_swapchain_format_,
                                                num_swapchain_images_);
   if (nullptr == gravitas_swapchain_)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed allocating CGravitasSwapchain";
      return false;
   }
   swapchain_index_ = 0;
   cur_frame_data_  = 0;
   return true;
}
