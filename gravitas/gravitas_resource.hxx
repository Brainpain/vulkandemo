//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include <cstdint>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

const uint64_t GRAV_RESOURCE_RENDER_OBJECT = 0x0001000000000000ULL;
const uint64_t GRAV_RESOURCE_SHADER        = 0x0002000000000000ULL;
const uint64_t GRAV_RESOURCE_TEXTURE       = 0x0003000000000000ULL;
const uint64_t GRAV_RESOURCE_MATERIAL      = 0x0004000000000000ULL;
const uint64_t GRAV_RESOURCE_PIPELINE      = 0x0005000000000000ULL;
const uint64_t GRAV_RESOURCE_MASK          = 0xFFFF000000000000ULL;
const uint32_t GRAV_RESOURCE_SHIFT         = 12;

// Resource create structs

struct GravitasResourceCreateInfo
{
   uint64_t flag;
};

enum GravitasRenderObjectGenType
{
   RENDER_OBJECT_GEN_NOTHING = 0,
   RENDER_OBJECT_GEN_QUAD,
   RENDER_OBJECT_GEN_PYRAMID,
   RENDER_OBJECT_GEN_CUBE,
   RENDER_OBJECT_GEN_SPHERE,
};

class CGravitasScene;

struct GravitasRenderObjectResourceCreateInfo
{
   GravitasResourceCreateInfo  resource_info;
   std::string                 name;
   std::string                 directory_string;
   std::string                 file_string;
   CGravitasScene             *gravitas_scene;
   GravitasRenderObjectGenType gen_type = RENDER_OBJECT_GEN_NOTHING;
   glm::vec3                   scale    = {1.f, 1.f, 1.f};
   glm::vec3                   color    = {1.f, 1.f, 1.f};
   std::string                 texture_directory;
   std::string                 texture_file;
   std::string                 shader_directory;
   std::string                 vertex_shader;
   std::string                 fragment_shader;
   glm::mat4                   model_base_orienation = glm::mat4(1.f);
};

struct GravitasShaderResourceCreateInfo
{
   GravitasResourceCreateInfo resource_info;
   std::string                shader_name; // If a common shader, all other fields should be empty
   std::string                directory_string;
   std::string                file_string; // TODO: File if not included as header
   std::string                entrypoint;
   VkShaderStageFlagBits      shader_stage;
   std::vector<uint32_t>      shader_spirv;
};

struct GravitasTextureResourceCreateInfo
{
   GravitasResourceCreateInfo resource_info;
   std::string                name;
   std::string                directory_string;
   std::string                file_string;
   bool                       generate_mipmaps;
};

struct GravitasMaterialResourceCreateInfo
{
   GravitasResourceCreateInfo resource_info;
   std::string                name;
   glm::vec3                  ambient_color;
   glm::vec3                  diffuse_color;
   glm::vec3                  specular_color;
   glm::vec3                  emissive_color;
   std::vector<uint64_t>      shader_resource_ids;
   std::vector<uint64_t>      texture_resource_ids;
};

struct GravitasPipelineResourceCreateInfo
{
   GravitasResourceCreateInfo                     resource_info;
   uint64_t                                       material_resource_id;
   VkPipelineVertexInputStateCreateFlags          pipeline_vertex_input_state_flags;
   std::vector<VkVertexInputBindingDescription>   vertex_input_binding_descs;
   std::vector<VkVertexInputAttributeDescription> vertex_input_attrib_descs;
   VkRenderPass                                   render_pass = VK_NULL_HANDLE;
   VkPrimitiveTopology                vertex_topology         = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
   VkPolygonMode                      polygon_mode            = VK_POLYGON_MODE_FILL;
   VkCullModeFlags                    cull_mode               = VK_CULL_MODE_BACK_BIT;
   VkFrontFace                        front_face              = VK_FRONT_FACE_COUNTER_CLOCKWISE;
   bool                               enable_depth_test       = true;
   bool                               enable_depth_write      = true;
   VkCompareOp                        depth_compare           = VK_COMPARE_OP_LESS;
   float                              min_depth               = 0.f;
   float                              max_depth               = 1.f;
   VkViewport                         viewport;
   VkRect2D                           scissor;
   std::vector<VkDescriptorSetLayout> desc_set_layouts;
   std::vector<VkPushConstantRange>   push_constants;
};

// Resource structs

class CGravitasRenderObject;
class CGravitasTexture;

struct GravitasResource
{
   uint64_t resource_id        = 0;
   uint32_t imported_ref_count = 0;
   uint32_t upload_ref_count   = 0;
};

struct GravitasRenderObjectResource
{
   GravitasResource            resource {};
   std::string                 name;
   std::string                 directory_string;
   std::string                 file_string;
   GravitasRenderObjectGenType gen_type = RENDER_OBJECT_GEN_NOTHING;
   glm::vec3                   scale    = {1.f, 1.f, 1.f};
   glm::vec3                   color    = {1.f, 1.f, 1.f};
   std::string                 texture_directory;
   std::string                 texture_name;
   std::string                 shader_directory;
   glm::mat4                   model_base_orienation;
   CGravitasRenderObject      *render_object = nullptr;
};

struct GravitasShaderResource
{
   GravitasResource      resource {};
   std::string           shader_name;
   std::string           entrypoint;
   VkShaderStageFlagBits shader_stage;
   std::vector<uint32_t> shader_spirv;
   VkShaderModule        vk_module = VK_NULL_HANDLE;
};

struct GravitasTextureResource
{
   GravitasResource  resource {};
   std::string       name;
   std::string       directory_string;
   std::string       file_string;
   bool              generate_mipmaps;
   CGravitasTexture *texture = nullptr;
};

struct GravitasMaterialResource
{
   GravitasResource                       resource {};
   std::string                            name;
   glm::vec3                              ambient_color;
   glm::vec3                              diffuse_color;
   glm::vec3                              specular_color;
   glm::vec3                              emissive_color;
   std::vector<GravitasShaderResource *>  shaders;
   std::vector<GravitasTextureResource *> textures;
   VkSampler                              sampler         = VK_NULL_HANDLE;
   VkDescriptorSetLayout                  desc_set_layout = VK_NULL_HANDLE;
   VkDescriptorSet                        desc_set        = VK_NULL_HANDLE;
};

struct GravitasPipelineResource
{
   GravitasResource                               resource {};
   GravitasMaterialResource                      *material_resource;
   VkPipelineVertexInputStateCreateFlags          pipeline_vertex_input_state_flags;
   std::vector<VkVertexInputBindingDescription>   vertex_input_binding_descs;
   std::vector<VkVertexInputAttributeDescription> vertex_input_attrib_descs;
   VkRenderPass                                   render_pass = VK_NULL_HANDLE;
   VkPrimitiveTopology                vertex_topology         = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
   VkPolygonMode                      polygon_mode            = VK_POLYGON_MODE_FILL;
   VkCullModeFlags                    cull_mode               = VK_CULL_MODE_BACK_BIT;
   VkFrontFace                        front_face              = VK_FRONT_FACE_COUNTER_CLOCKWISE;
   bool                               enable_depth_test       = true;
   bool                               enable_depth_write      = true;
   VkCompareOp                        depth_compare           = VK_COMPARE_OP_LESS;
   float                              min_depth               = 0.f;
   float                              max_depth               = 1.f;
   VkViewport                         viewport;
   VkRect2D                           scissor;
   std::vector<VkDescriptorSetLayout> desc_set_layouts;
   std::vector<VkPushConstantRange>   push_constants;
   VkPipelineLayout                   vk_layout   = VK_NULL_HANDLE;
   VkPipeline                         vk_pipeline = VK_NULL_HANDLE;
};
