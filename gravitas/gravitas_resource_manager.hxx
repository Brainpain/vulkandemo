//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_frame_manager.hxx"
#include "gravitas_resource.hxx"

class CGravitasDevice;
class IGravitasWindowOwner;
class IVulkanInstanceOwner;
struct GravitasDeviceDispatchTable;
class CGravitasPipelineLibrary;
class CGravitasMaterialLibrary;
class CGravitasRenderObjectLibrary;
class CGravitasShaderLibrary;
class CGravitasTextureLibrary;

class CGravitasResourceManager
{
 public:
   CGravitasResourceManager(CGravitasDevice            *gravitas_device,
                            const IGravitasWindowOwner *window_owner,
                            const IVulkanInstanceOwner *instance_owner);
   virtual ~CGravitasResourceManager();

   CGravitasFrameManager *GetFrameManager()
   {
      return frame_manager_;
   }

   bool CreateResource(GravitasResourceCreateInfo *create_info, uint64_t &resource_id);
   bool ReleaseResource(uint64_t resource_id);

   bool GetPipelineResource(uint64_t resource_id, GravitasPipelineResource **resource);
   bool GetMaterialResource(uint64_t resource_id, GravitasMaterialResource **resource);
   bool GetRenderObjectResource(uint64_t resource_id, GravitasRenderObjectResource **resource);
   bool GetShaderResource(uint64_t resource_id, GravitasShaderResource **resource);
   bool GetTextureResource(uint64_t resource_id, GravitasTextureResource **resource);

   bool UploadResource(uint64_t resource_id);
   bool UnloadResource(uint64_t resource_id);

   bool IncrementImportRef(uint64_t resource_id);
   bool DecrementImportRef(uint64_t resource_id);

   bool ResetBeforeResize();
   bool RestoreAfterResize();

 private:
   bool            InitFrameManager();
   inline uint64_t GetResourceFlag(uint64_t resource_id)
   {
      return (resource_id & GRAV_RESOURCE_MASK);
   }

   CGravitasDevice                   *gravitas_device_     = nullptr;
   const GravitasDeviceDispatchTable *vulkan_dev_dispatch_ = nullptr;
   const IGravitasWindowOwner        *window_owner_        = nullptr;
   const IVulkanInstanceOwner        *instance_owner_      = nullptr;
   CGravitasFrameManager             *frame_manager_       = nullptr;
   CGravitasMaterialLibrary          *material_lib_        = nullptr;
   CGravitasRenderObjectLibrary      *render_object_lib_   = nullptr;
   CGravitasPipelineLibrary          *pipeline_lib_        = nullptr;
   CGravitasShaderLibrary            *shader_lib_          = nullptr;
   CGravitasTextureLibrary           *texture_lib_         = nullptr;
};
