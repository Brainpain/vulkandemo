//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_texture_library.hxx"

#include "gravitas_device.hxx"
#include "gravitas_device_dispatch.hxx"
#include "gravitas_log.hxx"
#include "gravitas_resource_manager.hxx"
#include "gravitas_texture.hxx"

#include <iostream>
#include <sstream>

CGravitasTextureLibrary::CGravitasTextureLibrary(CGravitasResourceManager *resource_mgr,
                                                 CGravitasDevice          *gravitas_device)
  : CGravitasResourceLibrary(resource_mgr, gravitas_device, GRAV_RESOURCE_TEXTURE)
{}

CGravitasTextureLibrary::~CGravitasTextureLibrary()
{
   FreeInternals();
}

bool
CGravitasTextureLibrary::FindMatchingTexture(const GravitasTextureResourceCreateInfo *create_info,
                                             uint64_t                                &resource_id)
{
   bool found = false;
   for (auto &resource : resources_)
   {
      GravitasTextureResource *texture_resource =
        reinterpret_cast<GravitasTextureResource *>(resource.second);
      if (create_info->directory_string == texture_resource->directory_string &&
          create_info->file_string == texture_resource->file_string &&
          create_info->generate_mipmaps == texture_resource->generate_mipmaps)
      {
         resource_id = texture_resource->resource.resource_id;
         return true;
      }
   }

   return false;
}

// Finds if already present and bumps up ref-counts.  If not present, creates and sets ref-count
// to 1 before returning values.
bool
CGravitasTextureLibrary::AddTexture(const GravitasTextureResourceCreateInfo *create_info,
                                    uint64_t                                &resource_id)
{
   bool                     success          = false;
   GravitasTextureResource *texture_resource = nullptr;

   resource_id = 0ULL;
   if (!FindMatchingTexture(create_info, resource_id))
   {
      texture_resource = new GravitasTextureResource();
      if (nullptr == texture_resource)
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed creating texture resource for " << create_info->name;
         goto out;
      }
      texture_resource->texture =
        new CGravitasTexture(gravitas_device_, create_info->generate_mipmaps);
      if (nullptr == texture_resource)
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating texture for " << create_info->name;
         goto out;
      }

      if (!create_info->file_string.empty())
      {
         std::string full_path = create_info->directory_string + '/' + create_info->file_string;
         if (!texture_resource->texture->LoadFromFile(full_path))
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Failed loading texture " << create_info->name << " file " << full_path;
            goto out;
         }
      }
      else
      {
         CGravitasLog(GRAV_LOG_ERROR) << "No texture data for texture " << create_info->name;
         goto out;
      }

      texture_resource->resource.imported_ref_count = 0;
      texture_resource->resource.upload_ref_count   = 0;
      texture_resource->name                        = create_info->name;
      texture_resource->directory_string            = create_info->directory_string;
      texture_resource->file_string                 = create_info->file_string;
      texture_resource->generate_mipmaps            = create_info->generate_mipmaps;
      if (!AddResource(&texture_resource->resource, resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed adding texture resource";
         goto out;
      }
   }

   ImportRef(resource_id);
   success = true;

out:

   return success;
}

// Drops ref-count, and deletes if 0.
bool
CGravitasTextureLibrary::RemoveTexture(uint64_t &resource_id)
{
   VkDevice           vk_dev = gravitas_device_->GetVulkanDevice();
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed releasing texture resource " << outstream_uint64.str()
                                   << " because it does not exist!";
      return false;
   }
   GravitasTextureResource *texture_resource =
     reinterpret_cast<GravitasTextureResource *>(resources_[resource_id]);
   if (!ImportUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed decrementing import ref for texture resource " << outstream_uint64.str() << "!";
      return false;
   }

   if (texture_resource->resource.imported_ref_count == 0)
   {
      delete texture_resource->texture;
      delete texture_resource;
      resources_.erase(resource_id);
   }
   return true;
}

void
CGravitasTextureLibrary::FreeInternals()
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   for (auto &resource : resources_)
   {
      GravitasTextureResource *texture_resource =
        reinterpret_cast<GravitasTextureResource *>(resource.second);
      delete texture_resource->texture;
      delete texture_resource;
   }
   resources_.clear();
}

bool
CGravitasTextureLibrary::Upload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed uploading texture resource " << outstream_uint64.str()
                                   << " because it does not exist!";
      return false;
   }
   GravitasTextureResource *texture_resource =
     reinterpret_cast<GravitasTextureResource *>(resources_[resource_id]);
   if (texture_resource->resource.upload_ref_count == 0)
   {
      if (!texture_resource->texture->Upload())
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed uploading texture resource " << outstream_uint64.str() << "!";
         return false;
      }
   }
   return UploadRef(resource_id);
}

bool
CGravitasTextureLibrary::Unload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (!UploadUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed unloading texture resource " << outstream_uint64.str() << "!";
   }
   GravitasTextureResource *texture_resource =
     reinterpret_cast<GravitasTextureResource *>(resources_[resource_id]);
   if (texture_resource->resource.upload_ref_count == 0)
   {
      texture_resource->texture->Unload();
   }
   return true;
}
