//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_pipeline_library.hxx"

#include "gravitas_device.hxx"
#include "gravitas_device_dispatch.hxx"
#include "gravitas_log.hxx"
#include "gravitas_resource_manager.hxx"

#include <iostream>
#include <sstream>

CGravitasPipelineLibrary::CGravitasPipelineLibrary(CGravitasResourceManager *resource_mgr,
                                                   CGravitasDevice          *gravitas_device)
  : CGravitasResourceLibrary(resource_mgr, gravitas_device, GRAV_RESOURCE_PIPELINE)
{}

CGravitasPipelineLibrary::~CGravitasPipelineLibrary()
{
   FreeInternals();
}

bool
CGravitasPipelineLibrary::BindingDescsMatch(
  const std::vector<VkVertexInputBindingDescription> &binding1,
  const std::vector<VkVertexInputBindingDescription> &binding2)
{
   bool match = false;
   if (binding1.size() == binding2.size())
   {
      bool all_found = true;
      for (uint32_t b1 = 0; b1 < static_cast<uint32_t>(binding1.size()); ++b1)
      {
         bool found = false;
         for (uint32_t b2 = 0; b2 < static_cast<uint32_t>(binding2.size()); ++b2)
         {
            if (binding1[b1].binding == binding2[b2].binding &&
                binding1[b1].inputRate == binding2[b2].inputRate &&
                binding1[b1].stride == binding2[b2].stride)
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            all_found = false;
            break;
         }
      }
      if (all_found)
      {
         match = true;
      }
   }
   return match;
}

bool
CGravitasPipelineLibrary::AttribDescsMatch(
  const std::vector<VkVertexInputAttributeDescription> &attrib1,
  const std::vector<VkVertexInputAttributeDescription> &attrib2)
{
   bool match = false;
   if (attrib1.size() == attrib2.size())
   {
      bool all_found = true;
      for (uint32_t a1 = 0; a1 < static_cast<uint32_t>(attrib1.size()); ++a1)
      {
         bool found = false;
         for (uint32_t a2 = 0; a2 < static_cast<uint32_t>(attrib2.size()); ++a2)
         {
            if (attrib1[a1].binding == attrib2[a2].binding &&
                attrib1[a1].format == attrib2[a2].format &&
                attrib1[a1].location == attrib2[a2].location &&
                attrib1[a1].offset == attrib2[a2].offset)
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            all_found = false;
            break;
         }
      }
      if (all_found)
      {
         match = true;
      }
   }
   return match;
}

bool
CGravitasPipelineLibrary::DoMaterialsMatch(const GravitasMaterialResource *res1,
                                           const GravitasMaterialResource *res2)
{
   if (res1 == res2)
   {
      return true;
   }
   else if (res1->shaders.size() == res2->shaders.size() &&
            res1->textures.size() == res2->textures.size())
   {
      for (uint32_t shdr1 = 0; shdr1 < res1->shaders.size(); ++shdr1)
      {
         bool matches = false;
         for (uint32_t shdr2 = 0; shdr2 < res2->shaders.size(); ++shdr2)
         {
            if (res1->shaders[shdr1]->vk_module == res2->shaders[shdr2]->vk_module &&
                res1->shaders[shdr1]->entrypoint == res2->shaders[shdr2]->entrypoint &&
                res1->shaders[shdr1]->shader_stage == res2->shaders[shdr2]->shader_stage)
            {
               matches = true;
               break;
            }
         }
         if (!matches)
         {
            return false;
         }
      }
      return true;
   }
   return false;
}

bool
CGravitasPipelineLibrary::FindMatchingPipeline(
  const GravitasPipelineResourceCreateInfo *create_info, uint64_t &resource_id)
{
   bool found = false;
   for (auto &resource : resources_)
   {
      GravitasPipelineResource *pipeline_resource =
        reinterpret_cast<GravitasPipelineResource *>(resource.second);
      if (pipeline_resource->pipeline_vertex_input_state_flags ==
            create_info->pipeline_vertex_input_state_flags &&
          pipeline_resource->render_pass == create_info->render_pass &&
          pipeline_resource->vertex_topology == create_info->vertex_topology &&
          pipeline_resource->polygon_mode == create_info->polygon_mode &&
          pipeline_resource->cull_mode == create_info->cull_mode &&
          pipeline_resource->front_face == create_info->front_face &&
          pipeline_resource->enable_depth_test == create_info->enable_depth_test &&
          pipeline_resource->enable_depth_write == create_info->enable_depth_write &&
          pipeline_resource->depth_compare == create_info->depth_compare &&
          pipeline_resource->min_depth == create_info->min_depth &&
          pipeline_resource->max_depth == create_info->max_depth &&
          pipeline_resource->viewport.height == create_info->viewport.height &&
          pipeline_resource->viewport.width == create_info->viewport.width &&
          pipeline_resource->viewport.x == create_info->viewport.x &&
          pipeline_resource->viewport.y == create_info->viewport.y &&
          pipeline_resource->viewport.maxDepth == create_info->viewport.maxDepth &&
          pipeline_resource->viewport.minDepth == create_info->viewport.minDepth &&
          pipeline_resource->scissor.extent.width == create_info->scissor.extent.width &&
          pipeline_resource->scissor.extent.height == create_info->scissor.extent.height &&
          pipeline_resource->scissor.offset.x == create_info->scissor.offset.x &&
          pipeline_resource->scissor.offset.y == create_info->scissor.offset.y &&
          pipeline_resource->desc_set_layouts.size() == create_info->desc_set_layouts.size() &&
          pipeline_resource->push_constants.size() == create_info->push_constants.size())
      {
         bool matches = false;
         for (uint32_t dsl1 = 0;
              dsl1 < static_cast<uint32_t>(pipeline_resource->desc_set_layouts.size());
              ++dsl1)
         {
            for (uint32_t dsl2 = 0; dsl2 < create_info->desc_set_layouts.size(); ++dsl2)
            {
               if (pipeline_resource->desc_set_layouts[dsl1] == create_info->desc_set_layouts[dsl2])
               {
                  matches = true;
                  break;
               }
            }
         }
         if (!matches)
         {
            continue;
         }
         matches = false;
         for (uint32_t pc1 = 0;
              pc1 < static_cast<uint32_t>(pipeline_resource->push_constants.size());
              ++pc1)
         {
            for (uint32_t pc2 = 0; pc2 < create_info->push_constants.size(); ++pc2)
            {
               if (pipeline_resource->push_constants[pc1].stageFlags ==
                     create_info->push_constants[pc2].stageFlags &&
                   pipeline_resource->push_constants[pc1].offset ==
                     create_info->push_constants[pc2].offset &&
                   pipeline_resource->push_constants[pc1].size ==
                     create_info->push_constants[pc2].size)
               {
                  matches = true;
                  break;
               }
            }
         }
         if (!matches)
         {
            continue;
         }
         GravitasMaterialResource *material_resource = nullptr;
         if (BindingDescsMatch(pipeline_resource->vertex_input_binding_descs,
                               create_info->vertex_input_binding_descs) &&
             AttribDescsMatch(pipeline_resource->vertex_input_attrib_descs,
                              create_info->vertex_input_attrib_descs) &&
             resource_manager_->GetMaterialResource(create_info->material_resource_id,
                                                    &material_resource) &&
             DoMaterialsMatch(pipeline_resource->material_resource, material_resource))
         {
            resource_id = pipeline_resource->resource.resource_id;
            return true;
         }
      }
   }
   return false;
}

// Finds if already present and bumps up ref-counts.  If not present, creates and sets ref-count
// to 1 before returning values.
bool
CGravitasPipelineLibrary::AddPipeline(const GravitasPipelineResourceCreateInfo *create_info,
                                      uint64_t                                 &resource_id)
{
   bool success = false;

   resource_id = 0ULL;
   if (!FindMatchingPipeline(create_info, resource_id))
   {
      GravitasPipelineResource *pipeline_resource = new GravitasPipelineResource();
      if (nullptr == pipeline_resource)
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating pipeline resource";
         goto out;
      }
      // Don't just compare material directly, compare info inside of each
      if (!resource_manager_->GetMaterialResource(create_info->material_resource_id,
                                                  &pipeline_resource->material_resource))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed querying material resource for pipeline";
         goto out;
      }

      VkPipelineLayout vk_layout   = VK_NULL_HANDLE;
      VkPipeline       vk_pipeline = VK_NULL_HANDLE;

      // We now need to build a pipeline and layout with the information we have in the pipeline
      // description
      assert(create_info->vertex_input_binding_descs.size() > 0);
      assert(create_info->vertex_input_attrib_descs.size() > 0);

      // Define the vertex input state (contains the equivalent of the VAO in OpenGL).
      // This includes the vertex buffer and vertex format information.vertex_input_desc
      // Initialize it to empty for now.
      VkPipelineVertexInputStateCreateInfo vis_create = {};
      vis_create.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
      vis_create.pNext = nullptr;
      vis_create.flags = create_info->pipeline_vertex_input_state_flags;
      vis_create.vertexBindingDescriptionCount =
        static_cast<uint32_t>(create_info->vertex_input_binding_descs.size());
      vis_create.pVertexBindingDescriptions = create_info->vertex_input_binding_descs.data();
      vis_create.vertexAttributeDescriptionCount =
        static_cast<uint32_t>(create_info->vertex_input_attrib_descs.size());
      vis_create.pVertexAttributeDescriptions = create_info->vertex_input_attrib_descs.data();

      // Define how vertex data is configured
      VkPipelineInputAssemblyStateCreateInfo ias_create = {};
      ias_create.sType    = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
      ias_create.pNext    = nullptr;
      ias_create.flags    = 0;
      ias_create.topology = create_info->vertex_topology;
      ias_create.primitiveRestartEnable = VK_FALSE; // No primitive restart for now

      // Define the fixed function rasterization state
      VkPipelineRasterizationStateCreateInfo ras_create = {};
      ras_create.sType            = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
      ras_create.pNext            = nullptr;
      ras_create.flags            = 0;
      ras_create.depthClampEnable = VK_FALSE;        // no clamp by default
      ras_create.rasterizerDiscardEnable = VK_FALSE; // no discard by default
      ras_create.polygonMode             = create_info->polygon_mode;
      ras_create.cullMode                = create_info->cull_mode;
      ras_create.frontFace               = create_info->front_face;
      ras_create.depthBiasEnable         = VK_FALSE; // no bias by default
      ras_create.depthBiasConstantFactor = 0.0f;
      ras_create.depthBiasClamp          = 0.0f;
      ras_create.depthBiasSlopeFactor    = 0.0f;
      ras_create.lineWidth               = 1.0f; // default to 1 pixel line width

      // Define multisample state
      VkPipelineMultisampleStateCreateInfo mss_create = {};
      mss_create.sType                 = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
      mss_create.pNext                 = nullptr;
      mss_create.flags                 = 0;
      mss_create.rasterizationSamples  = VK_SAMPLE_COUNT_1_BIT; // default disable multisample
      mss_create.sampleShadingEnable   = VK_FALSE;
      mss_create.minSampleShading      = 1.0f;
      mss_create.pSampleMask           = nullptr;
      mss_create.alphaToCoverageEnable = VK_FALSE;
      mss_create.alphaToOneEnable      = VK_FALSE;

      // Define default color blend state (we want to output all colors with no blending by default)
      VkPipelineColorBlendAttachmentState cbas_create = {};
      cbas_create.blendEnable                         = VK_FALSE; // Disable blend by default
      cbas_create.srcColorBlendFactor                 = VK_BLEND_FACTOR_ZERO;
      cbas_create.dstColorBlendFactor                 = VK_BLEND_FACTOR_ZERO;
      cbas_create.colorBlendOp                        = VK_BLEND_OP_ADD;
      cbas_create.srcAlphaBlendFactor                 = VK_BLEND_FACTOR_ZERO;
      cbas_create.dstAlphaBlendFactor                 = VK_BLEND_FACTOR_ZERO;
      cbas_create.alphaBlendOp                        = VK_BLEND_OP_ADD;
      cbas_create.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
                                   VK_COLOR_COMPONENT_B_BIT |
                                   VK_COLOR_COMPONENT_A_BIT; // Write out all colors
      VkPipelineColorBlendStateCreateInfo cbs_create = {};
      cbs_create.sType             = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
      cbs_create.pNext             = nullptr;
      cbs_create.flags             = 0;
      cbs_create.logicOpEnable     = VK_FALSE;         // default to no logic op
      cbs_create.logicOp           = VK_LOGIC_OP_COPY; // default to copy
      cbs_create.attachmentCount   = 1; // default to one attachment (the above struct)
      cbs_create.pAttachments      = &cbas_create;
      cbs_create.blendConstants[0] = 0.0f;
      cbs_create.blendConstants[1] = 0.0f;
      cbs_create.blendConstants[2] = 0.0f;
      cbs_create.blendConstants[3] = 0.0f;

      VkPipelineDepthStencilStateCreateInfo dss_create = {};
      dss_create.sType                 = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
      dss_create.pNext                 = nullptr;
      dss_create.depthTestEnable       = create_info->enable_depth_test;
      dss_create.depthWriteEnable      = create_info->enable_depth_write;
      dss_create.depthCompareOp        = create_info->depth_compare;
      dss_create.depthBoundsTestEnable = VK_FALSE;
      dss_create.stencilTestEnable     = VK_FALSE;
      dss_create.minDepthBounds        = create_info->min_depth;
      dss_create.maxDepthBounds        = create_info->max_depth;

      // Define the default viewport state
      VkPipelineViewportStateCreateInfo vs_create = {};
      vs_create.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
      vs_create.pNext         = nullptr;
      vs_create.flags         = 0;
      vs_create.viewportCount = 1;
      vs_create.pViewports    = &create_info->viewport;
      vs_create.scissorCount  = 1;
      vs_create.pScissors     = &create_info->scissor;

      std::vector<VkPipelineShaderStageCreateInfo> ss_creates;
      for (uint32_t shader = 0; shader < pipeline_resource->material_resource->shaders.size();
           ++shader)
      {
         VkPipelineShaderStageCreateInfo shader_info = {};
         shader_info.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
         shader_info.pNext  = nullptr;
         shader_info.flags  = 0;
         shader_info.stage  = pipeline_resource->material_resource->shaders[shader]->shader_stage;
         shader_info.module = pipeline_resource->material_resource->shaders[shader]->vk_module;
         shader_info.pName =
           pipeline_resource->material_resource->shaders[shader]->entrypoint.c_str();
         ss_creates.push_back(shader_info);
      }

      VkDevice vk_dev = gravitas_device_->GetVulkanDevice();

      std::vector<VkDescriptorSetLayout> all_desc_set_layouts;
      for (auto &ci_dsl : create_info->desc_set_layouts)
      {
         all_desc_set_layouts.push_back(ci_dsl);
      }
      if (VK_NULL_HANDLE != pipeline_resource->material_resource->desc_set_layout)
      {
         all_desc_set_layouts.push_back(pipeline_resource->material_resource->desc_set_layout);
      }
      // Simple non-intrusive pipeline layout
      VkPipelineLayoutCreateInfo pipeline_layout_create = {};
      pipeline_layout_create.sType          = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
      pipeline_layout_create.pNext          = nullptr;
      pipeline_layout_create.flags          = 0;
      pipeline_layout_create.setLayoutCount = static_cast<uint32_t>(all_desc_set_layouts.size());
      pipeline_layout_create.pSetLayouts    = all_desc_set_layouts.data();
      pipeline_layout_create.pushConstantRangeCount =
        static_cast<uint32_t>(create_info->push_constants.size());
      pipeline_layout_create.pPushConstantRanges = create_info->push_constants.data();
      if (VK_SUCCESS != vulkan_dev_dispatch_->CreatePipelineLayout(
                          vk_dev, &pipeline_layout_create, nullptr, &vk_layout))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating pipeline layout!";
         goto out;
      }

      VkGraphicsPipelineCreateInfo create = {};
      create.sType                        = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
      create.pNext                        = nullptr;
      create.flags                        = 0;
      create.stageCount                   = (uint32_t)(ss_creates.size());
      create.pStages                      = ss_creates.data();
      create.pVertexInputState            = &vis_create;
      create.pInputAssemblyState          = &ias_create;
      create.pTessellationState           = nullptr;
      create.pViewportState               = &vs_create;
      create.pRasterizationState          = &ras_create;
      create.pMultisampleState            = &mss_create;
      create.pDepthStencilState           = &dss_create;
      create.pColorBlendState             = &cbs_create;
      create.pDynamicState                = nullptr;
      create.layout                       = vk_layout;
      create.renderPass                   = create_info->render_pass;
      create.subpass                      = 0;
      create.basePipelineHandle           = VK_NULL_HANDLE;
      create.basePipelineIndex            = 0;

      // Don't support pipeline cache for now.
      if (VK_SUCCESS != vulkan_dev_dispatch_->CreateGraphicsPipelines(
                          vk_dev, VK_NULL_HANDLE, 1, &create, nullptr, &vk_pipeline))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating graphics pipeline";
         goto out;
      }
      pipeline_resource->vertex_input_binding_descs.resize(
        create_info->vertex_input_binding_descs.size());
      for (uint32_t b1 = 0;
           b1 < static_cast<uint32_t>(create_info->vertex_input_binding_descs.size());
           ++b1)
      {
         pipeline_resource->vertex_input_binding_descs[b1].binding =
           create_info->vertex_input_binding_descs[b1].binding;
         pipeline_resource->vertex_input_binding_descs[b1].inputRate =
           create_info->vertex_input_binding_descs[b1].inputRate;
         pipeline_resource->vertex_input_binding_descs[b1].stride =
           create_info->vertex_input_binding_descs[b1].stride;
      }
      pipeline_resource->vertex_input_attrib_descs.resize(
        create_info->vertex_input_attrib_descs.size());
      for (uint32_t a1 = 0;
           a1 < static_cast<uint32_t>(create_info->vertex_input_attrib_descs.size());
           ++a1)
      {
         pipeline_resource->vertex_input_attrib_descs[a1].binding =
           create_info->vertex_input_attrib_descs[a1].binding;
         pipeline_resource->vertex_input_attrib_descs[a1].format =
           create_info->vertex_input_attrib_descs[a1].format;
         pipeline_resource->vertex_input_attrib_descs[a1].location =
           create_info->vertex_input_attrib_descs[a1].location;
         pipeline_resource->vertex_input_attrib_descs[a1].offset =
           create_info->vertex_input_attrib_descs[a1].offset;
      }
      pipeline_resource->resource.imported_ref_count = 0;
      pipeline_resource->resource.upload_ref_count   = 0;
      pipeline_resource->pipeline_vertex_input_state_flags =
        create_info->pipeline_vertex_input_state_flags;
      pipeline_resource->render_pass        = create_info->render_pass;
      pipeline_resource->vertex_topology    = create_info->vertex_topology;
      pipeline_resource->polygon_mode       = create_info->polygon_mode;
      pipeline_resource->cull_mode          = create_info->cull_mode;
      pipeline_resource->front_face         = create_info->front_face;
      pipeline_resource->enable_depth_test  = create_info->enable_depth_test;
      pipeline_resource->enable_depth_write = create_info->enable_depth_write;
      pipeline_resource->depth_compare      = create_info->depth_compare;
      pipeline_resource->min_depth          = create_info->min_depth;
      pipeline_resource->max_depth          = create_info->max_depth;
      pipeline_resource->vk_layout          = vk_layout;
      pipeline_resource->vk_pipeline        = vk_pipeline;
      pipeline_resource->viewport           = create_info->viewport;
      pipeline_resource->scissor            = create_info->scissor;
      pipeline_resource->desc_set_layouts.resize(create_info->desc_set_layouts.size());
      for (uint32_t dsl = 0; dsl < static_cast<uint32_t>(create_info->desc_set_layouts.size());
           ++dsl)
      {
         pipeline_resource->desc_set_layouts[dsl] = create_info->desc_set_layouts[dsl];
      }
      pipeline_resource->push_constants.resize(create_info->push_constants.size());
      for (uint32_t pc = 0; pc < static_cast<uint32_t>(create_info->push_constants.size()); ++pc)
      {
         pipeline_resource->push_constants[pc] = create_info->push_constants[pc];
      }
      if (!AddResource(&pipeline_resource->resource, resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed adding pipeline resource";
         goto out;
      }
   }

   ImportRef(resource_id);
   success = true;

out:

   return success;
}

// Drops ref-count, and deletes if 0.
bool
CGravitasPipelineLibrary::RemovePipeline(uint64_t &resource_id)
{
   VkDevice           vk_dev = gravitas_device_->GetVulkanDevice();
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed releasing pipeline resource "
                                   << outstream_uint64.str() << " because it does not exist!";
      return false;
   }
   GravitasPipelineResource *pipeline_resource =
     reinterpret_cast<GravitasPipelineResource *>(resources_[resource_id]);

   if (!ImportUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed decrementing import ref for pipeline resource " << outstream_uint64.str() << "!";
      return false;
   }

   if (pipeline_resource->resource.imported_ref_count == 0)
   {
      vulkan_dev_dispatch_->DestroyPipelineLayout(vk_dev, pipeline_resource->vk_layout, nullptr);
      vulkan_dev_dispatch_->DestroyPipeline(vk_dev, pipeline_resource->vk_pipeline, nullptr);
      delete pipeline_resource;
      resources_.erase(resource_id);
   }
   return true;
}

void
CGravitasPipelineLibrary::FreeInternals()
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   VkDevice                     vk_dev = gravitas_device_->GetVulkanDevice();
   for (auto &resource : resources_)
   {
      GravitasPipelineResource *pipeline_resource =
        reinterpret_cast<GravitasPipelineResource *>(resource.second);
      vulkan_dev_dispatch_->DestroyPipelineLayout(vk_dev, pipeline_resource->vk_layout, nullptr);
      vulkan_dev_dispatch_->DestroyPipeline(vk_dev, pipeline_resource->vk_pipeline, nullptr);
      delete pipeline_resource;
   }
   resources_.clear();
}

bool
CGravitasPipelineLibrary::Upload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed uploading pipeline resource "
                                   << outstream_uint64.str() << " because it does not exist!";
      return false;
   }
   return UploadRef(resource_id);
}

bool
CGravitasPipelineLibrary::Unload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (!UploadUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed unloading pipeline resource " << outstream_uint64.str() << "!";
   }
   return true;
}
