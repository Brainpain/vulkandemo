//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_allocations.hxx"

#include <string>
#include <vector>

class CGravitasDevice;

class CGravitasTexture
{
 public:
   CGravitasTexture(CGravitasDevice *device, bool generate_mipmaps);
   ~CGravitasTexture();

   uint32_t GetNumberMipLevels()
   {
      return num_mipmap_levels_;
   }
   bool Upload();
   void Unload();
   bool HasBeenUploaded() const
   {
      return uploaded_;
   }

   uint32_t GetWidth() const
   {
      return width_;
   }
   uint32_t GetHeight() const
   {
      return height_;
   }
   uint32_t GetNumComponents() const
   {
      return num_components_;
   }
   VkImage GetVkImage()
   {
      return texture_image_.vk_image;
   }
   VkImageView GetVkImageView()
   {
      return texture_image_view_;
   }
   void SetTransferUploadComplete(bool uploaded);

   bool                   LoadFromFile(const std::string &filename);
   const CGravitasDevice *Device()
   {
      return gravitas_device_;
   }

 private:
   uint32_t CalculateMipLevels();

   std::string             name_;
   CGravitasDevice        *gravitas_device_   = nullptr;
   bool                    generate_mipmaps_  = false;
   uint32_t                num_mipmap_levels_ = 1;
   GravitasAllocatedImage  texture_image_;
   VkImageView             texture_image_view_ = VK_NULL_HANDLE;
   GravitasAllocatedBuffer texture_transfer_buffer_;
   uint32_t                width_          = 0;
   uint32_t                height_         = 0;
   uint8_t                 num_components_ = 0;
   void                   *data_           = nullptr;
   bool                    allocated_      = false;
   bool                    uploaded_       = false;
};
