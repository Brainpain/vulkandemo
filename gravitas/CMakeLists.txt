# clang-format off
add_library(gravitas STATIC "")

target_sources(
    gravitas PRIVATE
        gravitas_allocations.hxx
        gravitas_allocations.cxx
        gravitas_app.hxx
        gravitas_app.cxx
        gravitas_base_types.hxx
        gravitas_camera.hxx
        gravitas_camera.cxx
        gravitas_device_dispatch.hxx
        gravitas_device_dispatch.cxx
        gravitas_device.hxx
        gravitas_device.cxx
        gravitas_frame_manager.hxx
        gravitas_frame_manager.cxx
        gravitas_log.hxx
        gravitas_material_library.hxx
        gravitas_material_library.cxx
        gravitas_pipeline_library.hxx
        gravitas_pipeline_library.cxx
        gravitas_render_obj_library.hxx
        gravitas_render_obj_library.cxx
        gravitas_render_object.hxx
        gravitas_render_object.cxx
        gravitas_resource_manager.hxx
        gravitas_resource_manager.cxx
        gravitas_resource_library.hxx
        gravitas_resource_library.cxx
        gravitas_scene.hxx
        gravitas_scene.cxx
        gravitas_shader_library.hxx
        gravitas_shader_library.cxx
        gravitas_swapchain.hxx
        gravitas_swapchain.cxx
        gravitas_texture.hxx
        gravitas_texture.cxx
        gravitas_texture_library.hxx
        gravitas_texture_library.cxx
        gravitas_window.hxx
        gravitas_window.cxx
    )

target_link_libraries(
    gravitas PUBLIC
        ${SDL2_LIBRARIES}
        ${Vulkan_LIBRARY}
    )
