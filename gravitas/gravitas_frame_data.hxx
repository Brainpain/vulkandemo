//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include <vulkan/vk_platform.h>
#include <vulkan/vulkan_core.h>

// FrameData and utility methods
struct SFrameData
{
   uint8_t         index;
   VkSemaphore     frame_present_sem;
   VkSemaphore     frame_render_sem;
   VkFence         command_buffer_usage_fence;
   VkCommandPool   primary_command_pool;
   VkCommandBuffer primary_command_buffer;
   VkFramebuffer   framebuffer;
};
