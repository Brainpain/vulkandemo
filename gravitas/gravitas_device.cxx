//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_device.hxx"

#include "gravitas_log.hxx"
#include "gravitas_render_object.hxx"
#include "gravitas_scene.hxx"
#include "gravitas_swapchain.hxx"
#include "gravitas_texture.hxx"

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

#include <algorithm>
#include <iostream>
#include <thread>

void
TransferCmdBufDeleteThread(CGravitasDevice *device)
{
   while (true)
   {
      if (device->WaitingForTransfer())
      {
         device->CleanupAfterCompletedTransfer();
      }
      if (device->ExitTransferThread())
      {
         break;
      }
   }
}

CGravitasDevice::CGravitasDevice(IVulkanInstanceOwner      *instance_owner,
                                 IGravitasWindowOwner      *window_owner,
                                 IGravitasSceneOwner       *scene_owner,
                                 GravitasVersionFields      api_version,
                                 VkPhysicalDevice           physical_device,
                                 std::vector<const char *>  enabled_layers,
                                 std::vector<const char *> &required_extensions)
  : instance_owner_(instance_owner)
  , window_owner_(window_owner)
  , scene_owner_(scene_owner)
  , api_version_(api_version)
  , vk_physical_device_(physical_device)
{
   VkPhysicalDeviceProperties props;
   vkGetPhysicalDeviceProperties(physical_device, &props);
   device_limits_ = props.limits;

   vkGetPhysicalDeviceFeatures(physical_device, &device_features_);

   std::vector<VkQueueFamilyProperties> queue_family_props;
   uint32_t                             queue_count         = 0;
   float                                queue_priorities[1] = {1.0};

   vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_count, nullptr);
   if (0 == queue_count)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to query vkGetPhysicalDeviceQueueFamilyProperties "
                                      "count!";
      abort();
   }

   queue_family_props.resize(queue_count);
   vkGetPhysicalDeviceQueueFamilyProperties(
     physical_device, &queue_count, queue_family_props.data());
   if (0 == queue_count)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to query vkGetPhysicalDeviceQueueFamilyProperties "
                                      "queues!";
      abort();
   }

   for (uint32_t queue = 0; queue < queue_family_props.size(); ++queue)
   {
      auto queue_props = queue_family_props[queue];
      if (queue_props.queueFlags & VK_QUEUE_GRAPHICS_BIT)
      {
         graphics_queue_index_ = queue;
         if (!separate_transfer_queue_ && (queue_props.queueFlags & VK_QUEUE_TRANSFER_BIT))
         {
            transfer_queue_index_ = queue;
         }
      }
      else if (queue_props.queueFlags & VK_QUEUE_TRANSFER_BIT)
      {
         transfer_queue_index_    = queue;
         separate_transfer_queue_ = true;
      }
   }

   // Set how many queues we're actually going to use
   queue_count = separate_transfer_queue_ ? 2 : 1;

   // Define queue create info
   VkDeviceQueueCreateInfo queue_create_info[2] = {};
   queue_create_info[0].sType                   = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
   queue_create_info[0].pNext                   = nullptr;
   queue_create_info[0].flags                   = 0;
   queue_create_info[0].queueFamilyIndex        = graphics_queue_index_;
   queue_create_info[0].queueCount              = 1;
   queue_create_info[0].pQueuePriorities        = queue_priorities;
   queue_create_info[1].sType                   = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
   queue_create_info[1].pNext                   = nullptr;
   queue_create_info[1].flags                   = 0;
   queue_create_info[1].queueFamilyIndex        = transfer_queue_index_;
   queue_create_info[1].queueCount              = 1;
   queue_create_info[1].pQueuePriorities        = queue_priorities;

   // Add device extensions we need in general
   required_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

   VkPhysicalDeviceFeatures device_features {};
   if (device_features_.samplerAnisotropy)
   {
      device_features.samplerAnisotropy = VK_TRUE;
   }

   // Define the device create info (referencing queue create info)
   VkDeviceCreateInfo device_create_info      = {};
   device_create_info.sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
   device_create_info.pNext                   = nullptr;
   device_create_info.flags                   = 0;
   device_create_info.queueCreateInfoCount    = queue_count;
   device_create_info.pQueueCreateInfos       = queue_create_info;
   device_create_info.enabledLayerCount       = static_cast<uint32_t>(enabled_layers.size());
   device_create_info.ppEnabledLayerNames     = enabled_layers.data();
   device_create_info.enabledExtensionCount   = static_cast<uint32_t>(required_extensions.size());
   device_create_info.ppEnabledExtensionNames = required_extensions.data();
   device_create_info.pEnabledFeatures        = &device_features;
   VkResult res = vkCreateDevice(vk_physical_device_, &device_create_info, nullptr, &vk_device_);
   if (VK_SUCCESS != res)
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to create VkDevice!";
      abort();
   }

   // Determine appropriate queues
   vkGetDeviceQueue(vk_device_, graphics_queue_index_, 0, &vk_graphics_queue_);

   // Create a command pool for the graphics queue for various work (including
   // transfers if there is no transfer queue)
   VkCommandPoolCreateInfo cmd_pool_info = {};
   cmd_pool_info.sType                   = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
   cmd_pool_info.pNext                   = nullptr;
   cmd_pool_info.flags                   = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
   cmd_pool_info.queueFamilyIndex        = graphics_queue_index_;
   if (VK_SUCCESS !=
       vkCreateCommandPool(vk_device_, &cmd_pool_info, nullptr, &vk_graphics_cmd_pool_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating device graphics command pool!";
      abort();
   }

   if (separate_transfer_queue_)
   {
      vkGetDeviceQueue(vk_device_, transfer_queue_index_, 0, &vk_transfer_queue_);

      // Create a command pool for the transfer queue
      VkCommandPoolCreateInfo cmd_pool_info = {};
      cmd_pool_info.sType                   = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
      cmd_pool_info.pNext                   = nullptr;
      cmd_pool_info.flags                   = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
      cmd_pool_info.queueFamilyIndex        = transfer_queue_index_;
      if (VK_SUCCESS !=
          vkCreateCommandPool(vk_device_, &cmd_pool_info, nullptr, &vk_transfer_cmd_pool_))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating device transfer command pool!";
         abort();
      }
   }

   // Get the global vkGetDeviceProcAddr to get the device-specific vkGetDeviceProcAddr
   PFN_vkGetDeviceProcAddr tmp_get_dev_proc = reinterpret_cast<PFN_vkGetDeviceProcAddr>(
     instance_owner->GetVulkanInstanceProcAddr("vkGetDeviceProcAddr"));
   if (nullptr == tmp_get_dev_proc)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Invalid vkGetDeviceProcAddr returned from vkGetInstanceProcAddr";
      abort();
   }
   pfn_vkGetDeviceProcAddr_ =
     reinterpret_cast<PFN_vkGetDeviceProcAddr>(tmp_get_dev_proc(vk_device_, "vkGetDeviceProcAddr"));
   if (nullptr == pfn_vkGetDeviceProcAddr_)
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Invalid vkGetDeviceProcAddr returned from vkGetDeviceProcAddr";
      abort();
   }

   GravitasInitDeviceDispatchTable(vk_device_, &vk_dispatch_table_, pfn_vkGetDeviceProcAddr_);

   // Initialize the Vulkan memory allocator for this device
   VmaAllocatorCreateInfo allocatorInfo = {};
   allocatorInfo.flags                  = 0;
   allocatorInfo.physicalDevice         = vk_physical_device_;
   allocatorInfo.device                 = vk_device_;
   allocatorInfo.instance               = instance_owner_->GetInstance();
   allocatorInfo.vulkanApiVersion = VK_MAKE_API_VERSION(0, api_version.major, api_version.minor, 0);
   if (VK_SUCCESS != vmaCreateAllocator(&allocatorInfo, &vma_allocator_))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating memory allocator for device!";
      abort();
   }

   transfer_delete_thread_ = new std::thread(TransferCmdBufDeleteThread, this);
}

CGravitasDevice::~CGravitasDevice()
{
   vmaDestroyAllocator(vma_allocator_);
   exit_transfer_thread_ = true;
   transfer_delete_thread_->join();
   if (VK_NULL_HANDLE != vk_graphics_cmd_pool_)
   {
      vk_dispatch_table_.DestroyCommandPool(vk_device_, vk_graphics_cmd_pool_, nullptr);
      vk_graphics_cmd_pool_ = VK_NULL_HANDLE;
   }
   if (VK_NULL_HANDLE != vk_transfer_cmd_pool_)
   {
      vk_dispatch_table_.DestroyCommandPool(vk_device_, vk_transfer_cmd_pool_, nullptr);
      vk_transfer_cmd_pool_ = VK_NULL_HANDLE;
   }
   if (VK_NULL_HANDLE != vk_device_)
   {
      vkDestroyDevice(vk_device_, nullptr);
   }
}

void
CGravitasDevice::CopyPendingTransferSemaphore(std::vector<VkSemaphore> &sem)
{
   std::unique_lock<std::mutex> lck(submitted_transfer_mutex_);
   sem.insert(sem.begin(), pending_transfer_sems_.begin(), pending_transfer_sems_.end());
   pending_transfer_sems_.clear();
}

bool
CGravitasDevice::TransitionImageToShaderReadable(CGravitasTexture *texture)
{
   VkCommandBufferAllocateInfo cmd_buf_info = {};
   cmd_buf_info.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
   cmd_buf_info.pNext                       = nullptr;
   cmd_buf_info.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
   cmd_buf_info.commandPool                 = vk_graphics_cmd_pool_;
   cmd_buf_info.commandBufferCount          = 1;

   VkSemaphoreCreateInfo semaphore_create_info = {};
   semaphore_create_info.sType                 = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
   semaphore_create_info.pNext                 = nullptr;
   semaphore_create_info.flags                 = 0;
   VkSemaphore signal_semaphore                = VK_NULL_HANDLE;
   if (VK_SUCCESS != vk_dispatch_table_.CreateSemaphore(
                       vk_device_, &semaphore_create_info, nullptr, &signal_semaphore))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer queue semaphore";
      return false;
   }

   VkFenceCreateInfo fence_create_info = {};
   fence_create_info.sType             = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
   fence_create_info.pNext             = nullptr;
   fence_create_info.flags             = 0;
   VkFence signal_fence                = VK_NULL_HANDLE;
   if (VK_SUCCESS !=
       vk_dispatch_table_.CreateFence(vk_device_, &fence_create_info, nullptr, &signal_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer fence";
      return false;
   }

   // Allocate a command buffer for the transfer
   VkCommandBuffer cmd_buf;
   if (VK_SUCCESS != vk_dispatch_table_.AllocateCommandBuffers(vk_device_, &cmd_buf_info, &cmd_buf))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer command buffer!";
      return false;
   }

   VkCommandBufferBeginInfo beginInfo {};
   beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
   beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

   vk_dispatch_table_.BeginCommandBuffer(cmd_buf, &beginInfo);

   VkImageSubresourceRange image_subresource_range;
   image_subresource_range.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
   image_subresource_range.baseMipLevel   = 0;
   image_subresource_range.levelCount     = 1;
   image_subresource_range.baseArrayLayer = 0;
   image_subresource_range.layerCount     = 1;

   // This performs the same copy process documented in the texture_mipmap_generation
   // sample in the Vulkan samples repository
   // (https://github.com/KhronosGroup/Vulkan-Samples)
   for (uint32_t cur_mip = 1; cur_mip < texture->GetNumberMipLevels(); cur_mip++)
   {
      uint32_t    prev_level                   = cur_mip - 1;
      VkImageBlit image_blit                   = {};
      image_blit.srcSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
      image_blit.srcSubresource.mipLevel       = prev_level;
      image_blit.srcSubresource.baseArrayLayer = 0;
      image_blit.srcSubresource.layerCount     = 1;
      image_blit.srcOffsets[0]                 = {0, 0, 0};
      image_blit.srcOffsets[1]                 = {
                        int32_t(texture->GetWidth() >> prev_level), int32_t(texture->GetHeight() >> prev_level), 1};
      image_blit.dstSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
      image_blit.dstSubresource.mipLevel       = cur_mip;
      image_blit.dstSubresource.baseArrayLayer = 0;
      image_blit.dstSubresource.layerCount     = 1;
      image_blit.dstOffsets[0]                 = {0, 0, 0};
      image_blit.dstOffsets[1]                 = {
                        int32_t(texture->GetWidth() >> cur_mip), int32_t(texture->GetHeight() >> cur_mip), 1};

      // Prepare current mip level as image blit destination
      image_subresource_range.baseMipLevel  = cur_mip;
      VkImageMemoryBarrier mip_dest_barrier = {};
      mip_dest_barrier.sType                = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
      mip_dest_barrier.pNext                = nullptr;
      mip_dest_barrier.oldLayout            = VK_IMAGE_LAYOUT_UNDEFINED;
      mip_dest_barrier.newLayout            = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
      mip_dest_barrier.image                = texture->GetVkImage();
      mip_dest_barrier.subresourceRange     = image_subresource_range;
      mip_dest_barrier.srcAccessMask        = 0;
      mip_dest_barrier.srcQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
      mip_dest_barrier.dstAccessMask        = VK_ACCESS_TRANSFER_WRITE_BIT;
      mip_dest_barrier.dstQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
      vk_dispatch_table_.CmdPipelineBarrier(cmd_buf,
                                            VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            0,
                                            0,
                                            nullptr,
                                            0,
                                            nullptr,
                                            1,
                                            &mip_dest_barrier);

      // Blit from previous level
      vkCmdBlitImage(cmd_buf,
                     texture->GetVkImage(),
                     VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                     texture->GetVkImage(),
                     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                     1,
                     &image_blit,
                     VK_FILTER_LINEAR);

      // Prepare current mip level as image blit source for next level
      VkImageMemoryBarrier mip_src_barrier = mip_dest_barrier;
      mip_src_barrier.oldLayout            = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
      mip_src_barrier.newLayout            = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
      mip_src_barrier.srcAccessMask        = VK_ACCESS_TRANSFER_READ_BIT;
      mip_src_barrier.dstAccessMask        = VK_ACCESS_TRANSFER_WRITE_BIT;
      vk_dispatch_table_.CmdPipelineBarrier(cmd_buf,
                                            VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            VK_PIPELINE_STAGE_TRANSFER_BIT,
                                            0,
                                            0,
                                            nullptr,
                                            0,
                                            nullptr,
                                            1,
                                            &mip_src_barrier);
   }

   // Add a barrier to change the top-level image layout to what we want for the number
   // of mipmap levels we need
   image_subresource_range.baseMipLevel = 0;
   image_subresource_range.levelCount   = texture->GetNumberMipLevels();

   VkImageMemoryBarrier transit_to_shader_read_barrier = {};
   transit_to_shader_read_barrier.sType                = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
   transit_to_shader_read_barrier.pNext                = nullptr;
   transit_to_shader_read_barrier.oldLayout            = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
   transit_to_shader_read_barrier.newLayout            = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
   transit_to_shader_read_barrier.image                = texture->GetVkImage();
   transit_to_shader_read_barrier.subresourceRange     = image_subresource_range;
   transit_to_shader_read_barrier.srcAccessMask        = VK_ACCESS_TRANSFER_READ_BIT;
   transit_to_shader_read_barrier.srcQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
   transit_to_shader_read_barrier.dstAccessMask        = VK_ACCESS_SHADER_READ_BIT;
   transit_to_shader_read_barrier.dstQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
   vk_dispatch_table_.CmdPipelineBarrier(cmd_buf,
                                         VK_PIPELINE_STAGE_TRANSFER_BIT,
                                         VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                                         0,
                                         0,
                                         nullptr,
                                         0,
                                         nullptr,
                                         1,
                                         &transit_to_shader_read_barrier);
   vk_dispatch_table_.EndCommandBuffer(cmd_buf);

   VkSubmitInfo submit_info         = {};
   submit_info.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
   submit_info.pNext                = nullptr;
   submit_info.waitSemaphoreCount   = 0;
   submit_info.pWaitSemaphores      = nullptr;
   submit_info.signalSemaphoreCount = 1;
   submit_info.pSignalSemaphores    = &signal_semaphore;
   submit_info.commandBufferCount   = 1;
   submit_info.pCommandBuffers      = &cmd_buf;
   if (VK_SUCCESS !=
       vk_dispatch_table_.QueueSubmit(vk_graphics_queue_, 1, &submit_info, signal_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to submit transfer command buffer to transfer queue";
      return false;
   }

   // Trigger a thread to wait on the fence for deletion
   // NOTE: Don't lock since we're already in the lock
   SubmittedCmdBufferInfo submitted_transfer_infos;
   submitted_transfer_infos.texture                = texture;
   submitted_transfer_infos.cmd_pool               = cmd_buf_info.commandPool;
   submitted_transfer_infos.cmd_buf                = cmd_buf;
   submitted_transfer_infos.fence                  = signal_fence;
   submitted_transfer_infos.semaphore              = signal_semaphore;
   submitted_transfer_infos.shader_read_transition = true;
   sub_transfer_info_.push_back(submitted_transfer_infos);
   pending_transfer_sems_.push_back(signal_semaphore);

   return true;
}

bool
CGravitasDevice::CleanupAfterCompletedTransfer()
{
   std::unique_lock<std::mutex> lck(submitted_transfer_mutex_);
   bool                         notify_complete = true;
   VkResult                     wait_result =
     vkWaitForFences(vk_device_, 1, &sub_transfer_info_[0].fence, true, 1000000000);
   if (VK_ERROR_DEVICE_LOST == wait_result)
   {
      CGravitasLog(GRAV_LOG_WARNING)
        << "Waiting fence transfer command buffer revealed DEVICE_LOST!";
   }
   vkDestroyFence(vk_device_, sub_transfer_info_[0].fence, nullptr);
   vkFreeCommandBuffers(
     vk_device_, sub_transfer_info_[0].cmd_pool, 1, &sub_transfer_info_[0].cmd_buf);

   // If texture, transfer to shader readable if this complete isn't already for a transition
   // to shader readable.
   if (nullptr != sub_transfer_info_[0].texture && !sub_transfer_info_[0].shader_read_transition)
   {
      TransitionImageToShaderReadable(sub_transfer_info_[0].texture);

      // Don't notify until we transfer again
      notify_complete = false;
   }

   for (auto &pending_sem : pending_transfer_sems_)
   {
      if (pending_sem == sub_transfer_info_[0].semaphore)
      {
         pending_transfer_sems_.erase(
           std::remove(pending_transfer_sems_.begin(), pending_transfer_sems_.end(), pending_sem),
           pending_transfer_sems_.end());
         vkDestroySemaphore(vk_device_, sub_transfer_info_[0].semaphore, nullptr);
         break;
      }
   }
   if (notify_complete)
   {
      if (nullptr != sub_transfer_info_[0].render_object)
      {
         sub_transfer_info_[0].render_object->SetTransferUploadComplete(true);
      }
      if (nullptr != sub_transfer_info_[0].texture)
      {
         sub_transfer_info_[0].texture->SetTransferUploadComplete(true);
      }
   }
   sub_transfer_info_.erase(sub_transfer_info_.begin());
   return true;
}

bool
CGravitasDevice::SubmitBufferTransferWork(CGravitasRenderObject *render_object,
                                          VkBuffer               src_buffer,
                                          VkBuffer               dst_buffer,
                                          VkDeviceSize           size)
{
   VkQueue                     transfer_queue;
   VkCommandBufferAllocateInfo cmd_buf_info = {};
   cmd_buf_info.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
   cmd_buf_info.pNext                       = nullptr;
   cmd_buf_info.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
   cmd_buf_info.commandBufferCount          = 1;
   if (HasSeperateTransferQueue())
   {
      cmd_buf_info.commandPool = vk_transfer_cmd_pool_;
      transfer_queue           = vk_transfer_queue_;
   }
   else
   {
      cmd_buf_info.commandPool = vk_graphics_cmd_pool_;
      transfer_queue           = vk_graphics_queue_;
   }

   // Allocate a command buffer for the transfer
   VkCommandBuffer cmd_buf;
   if (VK_SUCCESS != vk_dispatch_table_.AllocateCommandBuffers(vk_device_, &cmd_buf_info, &cmd_buf))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer command buffer!";
      return false;
   }

   VkSemaphoreCreateInfo semaphore_create_info = {};
   semaphore_create_info.sType                 = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
   semaphore_create_info.pNext                 = nullptr;
   semaphore_create_info.flags                 = 0;
   VkSemaphore signal_semaphore                = VK_NULL_HANDLE;
   if (VK_SUCCESS != vk_dispatch_table_.CreateSemaphore(
                       vk_device_, &semaphore_create_info, nullptr, &signal_semaphore))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer queue semaphore";
      return false;
   }

   VkFenceCreateInfo fence_create_info = {};
   fence_create_info.sType             = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
   fence_create_info.pNext             = nullptr;
   fence_create_info.flags             = 0;
   VkFence signal_fence                = VK_NULL_HANDLE;
   if (VK_SUCCESS !=
       vk_dispatch_table_.CreateFence(vk_device_, &fence_create_info, nullptr, &signal_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer fence";
      return false;
   }

   VkCommandBufferBeginInfo beginInfo {};
   beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
   beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

   vkBeginCommandBuffer(cmd_buf, &beginInfo);

   VkBufferCopy buffer_copy {};
   buffer_copy.size = size;
   vkCmdCopyBuffer(cmd_buf, src_buffer, dst_buffer, 1, &buffer_copy);

   vkEndCommandBuffer(cmd_buf);

   VkSubmitInfo submit_info         = {};
   submit_info.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
   submit_info.pNext                = nullptr;
   submit_info.waitSemaphoreCount   = 0;
   submit_info.pWaitSemaphores      = nullptr;
   submit_info.signalSemaphoreCount = 1;
   submit_info.pSignalSemaphores    = &signal_semaphore;
   submit_info.commandBufferCount   = 1;
   submit_info.pCommandBuffers      = &cmd_buf;
   if (VK_SUCCESS != vk_dispatch_table_.QueueSubmit(transfer_queue, 1, &submit_info, signal_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to submit transfer command buffer to transfer queue";
      return false;
   }

   // Trigger a thread to wait on the fence for deletion
   std::unique_lock<std::mutex> lck(submitted_transfer_mutex_);
   {
      SubmittedCmdBufferInfo submitted_transfer_infos;
      submitted_transfer_infos.render_object = render_object;
      submitted_transfer_infos.cmd_pool      = cmd_buf_info.commandPool;
      submitted_transfer_infos.cmd_buf       = cmd_buf;
      submitted_transfer_infos.fence         = signal_fence;
      submitted_transfer_infos.semaphore     = signal_semaphore;
      sub_transfer_info_.push_back(submitted_transfer_infos);
      pending_transfer_sems_.push_back(signal_semaphore);
   }
   return true;
}

bool
CGravitasDevice::SubmitImageTransferWork(CGravitasTexture *texture,
                                         VkBuffer          src_buffer,
                                         VkImage           dst_image,
                                         VkDeviceSize      size,
                                         bool              gen_mipmaps)
{
   VkQueue                     transfer_queue;
   VkCommandBufferAllocateInfo cmd_buf_info = {};
   cmd_buf_info.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
   cmd_buf_info.pNext                       = nullptr;
   cmd_buf_info.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
   cmd_buf_info.commandBufferCount          = 1;
   if (HasSeperateTransferQueue())
   {
      cmd_buf_info.commandPool = vk_transfer_cmd_pool_;
      transfer_queue           = vk_transfer_queue_;
   }
   else
   {
      cmd_buf_info.commandPool = vk_graphics_cmd_pool_;
      transfer_queue           = vk_graphics_queue_;
   }

   // Allocate a command buffer for the transfer
   VkCommandBuffer cmd_buf;
   if (VK_SUCCESS != vk_dispatch_table_.AllocateCommandBuffers(vk_device_, &cmd_buf_info, &cmd_buf))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer command buffer!";
      return false;
   }

   VkSemaphoreCreateInfo semaphore_create_info = {};
   semaphore_create_info.sType                 = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
   semaphore_create_info.pNext                 = nullptr;
   semaphore_create_info.flags                 = 0;
   VkSemaphore signal_semaphore                = VK_NULL_HANDLE;
   if (VK_SUCCESS != vk_dispatch_table_.CreateSemaphore(
                       vk_device_, &semaphore_create_info, nullptr, &signal_semaphore))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer queue semaphore";
      return false;
   }

   VkFenceCreateInfo fence_create_info = {};
   fence_create_info.sType             = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
   fence_create_info.pNext             = nullptr;
   fence_create_info.flags             = 0;
   VkFence signal_fence                = VK_NULL_HANDLE;
   if (VK_SUCCESS !=
       vk_dispatch_table_.CreateFence(vk_device_, &fence_create_info, nullptr, &signal_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed creating transfer fence";
      return false;
   }

   VkCommandBufferBeginInfo beginInfo {};
   beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
   beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

   vk_dispatch_table_.BeginCommandBuffer(cmd_buf, &beginInfo);

   VkImageSubresourceRange image_subresource_range;
   image_subresource_range.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
   image_subresource_range.baseMipLevel   = 0;
   image_subresource_range.levelCount     = 1;
   image_subresource_range.baseArrayLayer = 0;
   image_subresource_range.layerCount     = 1;

   // Add a barrier to control the transfer
   VkImageMemoryBarrier prepare_copy_barrier = {};
   prepare_copy_barrier.sType                = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
   prepare_copy_barrier.pNext                = nullptr;
   prepare_copy_barrier.oldLayout            = VK_IMAGE_LAYOUT_UNDEFINED;
   prepare_copy_barrier.newLayout            = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
   prepare_copy_barrier.image                = texture->GetVkImage();
   prepare_copy_barrier.subresourceRange     = image_subresource_range;
   prepare_copy_barrier.srcAccessMask        = 0;
   prepare_copy_barrier.srcQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
   prepare_copy_barrier.dstAccessMask        = VK_ACCESS_TRANSFER_WRITE_BIT;
   prepare_copy_barrier.dstQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
   vk_dispatch_table_.CmdPipelineBarrier(cmd_buf,
                                         VK_PIPELINE_STAGE_TRANSFER_BIT,
                                         VK_PIPELINE_STAGE_TRANSFER_BIT,
                                         0,
                                         0,
                                         nullptr,
                                         0,
                                         nullptr,
                                         1,
                                         &prepare_copy_barrier);

   // Now perform the copy operation
   VkBufferImageCopy image_copy               = {};
   image_copy.bufferOffset                    = 0;
   image_copy.bufferRowLength                 = 0;
   image_copy.bufferImageHeight               = 0;
   image_copy.imageSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
   image_copy.imageSubresource.mipLevel       = 0;
   image_copy.imageSubresource.baseArrayLayer = 0;
   image_copy.imageSubresource.layerCount     = 1;
   image_copy.imageExtent.width               = texture->GetWidth();
   image_copy.imageExtent.height              = texture->GetHeight();
   image_copy.imageExtent.depth               = 1;
   image_copy.imageOffset                     = {0, 0, 0};
   vk_dispatch_table_.CmdCopyBufferToImage(cmd_buf,
                                           src_buffer,
                                           texture->GetVkImage(),
                                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                           1,
                                           &image_copy);

   // Add a barrier to change the layout to what we want
   VkImageMemoryBarrier trans_to_read_barrier = prepare_copy_barrier;
   trans_to_read_barrier.oldLayout            = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
   trans_to_read_barrier.newLayout            = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
   trans_to_read_barrier.srcAccessMask        = VK_ACCESS_TRANSFER_WRITE_BIT;
   trans_to_read_barrier.dstAccessMask        = VK_ACCESS_TRANSFER_READ_BIT;
   vk_dispatch_table_.CmdPipelineBarrier(cmd_buf,
                                         VK_PIPELINE_STAGE_TRANSFER_BIT,
                                         VK_PIPELINE_STAGE_TRANSFER_BIT,
                                         0,
                                         0,
                                         nullptr,
                                         0,
                                         nullptr,
                                         1,
                                         &trans_to_read_barrier);
   vk_dispatch_table_.EndCommandBuffer(cmd_buf);

   VkSubmitInfo submit_info         = {};
   submit_info.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
   submit_info.pNext                = nullptr;
   submit_info.waitSemaphoreCount   = 0;
   submit_info.pWaitSemaphores      = nullptr;
   submit_info.signalSemaphoreCount = 1;
   submit_info.pSignalSemaphores    = &signal_semaphore;
   submit_info.commandBufferCount   = 1;
   submit_info.pCommandBuffers      = &cmd_buf;
   if (VK_SUCCESS != vk_dispatch_table_.QueueSubmit(transfer_queue, 1, &submit_info, signal_fence))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed to submit transfer command buffer to transfer queue";
      return false;
   }

   // Trigger a thread to wait on the fence for deletion
   std::unique_lock<std::mutex> lck(submitted_transfer_mutex_);
   {
      SubmittedCmdBufferInfo submitted_transfer_infos;
      submitted_transfer_infos.texture   = texture;
      submitted_transfer_infos.cmd_pool  = cmd_buf_info.commandPool;
      submitted_transfer_infos.cmd_buf   = cmd_buf;
      submitted_transfer_infos.fence     = signal_fence;
      submitted_transfer_infos.semaphore = signal_semaphore;
      sub_transfer_info_.push_back(submitted_transfer_infos);
      pending_transfer_sems_.push_back(signal_semaphore);
   }
   return true;
}