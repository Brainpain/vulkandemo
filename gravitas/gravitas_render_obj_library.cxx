//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_render_obj_library.hxx"

#include "gravitas_device.hxx"
#include "gravitas_device_dispatch.hxx"
#include "gravitas_log.hxx"
#include "gravitas_render_object.hxx"
#include "gravitas_resource_manager.hxx"
#include "gravitas_scene.hxx"

#include <iostream>
#include <sstream>

CGravitasRenderObjectLibrary::CGravitasRenderObjectLibrary(CGravitasResourceManager *resource_mgr,
                                                           CGravitasDevice *gravitas_device)
  : CGravitasResourceLibrary(resource_mgr, gravitas_device, GRAV_RESOURCE_RENDER_OBJECT)
{}

CGravitasRenderObjectLibrary::~CGravitasRenderObjectLibrary()
{
   FreeInternals();
}

bool
CGravitasRenderObjectLibrary::FindMatchingRenderObject(
  const GravitasRenderObjectResourceCreateInfo *create_info, uint64_t &resource_id)
{
   for (auto &resource : resources_)
   {
      GravitasRenderObjectResource *render_object_resource =
        reinterpret_cast<GravitasRenderObjectResource *>(resource.second);
      if (create_info->file_string == render_object_resource->file_string &&
          create_info->directory_string == render_object_resource->directory_string &&
          create_info->gen_type == render_object_resource->gen_type &&
          create_info->scale.x == render_object_resource->scale.x &&
          create_info->scale.y == render_object_resource->scale.y &&
          create_info->scale.z == render_object_resource->scale.z &&
          create_info->texture_file == render_object_resource->texture_name &&
          ((!create_info->texture_directory.empty() &&
            create_info->texture_directory == render_object_resource->texture_directory) ||
           create_info->texture_directory.empty()) &&
          ((!create_info->shader_directory.empty() &&
            create_info->shader_directory == render_object_resource->shader_directory) ||
           create_info->shader_directory.empty()))
      {
         resource_id = render_object_resource->resource.resource_id;
         return true;
      }
   }

   return false;
}

// Finds if already present and bumps up ref-counts.  If not present, creates and sets ref-count
// to 1 before returning values.
bool
CGravitasRenderObjectLibrary::AddRenderObject(
  const GravitasRenderObjectResourceCreateInfo *create_info, uint64_t &resource_id)
{
   bool                          success                = false;
   GravitasRenderObjectResource *render_object_resource = nullptr;
   GravitasRenderObjectInitInfo  init_info              = {};
   init_info.scale                                      = create_info->scale;
   init_info.color                                      = create_info->color;
   init_info.base_orientation                           = create_info->model_base_orienation;
   init_info.texture_directory                          = create_info->texture_directory;
   init_info.texture_file                               = create_info->texture_file;
   init_info.shader_directory                           = create_info->shader_directory;
   if (!create_info->vertex_shader.empty())
   {
      GravitasShaderRequest shader_req;
      shader_req.stage     = VK_SHADER_STAGE_VERTEX_BIT;
      shader_req.file_name = create_info->vertex_shader;
      init_info.shaders.push_back(shader_req);
   }
   if (!create_info->fragment_shader.empty())
   {
      GravitasShaderRequest shader_req;
      shader_req.stage     = VK_SHADER_STAGE_FRAGMENT_BIT;
      shader_req.file_name = create_info->fragment_shader;
      init_info.shaders.push_back(shader_req);
   }

   resource_id = 0ULL;
   if (!FindMatchingRenderObject(create_info, resource_id))
   {
      render_object_resource = new GravitasRenderObjectResource();
      if (nullptr == render_object_resource)
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating mesh resource for " << create_info->name;
         goto out;
      }
      render_object_resource->render_object =
        new CGravitasRenderObject(gravitas_device_, resource_manager_);
      if (nullptr == render_object_resource)
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed creating mesh for " << create_info->name;
         goto out;
      }

      if (!create_info->file_string.empty())
      {
         if (create_info->file_string.find(".obj") != std::string::npos)
         {
            if (!render_object_resource->render_object->LoadFromObjFile(
                  init_info, create_info->directory_string, create_info->file_string))
            {
               CGravitasLog(GRAV_LOG_ERROR)
                 << "Failed loading OBJ mesh " << create_info->name << " file "
                 << create_info->directory_string + '/' + create_info->file_string;
               goto out;
            }
         }
         else if (create_info->file_string.find(".gltf") != std::string::npos)
         {
            if (!render_object_resource->render_object->LoadFromGltfFile(
                  init_info, create_info->directory_string, create_info->file_string))
            {
               CGravitasLog(GRAV_LOG_ERROR)
                 << "Failed loading GLTF mesh " << create_info->name << " file "
                 << create_info->directory_string + '/' + create_info->file_string;
               goto out;
            }
         }
         else
         {
            CGravitasLog(GRAV_LOG_ERROR)
              << "Unknown mesh file type for file "
              << create_info->directory_string + '/' + create_info->file_string << " for mesh "
              << create_info->name;
            goto out;
         }
      }
      else if (create_info->gen_type != RENDER_OBJECT_GEN_NOTHING)
      {
         switch (create_info->gen_type)
         {
            case RENDER_OBJECT_GEN_QUAD:
               if (!render_object_resource->render_object->CreateQuad(init_info))
               {
                  CGravitasLog(GRAV_LOG_ERROR) << "Failed creating quad mesh " << create_info->name;
                  goto out;
               }
               break;
            case RENDER_OBJECT_GEN_CUBE:
               if (!render_object_resource->render_object->CreateCube(init_info))
               {
                  CGravitasLog(GRAV_LOG_ERROR) << "Failed creating cube mesh " << create_info->name;
                  goto out;
               }
               break;
            case RENDER_OBJECT_GEN_SPHERE:
               if (!render_object_resource->render_object->CreateSphere(init_info))
               {
                  CGravitasLog(GRAV_LOG_ERROR)
                    << "Failed creating sphere mesh " << create_info->name;
                  goto out;
               }
               break;
            case RENDER_OBJECT_GEN_PYRAMID:
               if (!render_object_resource->render_object->CreatePyramid(init_info))
               {
                  CGravitasLog(GRAV_LOG_ERROR)
                    << "Failed creating pyramid mesh " << create_info->name;
                  goto out;
               }
               break;
            default:
               CGravitasLog(GRAV_LOG_ERROR)
                 << "Failed creating mesh because type is unknown " << create_info->name;
               goto out;
         }
      }
      else
      {
         CGravitasLog(GRAV_LOG_ERROR) << "No mesh data for mesh " << create_info->name;
         goto out;
      }

      render_object_resource->resource.imported_ref_count = 0;
      render_object_resource->resource.upload_ref_count   = 0;
      render_object_resource->name                        = create_info->name;
      render_object_resource->file_string                 = create_info->file_string;
      render_object_resource->directory_string            = create_info->directory_string;
      render_object_resource->gen_type                    = create_info->gen_type;
      render_object_resource->scale                       = create_info->scale;
      render_object_resource->color                       = create_info->color;
      render_object_resource->texture_name                = create_info->texture_file;
      if (create_info->texture_directory.empty())
      {
         render_object_resource->texture_directory = create_info->directory_string;
      }
      else
      {
         render_object_resource->texture_directory = create_info->texture_directory;
      }
      if (create_info->shader_directory.empty())
      {
         render_object_resource->shader_directory = create_info->directory_string;
      }
      else
      {
         render_object_resource->shader_directory = create_info->shader_directory;
      }
      if (!AddResource(&render_object_resource->resource, resource_id))
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed adding mesh resource";
         goto out;
      }
      render_object_resource->render_object->SetResourceId(resource_id);
   }

   // Add it to the correct scene now.
   create_info->gravitas_scene->AddRenderObject(render_object_resource->render_object);

   ImportRef(resource_id);
   success = true;

out:

   return success;
}

// Drops ref-count, and deletes if 0.
bool
CGravitasRenderObjectLibrary::RemoveRenderObject(uint64_t &resource_id)
{
   VkDevice           vk_dev = gravitas_device_->GetVulkanDevice();
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed releasing render object resource "
                                   << outstream_uint64.str() << " because it does not exist!";
      return false;
   }
   GravitasRenderObjectResource *render_object_resource =
     reinterpret_cast<GravitasRenderObjectResource *>(resources_[resource_id]);
   if (!ImportUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed decrementing import ref for render object resource "
                                   << outstream_uint64.str() << "!";
      return false;
   }

   if (render_object_resource->resource.imported_ref_count == 0)
   {
      delete render_object_resource->render_object;
      delete render_object_resource;
      resources_.erase(resource_id);
   }
   return true;
}

void
CGravitasRenderObjectLibrary::FreeInternals()
{
   std::unique_lock<std::mutex> lck(resource_mutex_);
   for (auto &resource : resources_)
   {
      GravitasRenderObjectResource *render_object_resource =
        reinterpret_cast<GravitasRenderObjectResource *>(resource.second);
      delete render_object_resource->render_object;
      delete render_object_resource;
   }
   resources_.clear();
}

bool
CGravitasRenderObjectLibrary::Upload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (resources_.find(resource_id) == resources_.end())
   {
      CGravitasLog(GRAV_LOG_ERROR) << "Failed uploading render object resource "
                                   << outstream_uint64.str() << " because it does not exist!";
      return false;
   }
   GravitasRenderObjectResource *render_object_resource =
     reinterpret_cast<GravitasRenderObjectResource *>(resources_[resource_id]);
   if (render_object_resource->resource.upload_ref_count == 0)
   {
      if (!render_object_resource->render_object->Upload())
      {
         CGravitasLog(GRAV_LOG_ERROR)
           << "Failed uploading render object resource " << outstream_uint64.str() << "!";
         return false;
      }
   }
   return UploadRef(resource_id);
}

bool
CGravitasRenderObjectLibrary::Unload(const uint64_t &resource_id)
{
   std::ostringstream outstream_uint64;
   outstream_uint64 << resource_id;
   if (!UploadUnref(resource_id))
   {
      CGravitasLog(GRAV_LOG_ERROR)
        << "Failed unloading render obejct resource " << outstream_uint64.str() << "!";
   }
   GravitasRenderObjectResource *render_object_resource =
     reinterpret_cast<GravitasRenderObjectResource *>(resources_[resource_id]);
   if (render_object_resource->resource.upload_ref_count == 0)
   {
      render_object_resource->render_object->Unload();
   }
   return true;
}
