//
// Copyright (c) 2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_allocations.hxx"
#include "gravitas_resource_library.hxx"

class CGravitasDevice;
struct GravitasDeviceDispatchTable;
class CGravitasResourceManager;

class CGravitasPipelineLibrary : public CGravitasResourceLibrary
{
 public:
   CGravitasPipelineLibrary(CGravitasResourceManager *resource_mgr,
                            CGravitasDevice          *gravitas_device);
   virtual ~CGravitasPipelineLibrary();

   GravitasPipelineResource *GetResource(const uint64_t &resource_id)
   {
      return reinterpret_cast<GravitasPipelineResource *>(resources_[resource_id]);
   }

   bool AddPipeline(const GravitasPipelineResourceCreateInfo *create_info, uint64_t &resource_id);
   bool RemovePipeline(uint64_t &resource_id);
   bool Upload(const uint64_t &resource_id);
   bool Unload(const uint64_t &resource_id);

   // This should only be called on a swapchain refresh or class destruction since all items
   // created with a renderpass will be invalid in the former case and we're just permanently
   // deleting everything in the later.
   void FreeInternals();

 private:
   bool FindMatchingPipeline(const GravitasPipelineResourceCreateInfo *create_info,
                             uint64_t                                 &resource_id);
   bool BindingDescsMatch(const std::vector<VkVertexInputBindingDescription> &binding1,
                          const std::vector<VkVertexInputBindingDescription> &binding2);
   bool AttribDescsMatch(const std::vector<VkVertexInputAttributeDescription> &attrib1,
                         const std::vector<VkVertexInputAttributeDescription> &attrib2);
   bool DoMaterialsMatch(const GravitasMaterialResource *res1,
                         const GravitasMaterialResource *res2);
};
