//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#pragma once

#include "gravitas_base_types.hxx"
#include "gravitas_frame_data.hxx"
#include "gravitas_render_object.hxx"
#include "gravitas_window.hxx"

#include <glm/glm.hpp>
#include <string>
#include <unordered_map>
#include <vector>
#include <vulkan/vk_platform.h>
#include <vulkan/vulkan_core.h>

class CGravitasDevice;
class CGravitasResourceManager;
class CGravitasScene;

class IVulkanInstanceOwner
{
 public:
   IVulkanInstanceOwner()
   {}
   virtual ~IVulkanInstanceOwner()
   {}

   virtual VkInstance         GetInstance() const                         = 0;
   virtual PFN_vkVoidFunction GetVulkanInstanceProcAddr(const char *name) = 0;
};

class IGravitasSceneOwner
{
 public:
   IGravitasSceneOwner()
   {}
   virtual ~IGravitasSceneOwner()
   {}

   virtual CGravitasScene       *GetCurrentScene() const                = 0;
   virtual VkDescriptorSetLayout GetGlobalSceneDescriptorLayout() const = 0;
};

class CGravitasApp
  : public IGravitasWindowOwner
  , IVulkanInstanceOwner
  , IGravitasSceneOwner
{
 public:
   CGravitasApp(const std::string         &application_name,
                const uint32_t            &vulkan_major_version,
                const uint32_t            &vulkan_minor_version,
                std::vector<const char *> &required_instance_extensions,
                bool                       debug,
                bool                       fullscreen,
                uint32_t                   width,
                uint32_t                   height,
                VkFormat                   render_target_format,
                CGravitasScene            *start_scene);
   ~CGravitasApp();

   // Child application rendering loop
   bool RenderLoop();

   // Poll available SDL events : The return is whether or not application should exit
   bool PollSDLEvents();

   // Selection mechanism for chosing a particular render device.
   virtual bool     SelectRendererDevice(std::vector<const char *> &required_device_extensions);
   CGravitasDevice *GetDevice()
   {
      return gravitas_device_;
   }

   CGravitasResourceManager *GetResourceManager()
   {
      return gravitas_resource_manager_;
   }

   // IGravitasWindowOwner methods
   virtual CGravitasWindow *GetWindow() const override
   {
      return gravitas_window_;
   }
   virtual VkFormat GetDesiredFormat() const override
   {
      return render_target_format_;
   }

   // IVulkanInstanceOwner methods
   virtual VkInstance GetInstance() const override
   {
      return vk_instance_;
   }
   virtual PFN_vkVoidFunction GetVulkanInstanceProcAddr(const char *name)
   {
      return pfn_vkGetInstanceProcAddr_(vk_instance_, name);
   }

   bool ActivateNextScene(CGravitasScene *new_scene, float transition_time_sec = 0.f);
   virtual VkDescriptorSetLayout GetGlobalSceneDescriptorLayout() const override
   {
      return scene_desc_set_layout_;
   }
   virtual CGravitasScene *GetCurrentScene() const override
   {
      return cur_scene_;
   }

 protected:
   bool ResizeWindow(uint32_t width, uint32_t height);
   void TogglePause(bool pause);
   bool UpdateAndDrawActiveScenes(SFrameData *frame_data, float time_diff_ms);
   void DestroyOldScene(CGravitasScene *scene);

   std::string                         name_;
   GravitasVersionFields               api_version_;
   std::vector<const char *>           enabled_layers_;
   bool                                debug_                           = false;
   bool                                paused_                          = false;
   bool                                resize_pending_                  = false;
   uint32_t                            resize_width_                    = 0;
   uint32_t                            resize_height_                   = 0;
   CGravitasWindow                    *gravitas_window_                 = nullptr;
   VkInstance                          vk_instance_                     = VK_NULL_HANDLE;
   PFN_vkGetInstanceProcAddr           pfn_vkGetInstanceProcAddr_       = nullptr;
   CGravitasDevice                    *gravitas_device_                 = nullptr;
   CGravitasResourceManager           *gravitas_resource_manager_       = nullptr;
   VkFormat                            render_target_format_            = VK_FORMAT_UNDEFINED;
   VkDescriptorSetLayout               scene_desc_set_layout_           = VK_NULL_HANDLE;
   CGravitasScene                     *cur_scene_                       = nullptr;
   CGravitasScene                     *next_scene_                      = nullptr;
   bool                                transit_scenes_                  = false;
   float                               transition_time_remaining_       = 0.f;
   PFN_vkCreateDebugUtilsMessengerEXT  pfnCreateDebugUtilsMessengerEXT  = nullptr;
   PFN_vkDestroyDebugUtilsMessengerEXT pfnDestroyDebugUtilsMessengerEXT = nullptr;
   VkDebugUtilsMessengerEXT            debug_utils_messenger_           = VK_NULL_HANDLE;
};
