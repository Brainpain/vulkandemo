# clang-format off

cmake_minimum_required(VERSION 3.14)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_definitions(-DNOMINMAX -DSDL_MAIN_HANDLED)

project(lunarg_demoscene)

find_package(SDL2 REQUIRED)
find_package(Vulkan REQUIRED)

option(BENCHMARK_MODE "Enable Benchmark Mode" OFF)
option(FPS_TRACK "Enable FPS Tracking" OFF)
option(DEBUG_WIN_EVENTS "Debug Windows Events" OFF)
option(ASSERT_ON_ERRORS "Assert on Errors" OFF)

# Define a "bin" folder to place everything under
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Define the location of the shaders which will be generated into SPIR-V at compile-time
# and converted into header files.
add_subdirectory(resources)

include_directories(
   BEFORE SYSTEM
      ${Vulkan_INCLUDE_DIRS}
      ${SDL2_INCLUDE_DIRS}
      ${CMAKE_SOURCE_DIR}/external/vma/include
      ${CMAKE_SOURCE_DIR}/external/glm/glm
      ${CMAKE_SOURCE_DIR}/external/tinygltf
      ${CMAKE_SOURCE_DIR}/external/tinyobjloader
      ${CMAKE_SOURCE_DIR}/external/Vulkan-Headers/include
      ${CMAKE_SOURCE_DIR}/gravitas
      ${CMAKE_SOURCE_DIR}/resources/shaders/headers
   )

if (BENCHMARK_MODE)
    ADD_DEFINITIONS(-DENABLE_BENCHMARK_MODE)
endif(BENCHMARK_MODE)
if (FPS_TRACK)
    ADD_DEFINITIONS(-DTRACK_FRAMERATE)
endif(FPS_TRACK)
if (DEBUG_WIN_EVENTS)
    ADD_DEFINITIONS(-DDEBUG_WINDOWS_EVENTS)
endif(DEBUG_WIN_EVENTS)
if (ASSERT_ON_ERRORS)
    ADD_DEFINITIONS(-DASSERT_ON_ERRORS)
endif(ASSERT_ON_ERRORS)

add_subdirectory(external/glm)
add_subdirectory(gravitas)
add_subdirectory(samples)
