//
// Copyright (c) 2021-2022, LunarG, Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
//   Mark Young
//

#include "gravitas_app.hxx"
#include "gravitas_device.hxx"
#include "gravitas_frame_manager.hxx"
#include "gravitas_log.hxx"
#include "gravitas_pipeline_library.hxx"
#include "gravitas_render_obj_library.hxx"
#include "gravitas_render_object.hxx"
#include "gravitas_resource_manager.hxx"
#include "gravitas_scene.hxx"
#include "gravitas_shader_library.hxx"
#include "gravitas_swapchain.hxx"

#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/transform.hpp>
#include <time.h>

const char    application_name[]    = "SimpleSDLDemo";
const int32_t window_default_width  = 1024; // 1920;
const int32_t window_default_height = 768;  // 1080;

class CSimpleSDLScene : public CGravitasScene
{
 public:
   CSimpleSDLScene()
     : CGravitasScene()
   {}
   ~CSimpleSDLScene()
   {
      if (active_)
      {
         StopScene();
      }
      if (loaded_)
      {
         UnloadScene();
      }
   }

   virtual bool InitScene(CGravitasApp *parent, const std::string &scene_name) override
   {
      InternalInit(parent, scene_name);
      camera_.SetPosition(glm::vec3(0.f, 0.f, 5.f));
      camera_.LookAt(glm::vec3(0.f, 0.f, 0.f));

      if (!CreateRenderObjects())
      {
         CGravitasLog(GRAV_LOG_ERROR) << "Failed loading render object!";
         return false;
      }
      return true;
   }

   virtual bool UnloadScene() override
   {
      return CGravitasScene::UnloadScene();
   }

   virtual bool Update(SFrameData *frame_data, float update_time) override
   {
      if (!CGravitasScene::Update(frame_data, update_time))
      {
         return false;
      }

      for (auto &render_obj : render_objects_)
      {
         render_obj->SetOrientation(glm::vec3(0.f, glm::radians(current_frame_ * 0.4f), 0.f));
      }
      return true;
   }

   bool CreateRenderObjects()
   {
      uint64_t                               render_obj_resource_id = 0ULL;
      GravitasRenderObjectResourceCreateInfo render_obj_create_info = {};
      render_obj_create_info.resource_info.flag                     = GRAV_RESOURCE_RENDER_OBJECT;
      render_obj_create_info.name                                   = "cube";
      render_obj_create_info.gravitas_scene                         = this;
      render_obj_create_info.gen_type = RENDER_OBJECT_GEN_SPHERE; // CUBE;
      render_obj_create_info.scale.x                                = 3;
      render_obj_create_info.scale.y                                = 3;
      render_obj_create_info.scale.z                                = 3;
      render_obj_create_info.texture_directory                      = "resources/images";
      render_obj_create_info.texture_file                           = "zion_canyon.jpg";
      if (!gravitas_resource_mgr_->CreateResource(&render_obj_create_info.resource_info,
                                                  render_obj_resource_id))
      {
         return false;
      }
      return true;
   }
};

int32_t
main(int32_t argc, const char **argv)
{
   // Initialize the SDL window and Vulkan Instance
#ifndef NDEBUG
   bool debug_mode = true;
#else
   bool debug_mode = false;
#endif
   bool                      fullscreen        = false;
   uint32_t                  major_api_version = 1;
   uint32_t                  minor_api_version = 3;
   std::vector<const char *> required_instance_extensions;
   CSimpleSDLScene          *sdl_scene    = new CSimpleSDLScene();
   CGravitasApp             *gravitas_app = new CGravitasApp(application_name,
                                                 major_api_version,
                                                 minor_api_version,
                                                 required_instance_extensions,
                                                 debug_mode,
                                                 fullscreen,
                                                 window_default_width,
                                                 window_default_height,
                                                 VK_FORMAT_B8G8R8A8_UNORM,
                                                 sdl_scene);
   if (!gravitas_app)
   {
      return -1;
   }

   // Pick the best device for us
   std::vector<const char *> required_device_extensions;
   if (!gravitas_app->SelectRendererDevice(required_device_extensions))
   {
      return -1;
   }

   if (!gravitas_app->RenderLoop())
   {
      return -1;
   }

   // Cleanup
   delete gravitas_app;

   // Initial scene deletion handled in app

   return 0;
}
