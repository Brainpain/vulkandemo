#!/usr/bin/python3 -i
#
# Copyright (c) 2021-2022, LunarG, Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
import tempfile

# Initialize parser
parser = argparse.ArgumentParser()
 
# Adding optional argument
parser.add_argument("-v", "--Validator", required=True, help = "GlslangValidator Executable")
parser.add_argument("-i", "--Input",     required=True, help = "Input Filename")
parser.add_argument("-o", "--Output",    required=True, help = "Output Filename")

# Read arguments from command line
args = parser.parse_args()

if not args.Input.endswith(".glsl") or args.Input[-10] != '.':
    print("Shader file must end with \'<type>\'.\'.glsl\'.")
    sys.exit()

c_fied_string = args.Input.replace('\\', '/')
short_input_name = c_fied_string.rsplit('/', 1)[-1]
short_array_name = short_input_name.replace('.', '_')[:-5]
array_name = short_array_name + "_array"
tmp_name = tempfile.NamedTemporaryFile().name

c_fied_string = args.Output.replace('\\', '/')
short_output_name = c_fied_string.rsplit('/', 1)[-1]

print("Array Name: %s" % array_name)

glslang_validator_cmd_line = args.Validator + " -V -o " + tmp_name + " --vn " + array_name + " " + args.Input
print("Command Line: %s" % glslang_validator_cmd_line)
os.system(glslang_validator_cmd_line)

copyright_header = "//\n"\
                   "// Copyright (c) 2021-2022, LunarG, Inc\n" \
                   "//\n" \
                   "// Licensed under the Apache License, Version 2.0 (the \"License\");\n" \
                   "// you may not use this file except in compliance with the License.\n" \
                   "// You may obtain a copy of the License at\n" \
                   "//\n" \
                   "//     http://www.apache.org/licenses/LICENSE-2.0\n" \
                   "//\n" \
                   "// Unless required by applicable law or agreed to in writing, software\n" \
                   "// distributed under the License is distributed on an \"AS IS\" BASIS,\n" \
                   "// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" \
                   "// See the License for the specific language governing permissions and\n" \
                   "// limitations under the License.\n" \
                   "//\n" \
                   "// " + short_output_name + " generated from " + short_input_name + " using gen_shader_header.py\n" \
                   "//\n" \
                   "//  !!!! ATTENTION!  GENERATED FILE!  PLEASE MODIFY ORIGINAL SOURCE!\n" \
                   "//\n\n" \
                   "#include <cstdint>\n\n"

final_header_file = open(args.Output, 'w')
final_header_file.write(copyright_header)

tmp_header_file = open(tmp_name,'r')
for line in tmp_header_file:
    if line.find("//") != -1:
        final_header_file.write(line.lstrip())
    elif line.find("#pragma") != -1:
        final_header_file.write(line.lstrip())
        final_header_file.write("\n")
        final_header_file.write("// clang-format off\n\n")
    else:
        final_header_file.write(line)

tmp_header_file.close()
os.remove(tmp_name)

final_header_file.write("// clang-format on\n")
final_header_file.write("\n")
final_header_file .close()
