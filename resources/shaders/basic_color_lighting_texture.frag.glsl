#version 450

layout (location = 0) in vec3 i_color;
layout (location = 1) in vec2 i_texcoord;
layout (location = 2) in vec3 i_position;
layout (location = 3) in vec3 i_normal;

layout(set = 0, binding = 0) uniform SceneData
{
	mat4 view;
	mat4 proj;
	mat4 view_proj;
	vec4 ambient_color;
	vec4 light_position;
	vec4 light_color;
} i_scene_data;
layout(set = 1, binding = 0) uniform sampler2D tex1;

layout(location = 0) out vec4 o_color;

void main() {
	vec3 norm = normalize(i_normal);
	vec3 light_dir = normalize(vec3(i_scene_data.light_position) - i_position);
	float diff = max(dot(norm, light_dir), 0.0);
	vec3 total_outside_color = (i_scene_data.ambient_color.rgb + i_scene_data.light_color.rgb * diff);
	o_color = vec4(texture(tex1, i_texcoord).rgb, 1.0f) * vec4(i_color * total_outside_color, 1.0);
}
