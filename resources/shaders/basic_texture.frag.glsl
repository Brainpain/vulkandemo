#version 450

layout (location = 0) in vec3 i_color;
layout (location = 1) in vec2 i_texcoord;
layout (location = 2) in vec3 i_position;
layout (location = 3) in vec3 i_normal;

layout(set = 1, binding = 0) uniform sampler2D tex1;

layout(location = 0) out vec4 o_color;

void main() {
	o_color = vec4(texture(tex1, i_texcoord).rgb, 1.0f) * vec4(i_color, 1.0);
}
