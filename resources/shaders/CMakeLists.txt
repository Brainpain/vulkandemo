#
# Copyright (c) Mark Young, 2018-2020, All rights reserved.  
# SPDX-License-Identifier: MIT
#
# clang-format off

file(MAKE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/headers)

# Identify all shaders we need compiling
file(GLOB GLSL_SHADERS *.glsl)

if (Vulkan_GLSLANG_VALIDATOR_EXECUTABLE)
    set(GLSLANG_VALIDATOR ${Vulkan_GLSLANG_VALIDATOR_EXECUTABLE})
else()
    if (WIN32)
        find_program(GLSLANG_VALIDATOR glslangValidator.exe)
    else()
        find_program(GLSLANG_VALIDATOR glslangValidator)
    endif()
endif()

# Process vertex shaders
foreach(GlslShader IN LISTS GLSL_SHADERS)
    STRING(REPLACE "." "_" TmpUnderscoreName ${GlslShader})
    STRING(REPLACE "_glsl" ".h" OutputFileName ${TmpUnderscoreName})
    STRING(REPLACE "shaders" "shaders/headers" OutputHeaderFile ${OutputFileName})
    add_custom_command(
                OUTPUT ${OutputHeaderFile}
                COMMAND python3 gen_shader_header.py -v ${GLSLANG_VALIDATOR} -i ${GlslShader} -o ${OutputHeaderFile}
                DEPENDS ${Vulkan_GLSLANG_VALIDATOR_EXECUTABLE} ${GLSL_SHADERS} gen_shader_header.py ${GlslShader}
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                COMMENT "Generating python3 gen_shader_header.py -v ${GLSLANG_VALIDATOR} -i ${GlslShader} -o ${OutputHeaderFile}")
    set(GENERATED_FILES ${GENERATED_FILES} ${OutputHeaderFile})
endforeach()

# Add a result in ${GENERATED_SHADERS} which we will use elsewhere
add_custom_target(GENERATED_SHADERS DEPENDS ${GENERATED_FILES})
