#version 450

layout (location = 0) in vec3 i_position;
layout (location = 1) in vec3 i_normal;
layout (location = 2) in vec2 i_texcoord;

layout(set = 0, binding = 0) uniform SceneData
{
	mat4 view;
	mat4 proj;
	mat4 view_proj;
} i_scene_data;

layout( push_constant ) uniform constants
{
	mat4 transform_mat;
} i_push_consts;

layout (location = 0) out vec3 o_color;
layout (location = 1) out vec2 o_texcoord;
layout (location = 2) out vec3 o_frag_pos;
layout (location = 3) out vec3 o_normal;

void main()
{
	vec4 worldspace_vert = i_push_consts.transform_mat * vec4(i_position, 1.0f);
	o_color = vec3(1.0, 1.0, 1.0);
	o_frag_pos = worldspace_vert.xyz;
	o_texcoord = i_texcoord;
    o_normal = mat3(transpose(inverse(i_push_consts.transform_mat))) * i_normal;
	gl_Position = i_scene_data.view_proj * worldspace_vert;
}
