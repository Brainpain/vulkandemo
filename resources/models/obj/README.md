# Models

Folder:     saber_class
Model Name: Saber Class Yeager NCC 61947
Model Link: https://sketchfab.com/3d-models/saber-class-yeager-ncc-61947-b0081529aada4e0a8e50873469d5922b
Author:     morenostefanuto (https://sketchfab.com/morenostefanuto)
License:    CC Attribution (http://creativecommons.org/licenses/by/4.0/)

Folder:     viking_room
Model Name: Viking room
Model Link: https://sketchfab.com/3d-models/viking-room-a49f1b8e4f5c4ecf9e1fe7d81915ad38
Author:     nigelgoh (https://sketchfab.com/nigelgoh)
License:    CC Attribution (http://creativecommons.org/licenses/by/4.0/)
