# Vulkan Demo

VulkanDemo is a personal development tree that is a work in progress.
My intent is to design a modular interface that I can use to develop
a Vulkan demoscene style applications.

## License

This project is licensed under the Apache License Version 2.0 dated
January 2004.
More information about this license can be read in the provided
[LICENSE](LICENSE.txt) file as well as at the Apache License
website https://www.apache.org/licenses/.

Feel free to use it and release it as allowed under the license.

Anything under "external" will have its own license file in the
top-of the appropriate sub-folder.
Please respect those licenses for their respective project source.

Also, several items under the "resources" folder have their own
licenses which should be called out in license files in the
corresponding directory.

## Notice of Copyright

While this is a personal project, it should be seen as an extension
of my work at [LunarG](https://www.lunarg.com) and the
[LunarG VulkanTools repository](https://github.com/LunarG/VulkanTools), and
thus as copyrighted by LunarG starting in 2021.


## History and Future

This started out as work being done while reading through the
[Vulkan Guide](https://vkguide.dev).
While doing it, though, I deviated to better handle swapchain
resizing which lead to a different architecture.
I've also jumped back and forth over to the
[Vulkan Tutorial](https://vulkan-tutorial.com) so some of the changes
may look familiar to those who have followed that instead.
Once complete, I plan on implementing camera movement and paths using
timing and quaternions and then work on shader fanciness.

## Gravitas Class Diagram

![Gravitas Class Diagram](docs/images/gravitas_diagram.png)

## Directory Layout

```
  gravitas             // Gravitas engine files
  external             // External items (may be submodules)
     glm               //  + OpenGL Mathematics library
     KTX-Software      //  + Khronos KTX image software library
     tinygltf          //  + Header only C++ tiny glTF library
     tinyobjloader     //  + Tiny object model loader
     vma               //  + Vulkan Memory Allocator
     Vulkan-Headers    //  + Latest Vulkan headers
  resources
     images            // Images/textures used
     models            // Model data files (may also contain textures and shaders)
     shaders           // GLSL Shaders available for use
        headers        // Compiled SPIRV binary headers of the GLSL in the directory above
  samples
     simple_sdl_test      // A simple example of drawing using the SDL framework
     simple_obj_model     // A simple obj model loading example
```

## Building

Instructions for building this repository can be found in the [Build.md](./BUILD.md)
file.

